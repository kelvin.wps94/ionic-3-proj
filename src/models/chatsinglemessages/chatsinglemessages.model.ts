export interface ChatSingleMessages {
	key?: string;
	message: string;
	sender: string;
	type: string;
	msgSentTime: any;
}
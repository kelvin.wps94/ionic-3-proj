export interface PollOptions {
	key?: string;
	option: string;
	totalVote: number;
}
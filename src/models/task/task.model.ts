export interface Task {
	key?: string;
	taskName: string;
	taskDue: boolean;
	subTask: boolean;
	poll: boolean;
	assignedMember: boolean;
	priority: string;
}
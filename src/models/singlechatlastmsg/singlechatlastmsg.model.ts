export interface SingleChatLastMsg {
	key?: string;
	lastMsgDate?: any;
	unreadMsgCount?: number;
	isInChat?: boolean;
}
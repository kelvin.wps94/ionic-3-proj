export interface GroupLastMsg {
	key?: string;
	lastMessages: string;
	lastMsgDate: any;
	lastMsgType: string;
}
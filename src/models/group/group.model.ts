export interface Group {
	key?:string;
	groupName:string;
	admin:string;
	groupPhotoURL:string;
}
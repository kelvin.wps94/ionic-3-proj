export interface UserCredentials {
	key?: string;
	email: string;
	password: string;
	displayName?: string;
	photoURL?: string;
	status?: string;
}
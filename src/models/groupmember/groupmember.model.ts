export interface GroupMember {
	key?: string;
	isMember: boolean;
}
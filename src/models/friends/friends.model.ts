export interface Friends {
	key?: string;
	isFriend: boolean;
}
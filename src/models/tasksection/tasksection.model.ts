export interface TaskSection {
	key?: string;
	taskSectionTitle: string;
	isTaskCompletedSection: boolean;
	task?: any;
}
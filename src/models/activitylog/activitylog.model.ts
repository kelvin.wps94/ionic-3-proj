export interface ActivityLog {
	key?: string;
	activityType: string;
	handler: string;
	handlerIsAdmin: boolean;
	dateNTimeHandled: any;
	logActivity: string;
}
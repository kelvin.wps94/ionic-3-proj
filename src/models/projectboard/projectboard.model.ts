export interface ProjectBoard {
	key?: string;
	boardName: string;
	hasTaskCompleteSection: boolean;
	totalTask: number;
	totalCompletedTask: number;
}
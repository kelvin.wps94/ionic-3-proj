export interface AssignedMember {
	key?: string;
	isAssignedToTask: boolean;
}
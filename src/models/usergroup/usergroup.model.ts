export interface UserGroup {
	key?: string;
	hasThisGroup: boolean;
}
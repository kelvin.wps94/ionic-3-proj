export interface Poll {
	key?: string;
	pollingType: string;
	pollingTarget: string;
	pollingDueDate?: any;
	numOfAllowedVote: number;
}
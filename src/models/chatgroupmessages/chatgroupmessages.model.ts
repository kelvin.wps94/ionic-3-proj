export interface ChatGroupMessages {
	key?: string;
	message: string;
	sender: string;
	tempName: string;
	type: string;
	msgSentTime: any;
}
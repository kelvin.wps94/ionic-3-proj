export interface SubTask {
	key?: string;
	subTaskTitle: string;
	isDone: boolean;
}
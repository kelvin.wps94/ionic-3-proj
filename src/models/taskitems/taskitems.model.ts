export interface TaskItems {
	key?: string;
	dueDate: any;
	priority: string;
}
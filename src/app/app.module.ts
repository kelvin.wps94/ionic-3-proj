import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { IonicImageLoader } from 'ionic-image-loader';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FIREBASE_CONFIG } from './firebase.credentials';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FCM } from '@ionic-native/fcm';
import { DatetimePickerModule } from 'ion-datetime-picker-sn';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import FIREBASE from 'firebase';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { UserlistandrequestPage } from '../pages/userlistandrequest/userlistandrequest';
import { AuthProvider } from '../providers/auth/auth';
import { AlertProvider } from '../providers/alert/alert';
import { RegisterPage } from '../pages/register/register';
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { UserProvider } from '../providers/user/user';
import { ProfilepicPage } from '../pages/profilepic/profilepic';
import { ImagehandlerProvider } from '../providers/imagehandler/imagehandler';
import { AlluserlistPage } from '../pages/alluserlist/alluserlist';
import { FriendreqProvider } from '../providers/friendreq/friendreq';
import { FriendsProvider } from '../providers/friends/friends';
import { ToastProvider } from '../providers/toast/toast';
import { FriendsPage } from '../pages/friends/friends';
import { ChatsinglePage } from '../pages/chatsingle/chatsingle';
import { ChatmsgProvider } from '../providers/chatmsg/chatmsg';
import { CreategroupPage } from '../pages/creategroup/creategroup';
import { GroupProvider } from '../providers/group/group';
import { CameraProvider } from '../providers/camera/camera';
import { ChatsinglesettingPage } from '../pages/chatsinglesetting/chatsinglesetting';
import { SelectedprofilePage } from '../pages/selectedprofile/selectedprofile';
import { GroupprofilePage } from '../pages/groupprofile/groupprofile';
import { AddmembertogroupPage } from '../pages/addmembertogroup/addmembertogroup';
import { ChatgroupPage } from '../pages/chatgroup/chatgroup';
import { FirebasestorageProvider } from '../providers/firebasestorage/firebasestorage';
import { ProjectdashboardPage } from '../pages/projectdashboard/projectdashboard';
import { ProjectboardProvider } from '../providers/projectboard/projectboard';
import { SelectedprojboardPage } from '../pages/selectedprojboard/selectedprojboard';
import { TasksectionProvider } from '../providers/tasksection/tasksection';
import { TaskProvider } from '../providers/task/task';
import { PipesModule } from '../pipes/pipes.module';
import { SelectedtaskPage } from '../pages/selectedtask/selectedtask';
import { SubtaskProvider } from '../providers/subtask/subtask';
import { AssignmembertotaskPage } from '../pages/assignmembertotask/assignmembertotask';
import { ProgressBarModule } from 'angular-progress-bar';
import { DatetimeinstringProvider } from '../providers/datetimeinstring/datetimeinstring';
import { HomepagePage } from '../pages/homepage/homepage';
import { ConversationsPage } from '../pages/conversations/conversations';
import { ChatsinglepagePage } from '../pages/chatsinglepage/chatsinglepage';
import { ChatgrouppagePage } from '../pages/chatgrouppage/chatgrouppage';
import { AccountPage } from '../pages/account/account';
import { FriendlistPage } from '../pages/friendlist/friendlist';
import { GrouplistPage } from '../pages/grouplist/grouplist';
import { FindfriendsPage } from '../pages/findfriends/findfriends';
import { GprofilePage } from '../pages/gprofile/gprofile';
import { AddmemtogroupPage } from '../pages/addmemtogroup/addmemtogroup';
import { SelectedprojPage } from '../pages/selectedproj/selectedproj';
import { StaskPage } from '../pages/stask/stask';
import { AssignmemberPage } from '../pages/assignmember/assignmember';
import { ActivitylogPage } from '../pages/activitylog/activitylog';
import { PollPage } from '../pages/poll/poll';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomepagePage,
    ConversationsPage,
    ChatsinglepagePage,
    ChatgrouppagePage,
    AccountPage,
    FriendlistPage,
    GrouplistPage,
    FindfriendsPage,
    GprofilePage,
    AddmemtogroupPage,
    SelectedprojPage,
    StaskPage,
    AssignmemberPage,
    ActivitylogPage,
    PollPage,
    TabsPage,
    UserlistandrequestPage,
    RegisterPage,
    ForgotpasswordPage,
    ProfilepicPage,
    FriendsPage,
    ChatsinglePage,
    CreategroupPage,
    ChatsinglesettingPage,
    SelectedprofilePage,
    GroupprofilePage,
    AddmembertogroupPage,
    ChatgroupPage,
    ProjectdashboardPage,
    SelectedprojboardPage,
    SelectedtaskPage,
    AssignmembertotaskPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {tabsPlacement: 'top', tabsHideOnSubPages: true, mode: 'ios'}),
    IonicImageLoader.forRoot(),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicImageViewerModule,
    ProgressBarModule,
    NgCircleProgressModule.forRoot({
          radius: 35,
          outerStrokeWidth: 12,
          backgroundStroke: "#ffffff",
          backgroundColor: "#343434",
          backgroundOpacity: 1,
          toFixed: 0,
          maxPercent: 100,
          unitsFontSize: "10",
          unitsColor: "#fff",
          innerStrokeWidth: 2,
          outerStrokeColor: "#ffc13a",
          outerStrokeLinecap: "butt",
          innerStrokeColor: "#ffe28a",
          titleColor: "#ffffff",
          titleFontSize: "21",
          showSubtitle: false,
          animationDuration: 300,
        }),
    PipesModule,
    DatetimePickerModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomepagePage,
    ConversationsPage,
    ChatsinglepagePage,
    ChatgrouppagePage,
    AccountPage,
    FriendlistPage,
    GrouplistPage,
    FindfriendsPage,
    GprofilePage,
    AddmemtogroupPage,
    SelectedprojPage,
    StaskPage,
    AssignmemberPage,
    ActivitylogPage,
    PollPage,
    TabsPage,
    UserlistandrequestPage,
    RegisterPage,
    ForgotpasswordPage,
    ProfilepicPage,
    FriendsPage,
    ChatsinglePage,
    CreategroupPage,
    ChatsinglesettingPage,
    SelectedprofilePage,
    GroupprofilePage,
    AddmembertogroupPage,
    ChatgroupPage,
    ProjectdashboardPage,
    SelectedprojboardPage,
    SelectedtaskPage,
    AssignmembertotaskPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FilePath,
    IOSFilePicker,
    FileChooser,
    Camera,
    ScreenOrientation,
    FCM,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    AlertProvider,
    UserProvider,
    ImagehandlerProvider,
    FriendreqProvider,
    FriendsProvider,
    ToastProvider,
    ChatmsgProvider,
    GroupProvider,
    CameraProvider,
    FirebasestorageProvider,
    ProjectboardProvider,
    TasksectionProvider,
    TaskProvider,
    SubtaskProvider,
    DatetimeinstringProvider,
  ]
})
export class AppModule {}

import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { FCM } from '@ionic-native/fcm';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { HomePage } from '../pages/home/home';
import { HomepagePage } from '../pages/homepage/homepage';
import { ConversationsPage } from '../pages/conversations/conversations';
import { TabsPage } from '../pages/tabs/tabs';
import { ChatsinglePage } from '../pages/chatsingle/chatsingle';
import { GroupprofilePage } from '../pages/groupprofile/groupprofile';
import { ChatgroupPage } from '../pages/chatgroup/chatgroup';
import { AccountPage } from '../pages/account/account';
import { FindfriendsPage } from '../pages/findfriends/findfriends';
import { FriendlistPage } from '../pages/friendlist/friendlist';
import { GrouplistPage } from '../pages/grouplist/grouplist';
import { SelectedprofilePage } from '../pages/selectedprofile/selectedprofile';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('myNav') navCtrl: NavController
  rootPage:any;
  deviceToken: string = '';
  activePage: any;

  pages: Array<{title: string, component: any}>;

  constructor(fcm: FCM, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, afAuth: AngularFireAuth, afDatabase: AngularFireDatabase, private imageLoaderConfig: ImageLoaderConfig, screenOrientation: ScreenOrientation) {
    afAuth.authState.subscribe(user => {
      if(!user) {
        this.rootPage = HomePage;
      } else {
        this.rootPage = HomepagePage;

        fcm.getToken()
        .then( token => {
          afDatabase.list('users')
          .update(user.uid, {token: token})
          .then( res => {
            console.log(res);
          })
          .catch( err => {
            console.log(err);
          });
        })
        .catch( err => {
          console.log(err);
        });
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      screenOrientation.lock(screenOrientation.ORIENTATIONS.PORTRAIT);

      this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000);
      this.imageLoaderConfig.enableFallbackAsPlaceholder(false);
      this.imageLoaderConfig.enableDebugMode();

      if(platform.is('ios') || platform.is('android')) {
        fcm.onNotification().subscribe((data) => {
          if(data.wasTapped) {

            if(data.type == 'chatSingle') {

              this.navCtrl.push(ChatsinglePage, {
                key: data.user_id
              });
              
            } else if(data.type == 'groupJoined') {
              this.navCtrl.push(GroupprofilePage, {
                key: data.user_id
              });

            } else if(data.type == 'friendRequest') {
              this.navCtrl.push(SelectedprofilePage, {
                key: data.user_id
              });
            } else if(data.type == 'friends') {
              this.navCtrl.push(SelectedprofilePage, {
                key: data.user_id
              });
            } else if(data.type == 'chatGroup') {

              this.navCtrl.push(ChatgroupPage, {
                key: data.user_id
              });  
            }
          }
        });
      }
    });

    this.pages = [
      {title: 'Home', component: HomepagePage},
      {title: 'Conversations', component: ConversationsPage},
      {title: 'Account', component: AccountPage},
      {title: 'Find Friends', component: FindfriendsPage},
      {title: 'Friends', component: FriendlistPage},
      {title: 'Groups', component: GrouplistPage},
      {title: 'Tabs', component: TabsPage}
    ];

    this.activePage = this.pages[0];
  }

  openPage(page) {
    this.navCtrl.setRoot(page.component);
    this.activePage = page;
  }

  checkActive(page) {
    return page == this.activePage;
  }
}


import { Pipe, PipeTransform } from '@angular/core';
import { SingleChatLastMsg } from '../../models/singlechatlastmsg/singlechatlastmsg.model';
/**
 * Generated class for the SortsinglechatlistPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'sortsinglechatlist',
})
export class SortsinglechatlistPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: SingleChatLastMsg[]) {
  	let result = [];
    
  	if(value != null) {
  		result = value.sort((a, b) => new Date(b.lastMsgDate).getTime() - new Date(a.lastMsgDate).getTime());
  	}

    return result;
  }
}

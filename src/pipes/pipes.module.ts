import { NgModule } from '@angular/core';
import { GettaskPipe } from './gettask/gettask';
import { GetlastmsgPipe } from './getlastmsg/getlastmsg';
import { FilterlastmsgPipe } from './filterlastmsg/filterlastmsg';
import { GethandlernamePipe } from './gethandlername/gethandlername';
import { ReversethelistorderPipe } from './reversethelistorder/reversethelistorder';
import { FiltergroupchatlistPipe } from './filtergroupchatlist/filtergroupchatlist';
import { SortsinglechatlistPipe } from './sortsinglechatlist/sortsinglechatlist';
import { FilterexistinguserPipe } from './filterexistinguser/filterexistinguser';

@NgModule({
	declarations: [GettaskPipe,
    GetlastmsgPipe,
    FilterlastmsgPipe,
    GethandlernamePipe,
    ReversethelistorderPipe,
    FiltergroupchatlistPipe,
    SortsinglechatlistPipe,
    FilterexistinguserPipe],
	imports: [],
	exports: [GettaskPipe,
    GetlastmsgPipe,
    FilterlastmsgPipe,
    GethandlernamePipe,
    ReversethelistorderPipe,
    FiltergroupchatlistPipe,
    SortsinglechatlistPipe,
    FilterexistinguserPipe]
})
export class PipesModule {}

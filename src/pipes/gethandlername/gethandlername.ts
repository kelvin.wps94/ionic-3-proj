import { Pipe, PipeTransform } from '@angular/core';
import { UserCredentials } from '../../models/users/users.model';

/**
 * Generated class for the GethandlernamePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'gethandlername',
})
export class GethandlernamePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */

  transform(value: string, args: UserCredentials[]) {

  	var userName = '';

    args.forEach(elem => {
    	if(value == elem.key) {
    		userName = elem.displayName;
    	}
    });

    return userName;

  }
}

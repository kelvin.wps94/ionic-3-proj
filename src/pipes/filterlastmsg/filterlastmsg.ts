import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FilterlastmsgPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filterlastmsg',
})
export class FilterlastmsgPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, arg: string) {

  	let result = ''

  	if(arg == 'images') {
  		result = 'Image Attachment'
  	} else {
  		result = value;
  	}

    return result;
  }
}

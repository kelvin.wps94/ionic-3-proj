import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the GettaskPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'gettask',
})
export class GettaskPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(obj: any) {

    let result = [];

    for (var key in obj.task) {
      if (obj.task.hasOwnProperty(key)) {
        result.push({
        	key: key,
        	...obj.task[key]
        });
      }
    }

    return result;
  }
}

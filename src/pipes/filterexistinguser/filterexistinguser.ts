import { Pipe, PipeTransform } from '@angular/core';
import { UserCredentials } from '../../models/users/users.model';
import { Friends } from '../../models/friends/friends.model';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';

/**
 * Generated class for the FilterexistinguserPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filterexistinguser',
})
export class FilterexistinguserPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: UserCredentials[], sentReq: FriendRequest[], recReq: FriendRequest[], friendReq: Friends[]) {
  	var result = [];

  	value.forEach(elem => {
  		var found: boolean = false;

  		sentReq.forEach(item => {
  			if(item.key == elem.key) {
  				found = true;
  			}
  		});

  		recReq.forEach(item => {
  			if(item.key == elem.key) {
  				found = true;
  			}
  		});

  		friendReq.forEach(item => {
  			if(item.key == elem.key) {
  				found = true;
  			}
  		});

  		if(!found) {
  			result.push(elem);
  		}
  	});

  	return result;
  }
}

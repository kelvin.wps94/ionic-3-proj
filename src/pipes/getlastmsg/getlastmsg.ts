import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the GetlastmsgPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'getlastmsg',
})
export class GetlastmsgPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(obj: any) {
  	let result = [];

    for (var key in obj) {
    	
    	if(key != 'key' && key != 'lastMsgDate') {

    		result.push({
    			key: key,
    			...obj[key]
    		});
    	}
    }

    return result;
  }
}

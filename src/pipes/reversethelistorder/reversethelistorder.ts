import { Pipe, PipeTransform } from '@angular/core';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import { ActivitylogPage } from '../../pages/activitylog/activitylog';

/**
 * Generated class for the ReversethelistorderPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'reversethelistorder',
})
export class ReversethelistorderPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  
  constructor(private activityLogPage: ActivitylogPage) {

  }

  transform(obj: ActivityLog[], args: any) {
    var result: ActivityLog[] = [];

    var objectLength = 0;

    objectLength = obj.length - 1;

    for(var i = objectLength; i >= 0; i--) {
    	result.push(obj[i]);
    }

    if(args.filterType == 'all') {
    	return result;
    } else if(args.filterType == 'type') {

    	if(args.filterValue == '') {
    		return result;
    	}

    	var filteredResult: ActivityLog[] = [];

    	result.forEach(elem => {
    		if(elem.activityType == args.filterValue) {
    			filteredResult.push({
    				key: elem.key,
    				...elem
    			});
    		}
    	});

    	if(filteredResult.length == 0) {
    		this.activityLogPage.totalOfActivityLog = 0;
    	}

    	return filteredResult;

    } else if(args.filterType == 'date') {
    	let selectedDate = new Date(Date.parse(args.filterValue));
    	var filteredResult: ActivityLog[] = [];

    	result.forEach(elem =>{
    		let listItemDate = new Date(Date.parse(elem.dateNTimeHandled));

    		if(listItemDate.getFullYear() == selectedDate.getFullYear()) {

    			if(listItemDate.getMonth() == selectedDate.getMonth()) {

    				if(listItemDate.getDate() == selectedDate.getDate()) {

    					filteredResult.push({
		    				key: elem.key,
		    				...elem
		    			});
    				}
    			}
    		}
    	});

    	if(filteredResult.length == 0) {
    		this.activityLogPage.totalOfActivityLog = 0;
    	}

    	return filteredResult;
    }
    
  }
}

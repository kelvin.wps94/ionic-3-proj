import { Pipe, PipeTransform } from '@angular/core';
import { UserGroup } from '../../models/usergroup/usergroup.model'
import { GroupLastMsg } from '../../models/grouplastmsg/grouplastmsg.model';
/**
 * Generated class for the FiltergroupchatlistPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filtergroupchatlist',
})
export class FiltergroupchatlistPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: UserGroup[], args: GroupLastMsg[]) {
    let result = [];
    let newArgs = [];

    newArgs = args.sort((a, b) => new Date(b.lastMsgDate).getTime() - new Date(a.lastMsgDate).getTime());

    newArgs.forEach(snap => {
    	value.forEach(s => {
    		if(snap.key == s.key) {
    			result.push(s);
    		}
    	});
    });

    return result;
  }
}

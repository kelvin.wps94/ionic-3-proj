import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Task } from '../../models/task/task.model';
import { TaskItems } from '../../models/taskitems/taskitems.model';
import { AssignedMember } from '../../models/assignedmember/assignedmember.model';
import { PollOptions } from '../../models/polloptions/polloptions.model';
import { Poll } from '../../models/poll/poll.model';
import { Voters } from '../../models/voters/votes.model';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import firebase from 'firebase';
/*
  Generated class for the TaskProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaskProvider {

	private taskRef = this.afDatabase.list<Task>('taskSection');
	private selectedTaskRef = this.afDatabase.object<Task>('taskSection');
  private taskRef2 = this.afDatabase.list('task');
  fireDB = firebase.database().ref('/');

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  getServerTimeStamp() {
    var offsetRef = firebase.database().ref('/.info/serverTimeOffset');
    var estimatedServerTimeMs;

    offsetRef.on("value", function(snap) {
      var offset = snap.val();
      estimatedServerTimeMs = new Date().getTime() + offset;
    });

    return estimatedServerTimeMs;
  }

  addTask(projBoardKey, taskSectKey, task: Task) {
  	this.taskRef = this.afDatabase.list<Task>('taskSection/' + projBoardKey + '/' + taskSectKey + '/task');

  	var promise = new Promise((resolve, reject) => {
  		this.taskRef.push(task)
  		.then(res => {
  			resolve(true);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  firebaseCRUD(obj) {
    var promise = new Promise((resolve, reject) => {
      this.fireDB.update(obj)
      .then( res => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getSelectedTaskDetails(projBoardKey, taskSectKey, taskKey) {
  	this.selectedTaskRef = this.afDatabase.object<Task>('taskSection/' + projBoardKey + '/' + taskSectKey + '/task/' + taskKey);

  	return this.selectedTaskRef;
  }

  updateTaskItemsStatus(projBoardKey, taskSectKey, taskKey, task: Task) {
    this.taskRef = this.afDatabase.list<Task>('taskSection/' + projBoardKey + '/' + taskSectKey + '/task');

    var promise = new Promise((resolve, reject) => {
      this.taskRef.update(taskKey, task)
      .then( () => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  setTaskDueDate(taskKey, dueDate) {

    this.taskRef2 = this.afDatabase.list('task');

    var promise = new Promise((resolve,reject) => {
      this.taskRef2.update(taskKey, {
        dueDate: dueDate
      })
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  removeTaskDueDate(taskKey) {

    this.taskRef2 = this.afDatabase.list('task');

    var promise = new Promise((resolve,reject) => {
      this.taskRef2.update(taskKey, {
        dueDate: null
      })
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  setTaskPriority(taskKey, priority) {

    this.taskRef2 = this.afDatabase.list('task');

    var promise = new Promise((resolve,reject) => {
      this.taskRef2.update(taskKey, {
        priority: priority
      })
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  removeTaskPriority(taskKey) {

    this.taskRef2 = this.afDatabase.list('task');

    var promise = new Promise((resolve,reject) => {
      this.taskRef2.update(taskKey, {
        priority: null
      })
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  getSelectedTaskItemsInfo(taskKey) {
    return this.afDatabase.object<TaskItems>('task/' + taskKey);
  }

  assignMemberToTask(taskKey, assignedMem: AssignedMember, memKey) {
    this.taskRef2 = this.afDatabase.list<AssignedMember>('task/' + taskKey + '/assignedMember');

    console.log(memKey);
    
    var promise = new Promise((resolve,reject) => {

      this.taskRef2.set(memKey, assignedMem)
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  getPoll(taskKey) {
    return this.afDatabase.object<Poll>('task/' + taskKey + '/poll');
  }

  removePoll(taskKey) {
    this.taskRef2 = this.afDatabase.list<Poll>('task/' + taskKey);
    var promise = new Promise((resolve, reject) => {
      this.taskRef2.remove('poll')
      .then( res => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getPollOptions(taskKey) {
    return this.afDatabase.list<PollOptions>('task/' + taskKey + '/poll/pollOptions');
  }

  getAssignedMemList(taskKey) {
    return this.afDatabase.list<AssignedMember>('task/' + taskKey + '/assignedMember');
  }

  getSelectedVoters(taskKey) {
    return this.afDatabase.object<Voters>('task/' + taskKey + '/poll/voters/' + this.afAuth.auth.currentUser.uid);
  }

  getVoterMultipleVotes(taskKey) {
    return this.afDatabase.list<Voters>('task/' + taskKey + '/poll/voters/' + this.afAuth.auth.currentUser.uid);
  }

  removeAssignedMemberToTask(taskKey, memKey) {
    this.taskRef2 = this.afDatabase.list<AssignedMember>('task/' + taskKey + '/assignedMember');

    var promise = new Promise((resolve,reject) => {
      this.taskRef2.remove(memKey)
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
    });

    return promise;
  }

  activityLogging(taskKey, activityLog: ActivityLog) {
    this.taskRef2 = this.afDatabase.list<ActivityLog>('task/' + taskKey + '/activityLog');

    var promise = new Promise((resolve, reject) => {
      this.taskRef2.push(activityLog)
      .then(res => {
        resolve(true);
      }, err => {
        reject(err);
      });
    });

    return promise;
  }

  getActivityLogging(taskKey) {
    return this.afDatabase.list<ActivityLog>('task/' + taskKey + '/activityLog');
  }
}

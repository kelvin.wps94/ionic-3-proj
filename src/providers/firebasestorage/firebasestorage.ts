import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';

/*
  Generated class for the FirebasestorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebasestorageProvider {

	firestore = firebase.storage();

  constructor(public afAuth: AngularFireAuth) {
    
  }

  uploadProfilePhoto(imgResult) {
  	var promise = new Promise((resolve, reject) => {
  		this.firestore.ref('profileImages').child(this.afAuth.auth.currentUser.uid)
      	.putString(imgResult, 'data_url')
      	.then( res => {
        	this.firestore.ref('profileImages').child(this.afAuth.auth.currentUser.uid).getDownloadURL()
        	.then( url => {
          		resolve(url);
          	})
          	.catch( err => {
            	reject(err);
          	});
        })
        .catch( err => {
        	reject(err);
        });
  	});

  	return promise;
  	
  } 

}

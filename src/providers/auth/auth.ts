import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserCredentials } from '../../models/users/users.model';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(private afAuth: AngularFireAuth) {
    
  }

  loggingIn(userCredentials: UserCredentials) {
  	var promise = new Promise((resolve, reject) => {
  		this.afAuth.auth.signInWithEmailAndPassword(userCredentials.email, userCredentials.password)
  		.then( res => {
  			resolve(true);
  		}).catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  loggingOut() {
    var promise = new Promise((resolve, reject) => {
      this.afAuth.auth.signOut()
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getCurrentUserID() {
    return this.afAuth.auth.currentUser.uid;
  }

  getCurrentUserName() {
    return this.afAuth.auth.currentUser.displayName;
  }

}

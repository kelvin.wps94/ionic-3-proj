import { Injectable } from '@angular/core';
import { SubTask } from '../../models/subtask/subtask.model';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the SubtaskProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SubtaskProvider {

	private subTaskRef = this.afDatabase.list<SubTask>('task');

  constructor(public afDatabase: AngularFireDatabase) {
    
  }

  addSubTask(taskKey, subTask: SubTask) {
  	this.subTaskRef = this.afDatabase.list<SubTask>('task/' + taskKey + '/subTask');

  	var promise = new Promise((resolve,reject) => {
  		console.log(JSON.stringify(subTask));
  		this.subTaskRef.push(subTask)
  		.then(res => {
  			resolve(true);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  getSubTaskList(taskKey) {
  	return this.afDatabase.list<SubTask>('task/' + taskKey + '/subTask');
  }

  updateSubTaskStatus(taskKey, subTask: SubTask) {
  	this.subTaskRef = this.afDatabase.list<SubTask>('task/' + taskKey + '/subTask');

  	var promise = new Promise((resolve,reject) => {

  		this.subTaskRef.update(subTask.key, {
  			isDone: subTask.isDone,
  			subTaskTitle: subTask.subTaskTitle
  		})
  		.then(res => {
  			resolve(true);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  deleteSelectedSubTask(taskKey, subTask: SubTask) {
  	this.subTaskRef = this.afDatabase.list<SubTask>('task/' + taskKey + '/subTask');

  	var promise = new Promise((resolve,reject) => {

  		this.subTaskRef.remove(subTask.key)
  		.then(res => {
  			resolve(true);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

}

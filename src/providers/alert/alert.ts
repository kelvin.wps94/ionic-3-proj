import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertProvider {

	constructor( private alertCtrl: AlertController){

	}

	showAlert(title, errMsg) {
	  	let alert = this.alertCtrl.create({
	  		title: title,
	  		subTitle: errMsg,
	  		buttons: ['OK']
	  	});
	  	alert.present();
	}
}

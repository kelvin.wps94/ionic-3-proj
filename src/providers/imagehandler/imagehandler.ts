import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';

/*
  Generated class for the ImagehandlerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ImagehandlerProvider {

	nativepath: any;
	firestore = firebase.storage();
	imgsource: any;

  constructor(public filepicker: IOSFilePicker, public afAuth: AngularFireAuth, public filechooser: FileChooser, public plt: Platform) {
    
  }

  /*uploadImage() {

    if(this.plt.is('android')) {

      var promise = new Promise((resolve, reject) => {
        this.filechooser.open()
        .then(url => {
          (<any>window).FilePath.resolveNativePath(url, (result) => {
            this.nativepath = result;
            (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
              res.file(resFile => {
                var reader = new FileReader();
                reader.readAsArrayBuffer(resFile);
                reader.onloadend = (evt: any) => {
                  var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                  var imageStore = this.firestore.ref('/profileImages').child(this.afAuth.auth.currentUser.uid);
                  imageStore.put(imgBlob)
                  .then(res => {
                    this.firestore.ref('/profileImages').child(this.afAuth.auth.currentUser.uid).getDownloadURL()
                    .then(url => {
                      resolve(url);
                    })
                    .catch(err => {
                      reject(err);
                    });
                  })
                  .catch(err => {
                    reject(err);
                  })
                }
              });
            });
          });
        });
      });

      return promise;

    } else if (this.plt.is('ios')) {

      var promise = new Promise((resolve, reject) => {
        this.filepicker.pickFile()
        .then(uri => {
          (<any>window).FilePath.resolveNativePath(uri, (result) => {
            this.nativepath = result;
            (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
              res.file(resFile => {
                var reader = new FileReader();
                reader.readAsArrayBuffer(resFile);
                reader.onloadend = (evt: any) => {
                  var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                  alert(imgBlob);
                  var imageStore = this.firestore.ref('/profileImages').child(this.afAuth.auth.currentUser.uid);
                  imageStore.put(imgBlob) 
                  .then(res => {

                    this.firestore.ref('/profileImages').child(this.afAuth.auth.currentUser.uid).getDownloadURL()
                    .then(uri => {
                      resolve(uri);
                    })
                    .catch(err => {
                      reject(err);
                    });
                  })
                  .catch(err => {
                    reject(err);
                  });
                }
              });
            });
          });
        });
      });

      return promise;
    }

  }*/

}

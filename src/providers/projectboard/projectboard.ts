import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { ProjectBoard } from '../../models/projectboard/projectboard.model';
import firebase from 'firebase';

/*
  Generated class for the ProjectboardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProjectboardProvider {

	private projBoardRef = this.afDatabase.list<ProjectBoard>('projectBoard');
  fireDB = firebase.database().ref('/');

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  addNewProjBoard(key, projBoard: ProjectBoard) {
  	this.projBoardRef = this.afDatabase.list<ProjectBoard>('projectBoard/' + key);

  	var promise = new Promise((resolve,reject) => {
  		this.projBoardRef.push(projBoard)
  		.then( res => {
  			resolve(res);
  		}, err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  deleteProjBoard(proj) {
    var promise = new Promise((resolve, reject) => {
      this.fireDB.update(proj)
      .then( res => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getProjectBoardList(key) {
  	return this.afDatabase.list<ProjectBoard>('projectBoard/' + key);
  }

  getSelectedBoardDetails(groupKey, boardKey) {
  	return this.afDatabase.object<ProjectBoard>('projectBoard/' + groupKey + '/' + boardKey);
  }

  updateHasTaskCompletedSection(groupKey, boardKey, projBoard: ProjectBoard) {
  	this.projBoardRef = this.afDatabase.list<ProjectBoard>('projectBoard/' + groupKey);

  	var promise = new Promise((resolve,reject) => {
  		this.projBoardRef.update(boardKey, projBoard)
  		.then( () => {
  			resolve(true)
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  updateTaskProgress(groupKey, boardKey, projBoard: ProjectBoard) {
    this.projBoardRef = this.afDatabase.list<ProjectBoard>('projectBoard/' + groupKey);

    var promise = new Promise((resolve,reject) => {
      this.projBoardRef.update(boardKey, projBoard)
      .then( () => {
        resolve(true)
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  renameProjectBoard(groupKey, projBoard: ProjectBoard) {
    this.projBoardRef = this.afDatabase.list<ProjectBoard>('projectBoard/' + groupKey);

    var promise = new Promise((resolve,reject) => {
      this.projBoardRef.update(projBoard.key, {
        boardName: projBoard.boardName,
        hasTaskCompleteSection: projBoard.hasTaskCompleteSection,
        totalCompletedTask: projBoard.totalCompletedTask,
        totalTask: projBoard.totalTask
      })
      .then( () => {
        resolve(true)
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }
}

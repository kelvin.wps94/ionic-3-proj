import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { ChatSingleMessages } from '../../models/chatsinglemessages/chatsinglemessages.model';
import { ChatGroupMessages } from '../../models/chatgroupmessages/chatgroupmessages.model';
import { GroupMember } from '../../models/groupmember/groupmember.model';
import { SingleChatLastMsg } from '../../models/singlechatlastmsg/singlechatlastmsg.model';
import firebase from 'firebase';
/*
  Generated class for the ChatmsgProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChatmsgProvider {

  private notificationRef = this.afDatabase.list('notifications');
	private chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages');
  private chatGroupMsgListRef = this.afDatabase.list<ChatGroupMessages>('chatGroupMessages');
	private chatContactListRef;
	private chatMessagesRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages');
  private chatContactListRef2 = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages');
  private clearSelectedChatMsgRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages');
  firestore = firebase.storage();
  fireDB = firebase.database().ref('/');

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  updateGroupChatLastMsg(key, msg, date, type) {
    var promise = new Promise((resolve, reject) => {
      this.afDatabase.list('chatGroupMessages')
      .update(key, {
        lastMessages: msg,
        lastMsgDate: date,
        lastMsgType: type
      })
      .then( res => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  updateSingleChatLastMsg(key, object: SingleChatLastMsg) {
    var promise = new Promise((resolve, reject) => {
      this.afDatabase.list('chatSingleMessages/' + this.afAuth.auth.currentUser.uid)
      .update(key, object)
      .then( res => {
        this.afDatabase.list('chatSingleMessages/' + key)
        .update(this.afAuth.auth.currentUser.uid, object)
        .then( res => {
          resolve(true);
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  sendChatSingleMessages(chatSingleMsg: ChatSingleMessages, key) {

  	chatSingleMsg.sender = this.afAuth.auth.currentUser.uid;

  	this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);

    var msgKey;

  	msgKey = this.chatSingleMsgListRef.push(chatSingleMsg).key;
  	
  	this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + key + '/' + this.afAuth.auth.currentUser.uid);

  	this.chatSingleMsgListRef.set(msgKey, chatSingleMsg)
    .then( () => {
      this.notificationRef = this.afDatabase.list('notifications/' + key);
      this.notificationRef.push({
        from: this.afAuth.auth.currentUser.uid,
        type: 'chatSingle'
      });
    });

  	return this.chatSingleMsgListRef;
  }

  sendChatGroupMessages(chatGroupMsg: ChatGroupMessages, key) {
    chatGroupMsg.sender = this.afAuth.auth.currentUser.uid;

    this.chatGroupMsgListRef = this.afDatabase.list<ChatGroupMessages>('chatGroupMessages/' + key);

    return this.chatGroupMsgListRef.push(chatGroupMsg).key;

  }

  sendGroupNotifications(key) {

    var promise = new Promise((resolve, reject) => {
      this.fireDB.update(key)
      .then( res => {
        console.log(res);
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;

  }

  sendChatSingleMessagesWithImageFromCamera(chatSingleMsg: ChatSingleMessages, key, imgResult) {

    var promise = new Promise((resolve, reject) => {
      chatSingleMsg.sender = this.afAuth.auth.currentUser.uid;

      this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);

      var msgKey;

      msgKey = this.chatSingleMsgListRef.push(chatSingleMsg).key;
      
      this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + key + '/' + this.afAuth.auth.currentUser.uid);

      this.chatSingleMsgListRef.set(msgKey, chatSingleMsg);

      this.firestore.ref('chatImages/single/' + this.afAuth.auth.currentUser.uid + key).child(msgKey)
      .putString(imgResult, 'data_url')
      .then( res => {
        this.firestore.ref('chatImages/single/' + this.afAuth.auth.currentUser.uid + key).child(msgKey).getDownloadURL()
        .then( url => {
          chatSingleMsg.message = url;
          this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);
          this.chatSingleMsgListRef.update(msgKey, chatSingleMsg)
          .then( () => {
            this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + key + '/' + this.afAuth.auth.currentUser.uid)
            this.chatSingleMsgListRef.update(msgKey, chatSingleMsg)
            .then( () => {
              resolve(true);
            })
            .catch( err => {
              reject(err);
            });
          })
          .catch( err => {
            reject(err);
          });
        })
        .catch( err => {
          reject(err);
        });
      });
    });

    return promise;
  }

  sendChatGroupMessagesWithImage(chatGroupMsg: ChatGroupMessages, key, imgResult) {
    var promise = new Promise((resolve, reject) => {

      chatGroupMsg.sender = this.afAuth.auth.currentUser.uid;

      this.chatGroupMsgListRef = this.afDatabase.list<ChatGroupMessages>('chatGroupMessages/' + key);

      var msgKey;

      msgKey = this.chatGroupMsgListRef.push(chatGroupMsg).key;

      this.firestore.ref('chatImages/group/' + key).child(msgKey)
      .putString(imgResult, 'data_url')
      .then( res => {
        this.firestore.ref('chatImages/group/' + key).child(msgKey).getDownloadURL()
        .then( url => {
          chatGroupMsg.message = url;
          this.chatGroupMsgListRef = this.afDatabase.list<ChatGroupMessages>('chatGroupMessages/' + key);
          this.chatGroupMsgListRef.update(msgKey, chatGroupMsg)
          .then( () => {
            resolve(msgKey);
          })
          .catch( err => {
            reject(err);
          });
        })
        .catch( err => {
          reject(err);
        });
      });

    });

    return promise;
  }

  sendChatSingleMessagesToGetMsgKey (chatSingleMsg: ChatSingleMessages, key) {
    chatSingleMsg.sender = this.afAuth.auth.currentUser.uid;

    this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);

    var msgKey;

    msgKey = this.chatSingleMsgListRef.push(chatSingleMsg).key;
      
    this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + key + '/' + this.afAuth.auth.currentUser.uid);

    this.chatSingleMsgListRef.set(msgKey, chatSingleMsg);

    return msgKey;

  }

  updateChatSingleMessagesFromGalleryImagesMsg (chatSingleMsg: ChatSingleMessages, key, msgKey) {
    var promise = new Promise((resolve, reject) => {
      this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);
      this.chatSingleMsgListRef.update(msgKey, chatSingleMsg)
      .then( () => {
        this.chatSingleMsgListRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + key + '/' + this.afAuth.auth.currentUser.uid)
        this.chatSingleMsgListRef.update(msgKey, chatSingleMsg)
        .then( () => {
          resolve(true);
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getChatContactList() {
  	this.chatContactListRef = this.afDatabase.list('chatSingleMessages/' + this.afAuth.auth.currentUser.uid)
    .snapshotChanges()
    .map(changes =>
      changes.map(c => ({
        key: c.payload.key,
        chatContactList: this.afDatabase.object('users/' + c.payload.key).valueChanges(),
        ...c.payload.val()
      }))
    );

    return this.chatContactListRef;
  }

  getChatList() {
    this.chatContactListRef2 = this.afDatabase.list('chatSingleMessages/' + this.afAuth.auth.currentUser.uid);

    return this.chatContactListRef2;
  }

  getGroupChatMessages(key) {
    this.chatGroupMsgListRef = this.afDatabase.list<ChatGroupMessages>('chatGroupMessages/' + key);
    return this.chatGroupMsgListRef;
  }

  getAllGroupChatMessages() {
    return this.afDatabase.list('chatGroupMessages');
  }

  getChatMessages(key) {
  	this.chatMessagesRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid + '/' + key);
	  return this.chatMessagesRef;
  }

  clearChatMessages(key) {
    this.clearSelectedChatMsgRef = this.afDatabase.list<ChatSingleMessages>('chatSingleMessages/' + this.afAuth.auth.currentUser.uid);

    var promise = new Promise((resolve,reject) => {
      this.clearSelectedChatMsgRef.remove(key)
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

}

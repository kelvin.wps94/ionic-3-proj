import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Friends } from '../../models/friends/friends.model';

/*
  Generated class for the FriendsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FriendsProvider {

	private friendsListRef;
	private selectedFriendListRef = this.afDatabase.list<Friends>('friends');
  private friendListRef2 = this.afDatabase.list<Friends>('friends');

  constructor(private afDatabase:AngularFireDatabase, private afAuth:AngularFireAuth) {
    
  }

  getAllFriendList() {
  	this.friendsListRef = this.afDatabase.list('friends/' + this.afAuth.auth.currentUser.uid)
    .snapshotChanges()
    .map(changes =>
      changes.map(c => ({
        key: c.payload.key,
        frenList: this.afDatabase.object('users/' + c.payload.key).valueChanges(),
        ...c.payload.val()
      }))
    );

    return this.friendsListRef;
  }

  getFriendList() {
    this.friendListRef2 = this.afDatabase.list<Friends>('friends/' + this.afAuth.auth.currentUser.uid);

    return this.friendListRef2;
  }

  getSelectedFriends(key) {
    return this.afDatabase.list<Friends>('friends/' + this.afAuth.auth.currentUser.uid + '/' + key);
  }

  unfriendSelectedPerson(key) {
  	this.selectedFriendListRef = this.afDatabase.list<Friends>('friends/' + this.afAuth.auth.currentUser.uid);
  	var promise = new Promise((resolve, reject) => {
  		this.selectedFriendListRef.remove(key)
  		.then( () => {
  			this.selectedFriendListRef = this.afDatabase.list<Friends>('friends/' + key);
  			this.selectedFriendListRef.remove(this.afAuth.auth.currentUser.uid)
  			.then( () => {
  				resolve(true);
  			})
  			.catch( err => {
  				reject(err);
  			});
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

}

import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
/*
  Generated class for the CameraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CameraProvider {


  constructor(private camera: Camera) {
  }

  async triggerCamera(sourceType: number) {
  	try {
	  	const options: CameraOptions = {
		  quality: 80,
		  destinationType: this.camera.DestinationType.DATA_URL,
		  encodingType: this.camera.EncodingType.JPEG,
		  mediaType: this.camera.MediaType.PICTURE,
		  correctOrientation: true,
		  sourceType: sourceType
		}

		const result = await this.camera.getPicture(options);

		const image: any = `data:image/jpeg;base64,${result}`;

		return image;
	}
	catch(e) {
		console.error(e);
	}
  }

}

import { Injectable } from '@angular/core';

/*
  Generated class for the DatetimeinstringProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatetimeinstringProvider {

  constructor() {
    
  }

  getMonth(value): string {
    if(value == 0) {
      return 'Jan';
    } else if(value == 1) {
      return 'Feb';
    } else if(value == 2) {
      return 'Mar';
    } else if(value == 3) {
      return 'Apr';
    } else if(value == 4) {
      return 'May';
    } else if(value == 5) {
      return 'Jun';
    } else if(value == 6) {
      return 'Jul';
    } else if(value == 7) {
      return 'Aug';
    } else if(value == 8) {
      return 'Sep';
    } else if(value == 9) {
      return 'Oct';
    } else if(value == 10) {
      return 'Nov';
    } else if(value == 11) {
      return 'Dec';
    }
  }

  getDay(value): string {
    if(value == 0) {
      return 'Sun';
    } else if(value == 1) {
      return 'Mon';
    } else if(value == 2) {
      return 'Tue';
    } else if(value == 3) {
      return 'Wed';
    } else if(value == 4) {
      return 'Thu';
    } else if(value == 5) {
      return 'Fri';
    } else if(value == 6) {
      return 'Sat';
    }
  }

  getTime(hours, mins): string {
    var t = '';
    var hour = '';
    var min = mins.toString();
    var calculated;
    var zero = '';
    var zeroMin = '';

    if(mins < 10) {
    	zeroMin = '0'
    }


    if(hours > 11) {
      t = 'PM';

      if(hours > 12) {
        hour = (hours - 12).toString();
        calculated = hours - 12;

        if(calculated > 0 && calculated < 10) {
          zero = '0';
        } else {
          zero = '';
        }
      }
    } else {
      t = 'AM';

      if(hours == 0) {
        hour = '12';
      } else {
        hour = hours.toString();
      }

      if(hours > 0 && hours < 10) {
        zero = '0';
      } else {
        zero = '';
      }
    }

    return zero + hour + ':' + zeroMin + min + ' ' + t;
  }

}

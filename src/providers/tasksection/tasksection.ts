import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { TaskSection } from '../../models/tasksection/tasksection.model';

/*
  Generated class for the TasksectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksectionProvider {

	private taskSectionRef = this.afDatabase.list<TaskSection>('taskSection');
	private taskSectNTaskListRef;

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  addNewTaskSection(projBoardKey, taskSect: TaskSection) {

  	this.taskSectionRef = this.afDatabase.list<TaskSection>('taskSection/' + projBoardKey);

  	var promise = new Promise((resolve,reject) => {
  		this.taskSectionRef.push(taskSect)
  		.then( res => {
  			resolve(true);
  		},  err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  getTaskSectionList(projBoardKey) {
  	return this.afDatabase.list<TaskSection>('taskSection/' + projBoardKey);
  }

  getTaskSectionAndTaskList(projBoardKey) {
  	return this.afDatabase.list('taskSection/' + projBoardKey);
  }

  getSelectedTaskSectionDetails(projBoardKey, taskSectKey) {
  	return this.afDatabase.object<TaskSection>('taskSection/' + projBoardKey + '/' + taskSectKey);
  }

  removeTaskSection(projBoardKey, taskSectKey) {
    this.taskSectionRef = this.afDatabase.list<TaskSection>('taskSection/' + projBoardKey);

    var promise = new Promise((resolve, reject) => {
      this.taskSectionRef.remove(taskSectKey)
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  renameTaskSection(projBoardKey, taskSectKey, taskSect: TaskSection) {
    this.taskSectionRef = this.afDatabase.list<TaskSection>('taskSection/' + projBoardKey);

    var promise = new Promise((resolve, reject) => {
      this.taskSectionRef.update(taskSectKey, {
        taskSectionTitle: taskSect.taskSectionTitle,
        isTaskCompletedSection: taskSect.isTaskCompletedSection
      })
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

}

import { Injectable } from '@angular/core';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';
import { UserCredentials } from '../../models/users/users.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Friends } from '../../models/friends/friends.model';
import firebase from 'firebase';

/*
  Generated class for the FriendreqProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FriendreqProvider {

  private usersListRef;
  private sentReqListRef = this.afDatabase.list<FriendRequest>('sentFriendRequest');
  private recReqListRef = this.afDatabase.list<FriendRequest>('receiveFriendRequest');
  private friendRequestListRef = this.afDatabase.list<FriendRequest>('friendRequest');
  private requestListRef = this.afDatabase.list<FriendRequest>('friendRequest');
  private friendListRef = this.afDatabase.list<Friends>('friends');
  private notifRef = this.afDatabase.list('notifications');

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  submitFriendReq(key, newFriendRequest: FriendRequest) {
    newFriendRequest.type = 'sent';
    this.friendRequestListRef = this.afDatabase.list<FriendRequest>('sentFriendRequest/' + this.afAuth.auth.currentUser.uid)
    var promise = new Promise((resolve,reject) => {
      this.friendRequestListRef.set(key, newFriendRequest)
      .then( () => {
        newFriendRequest.type = 'receive';
        this.friendRequestListRef = this.afDatabase.list<FriendRequest>('receiveFriendRequest/' + key)
        this.friendRequestListRef.set(this.afAuth.auth.currentUser.uid, newFriendRequest)
        .then( () => {
          this.notifRef = this.afDatabase.list('notifications/' + key);
          this.notifRef.push({
            from: this.afAuth.auth.currentUser.uid,
            type: 'friendRequest'
          })
          .then( () => {
            resolve(true);
          });
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getAllFriendRequest() {
    this.usersListRef = this.afDatabase.list('receiveFriendRequest/' + this.afAuth.auth.currentUser.uid)
    .snapshotChanges()
    .map(changes =>
      changes.map(c => ({
        key: c.payload.key,
        friendReqList: this.afDatabase.object('users/' + c.payload.key).valueChanges(),
        ...c.payload.val()
      }))
    );

    return this.usersListRef;
  }

  getAllSentRequest() {
    this.usersListRef = this.afDatabase.list('sentFriendRequest/' + this.afAuth.auth.currentUser.uid)
    .snapshotChanges()
    .map(changes =>
      changes.map(c => ({
        key: c.payload.key,
        sentRequestList: this.afDatabase.object('users/' + c.payload.key).valueChanges(),
        ...c.payload.val()
      }))
    );

    return this.usersListRef;
  }

  getSentRequestListKey() {
    this.sentReqListRef = this.afDatabase.list<FriendRequest>('sentFriendRequest/' + this.afAuth.auth.currentUser.uid);

    return this.sentReqListRef;
  }

  getRecRequestListKey() {
    this.recReqListRef = this.afDatabase.list<FriendRequest>('receiveFriendRequest/' + this.afAuth.auth.currentUser.uid);

    return this.recReqListRef;
  }

  acceptFriendRequest(key, friend: Friends) {
    friend.isFriend = true;
    this.friendListRef = this.afDatabase.list<Friends>('friends/' + this.afAuth.auth.currentUser.uid);
    var promise = new Promise((resolve, reject) => {
      this.friendListRef.set(key, friend)
      .then( () => {
        this.friendListRef = this.afDatabase.list<Friends>('friends/' + key);
        this.friendListRef.set(this.afAuth.auth.currentUser.uid, friend)
        .then( () => {
          this.notifRef = this.afDatabase.list('notifications/' + key);
          this.notifRef.push({
            from: this.afAuth.auth.currentUser.uid,
            type: 'friends'
          })
          .then( () => {
            resolve(true);
          });
        })
        .catch(err => {
          reject(err);
        })
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  cancelSentFriendRequest(key) {
    this.friendRequestListRef = this.afDatabase.list<FriendRequest>('sentFriendRequest/' + this.afAuth.auth.currentUser.uid);
    var promise = new Promise((resolve, reject) => {
      this.friendRequestListRef.remove(key)
      .then( () => {
        this.friendRequestListRef = this.afDatabase.list<FriendRequest>('receiveFriendRequest/' + key);
        this.friendRequestListRef.remove(this.afAuth.auth.currentUser.uid)
        .then( () => {
          resolve(true);
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  cancelReceiveFriendRequest(key) {
    this.friendRequestListRef = this.afDatabase.list<FriendRequest>('receiveFriendRequest/' + this.afAuth.auth.currentUser.uid);
    var promise = new Promise((resolve, reject) => {
      this.friendRequestListRef.remove(key)
      .then( () => {
        this.friendRequestListRef = this.afDatabase.list<FriendRequest>('sentFriendRequest/' + key);
        this.friendRequestListRef.remove(this.afAuth.auth.currentUser.uid)
        .then( () => {
          resolve(true);
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  getSelectedSentRequest(key) {
    return this.afDatabase.list<FriendRequest>('sentFriendRequest/' + this.afAuth.auth.currentUser.uid + '/' + key);
  }

  getSelectedRecRequest(key) {
    return this.afDatabase.list<FriendRequest>('receiveFriendRequest/' + this.afAuth.auth.currentUser.uid + '/' + key);
  }

}

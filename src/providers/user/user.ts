import { Injectable } from '@angular/core';
import { UserCredentials } from '../../models/users/users.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

	private userListRef = this.afDatabase.list<UserCredentials>('users');
  private filteredUserListRef;

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  addUser(newUser: UserCredentials) {
  	var promise = new Promise( (resolve, reject) => {
  		this.afAuth.auth.createUserWithEmailAndPassword(newUser.email, newUser.password)
  		.then( () => {
  			this.afAuth.auth.currentUser.updateProfile({
  				displayName: newUser.displayName,
  				photoURL: ''
  			})
  			.then( () => {
  				
  				this.userListRef.set(this.afAuth.auth.currentUser.uid, newUser)
  				.then( () => {
  					resolve(true);
  				})
          .catch( err => {
            reject(err);
          });

  			})
  			.catch( err => {
  				reject(err);
  			});
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  passwordReset(email) {
  	var promise = new Promise((resolve, reject) => {
  		this.afAuth.auth.sendPasswordResetEmail(email)
	  	.then( () => {
	  		resolve(true);
	  	})
	  	.catch( err => {
	  		reject(err);
	  	});
  	})

  	return promise;
  	
  }

  cleanDeviceTokenLogout() {
    var promise = new Promise((resolve, reject) => {
      this.afDatabase.list('users')
      .update(this.afAuth.auth.currentUser.uid, {token: ''})
      .then( res => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  updateimage(imageurl, newUser: UserCredentials) {
  	var promise = new Promise((resolve, reject) => {
  		this.afAuth.auth.currentUser.updateProfile({
  			displayName: this.afAuth.auth.currentUser.displayName,
  			photoURL: imageurl
  		})
  		.then( () => {
        newUser.photoURL = imageurl;
  			this.userListRef.update(this.afAuth.auth.currentUser.uid, newUser)
  			.then( () => {
  				resolve(true);
  			})
  			.catch( err => {
  				reject(err);
  			});
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  getUserDetails() {
    return this.afDatabase.object<UserCredentials>('users/' + this.afAuth.auth.currentUser.uid);
  }

  getAllUser() {
    return this.userListRef;
  }

  getSelectedUsersDetails(key) {
    return this.afDatabase.object<UserCredentials>('users/' + key);
  }

  updateDisplayName(users: UserCredentials) {
    var promise = new Promise((resolve, reject) => {
      this.afAuth.auth.currentUser.updateProfile({
        displayName: users.displayName,
        photoURL: users.photoURL
      })
      .then( () => {
        this.userListRef.update(this.afAuth.auth.currentUser.uid, users)
        .then( () => {
          resolve(true);
        })
        .catch( err => {
          reject(err);
        });
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  updateUserStatus(users: UserCredentials) {
    var promise = new Promise((resolve, reject) => {
      this.userListRef.update(this.afAuth.auth.currentUser.uid, users)
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }

  testing() {
    this.filteredUserListRef = this.afDatabase.list('users');

    return this.filteredUserListRef;
  }



}

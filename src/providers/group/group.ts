import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Group } from '../../models/group/group.model';
import { UserGroup } from '../../models/usergroup/usergroup.model';
import { GroupMember } from '../../models/groupmember/groupmember.model';
import firebase from 'firebase';

/*
  Generated class for the GroupProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GroupProvider {

	private groupListRef;
	private groupMemberListRef;
	private groupRef = this.afDatabase.list<Group>('group');
	private usersGroupRef = this.afDatabase.list('users');
	private groupMember = this.afDatabase.list('groupMember');
	private selectedGroupRef = this.afDatabase.object<Group>('group');
	private groupListRef2 = this.afDatabase.list('users');
	private allGroupRef = this.afDatabase.list<Group>('group');
	private groupMemberKeyRef = this.afDatabase.list<GroupMember>('groupMember');
	fireDB = firebase.database().ref('/');
	firestore = firebase.storage();

	userGroup: UserGroup = {
		hasThisGroup: true
	}

	groupMem: GroupMember = {
		isMember: true
	}

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
    
  }

  addNewGroup(group: Group) {
  	let groupPushKey;

  	var promise = new Promise((resolve, reject) => {
  		groupPushKey = this.groupRef.push(group).key;

      this.usersGroupRef = this.afDatabase.list('users/' + this.afAuth.auth.currentUser.uid + '/joinedGroup');
  		this.usersGroupRef.set(groupPushKey, this.userGroup)
  		.then( () => {
  			this.groupMember = this.afDatabase.list('groupMember/' + groupPushKey);
  			this.groupMember.set(this.afAuth.auth.currentUser.uid, this.groupMem)
  			.then( () => {
  				resolve(groupPushKey);
  			})
  			.catch( err => {
  				reject(err);
  			});
  		})
  		.catch( err => {
  			reject(err);
  		});

  	})
 	
 	  return promise;
  }

  getAllGroupList() {
  	this.groupListRef = this.afDatabase.list('users/' + this.afAuth.auth.currentUser.uid + '/joinedGroup')
  	.snapshotChanges()
  	.map(changes =>
  		changes.map(c => ({
  			key: c.payload.key,
  			groupList: this.afDatabase.object('group/' + c.payload.key).valueChanges(),
  			...c.payload.val()
  		}))
  	);

  	return this.groupListRef;
  }

  getJoinedGroupList() {
  	this.groupListRef2 = this.afDatabase.list('users/' + this.afAuth.auth.currentUser.uid + '/joinedGroup');

  	return this.groupListRef2;
  }

  getSelectedJoinGroup(key) {
    this.groupListRef2 = this.afDatabase.list('users/' + this.afAuth.auth.currentUser.uid + '/joinedGroup/' + key);

    return this.groupListRef2;
  }

  getGroupList() {
  	return this.allGroupRef;
  }

  getSelectedGroupDetails(key) {
  	return this.selectedGroupRef = this.afDatabase.object<Group>('group/' + key);
  }

  getGroupMembersList(key) {
  	this.groupMemberListRef = this.afDatabase.list('groupMember/' + key)
  	.snapshotChanges()
  	.map(changes =>
      changes.map(c => ({
        key: c.payload.key,
        groupMemList: this.afDatabase.object('users/' + c.payload.key).valueChanges(),
        ...c.payload.val()
      }))
    );

    return this.groupMemberListRef;
  }

  getGroupMembersKeyList(key) {
  	this.groupMemberKeyRef = this.afDatabase.list<GroupMember>('groupMember/' + key);

  	return this.groupMemberKeyRef;
  }

  addNewMember(newMember) {
  	var promise = new Promise((resolve, reject) => {
  		this.fireDB.update(newMember)
  		.then( res => {
  			resolve(true);
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});
  	
  	return promise;
  }

  removeSelectedUserFromGroup(key, selectedGroupID) {
  	this.groupMember = this.afDatabase.list<GroupMember>('groupMember/' + selectedGroupID);
  	var promise = new Promise((resolve, reject) => {
  		this.groupMember.remove(key)
  		.then( () => {
  			this.usersGroupRef = this.afDatabase.list<UserGroup>('users/' + key + '/joinedGroup');
  			this.usersGroupRef.remove(selectedGroupID)
  			.then( () => {
  				resolve(true);
  			})
  			.catch( err => {
  				reject(err);
  			})
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});

  	return promise;
  }

  updateGroupProfileImages(imgResult, group: Group, key) {

    var promise = new Promise((resolve, reject) => {
      this.firestore.ref('group_profileImages').child(key)
      .putString(imgResult, 'data_url')
      .then( res => {
        this.firestore.ref('group_profileImages').child(key).getDownloadURL()
        .then( url => {
          group.groupPhotoURL = url;
          this.groupRef.update(key, group)
          .then( () => {
            resolve(true);
          })
          .catch( err => {
            reject(err);
          });
        })
        .catch( err => {
          reject(err);
        });
      });
    });

    return promise;
  }

  leaveDeleteSelectedGroup(selectedGroup) {
  	var promise = new Promise((resolve, reject) => {
  		this.fireDB.update(selectedGroup)
  		.then( () => {
  			resolve(true);
  		})
  		.catch( err => {
  			reject(err);
  		});
  	});
  	
  	return promise;
  }

  updateGroupName(group: Group, key) {
    var promise = new Promise((resolve, reject) => {
      this.groupRef.update(key, group)
      .then( () => {
        resolve(true);
      })
      .catch( err => {
        reject(err);
      });
    });

    return promise;
  }
}

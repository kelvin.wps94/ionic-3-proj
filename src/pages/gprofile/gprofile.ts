import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ActionSheetController, AlertController, ModalController } from 'ionic-angular';
import { ChatgrouppagePage } from '../chatgrouppage/chatgrouppage';
import { GroupProvider } from '../../providers/group/group';
import { Group } from '../../models/group/group.model';
import { UserCredentials } from '../../models/users/users.model';
import { Observable } from 'rxjs/Observable';
import { SelectedprofilePage } from '../selectedprofile/selectedprofile';
import { AuthProvider } from '../../providers/auth/auth';
import { ToastProvider } from '../../providers/toast/toast';
import { AlertProvider } from '../../providers/alert/alert';
import { GrouplistPage } from '../grouplist/grouplist';
import { CameraProvider } from '../../providers/camera/camera';
import { ProjectBoard } from '../../models/projectboard/projectboard.model';
import { ProjectboardProvider } from '../../providers/projectboard/projectboard';
import { AddmemtogroupPage } from '../addmemtogroup/addmemtogroup';
import { TasksectionProvider } from '../../providers/tasksection/tasksection';
import { SelectedprojPage } from '../selectedproj/selectedproj';
/**
 * Generated class for the GprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gprofile',
  templateUrl: 'gprofile.html',
})
export class GprofilePage {
	@ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;

	SwipedTabsIndicator :any= null;
	tabs:any=[];
	selectedGroupID: string = '';
	activeTabs: number = 0;

	group = {} as Group;
	adminID: string = '';
	currentUserId: string = '';
	admin: boolean = false;
	groupMemberList$: Observable<UserCredentials[]>;
	imgUrl: any;

  projBoardList$: Observable<ProjectBoard[]>;

	projBoard: ProjectBoard = {
	    boardName: '',
	    hasTaskCompleteSection: false,
	    totalTask: 0,
	    totalCompletedTask: 0
	}

  projectCount: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private groupProvider: GroupProvider, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController, private authProvider: AuthProvider, private toast: ToastProvider, private alert: AlertProvider, private cameraProvider: CameraProvider, private projBoardProvider: ProjectboardProvider, public modal: ModalController, private tasksecProvider: TasksectionProvider) {
  	this.selectedGroupID = this.navParams.get('key');
  	this.currentUserId = this.authProvider.getCurrentUserID();
  	this.tabs=["Member","Project"];

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupID)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {
	  		this.group = snap;
        
        	this.adminID = snap.admin;

	  		if(snap.admin == this.currentUserId) {
	  			this.admin = true;
	  		} else {
	  			this.admin = false;
	  		}
	  	} else {
	  		this.navCtrl.setRoot(GrouplistPage);
	  	}
  	});

  	this.groupMemberList$ = this.groupProvider.getGroupMembersList(this.selectedGroupID);
  
    this.projBoardList$ = this.projBoardProvider.getProjectBoardList(this.selectedGroupID)
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.projBoardProvider.getProjectBoardList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
      this.projectCount = snap.length;
    });
  }

  ionViewDidEnter() {
    this.SwipedTabsIndicator = document.getElementById("indicator");
    this.activeTabs = this.SwipedTabsSlider.getActiveIndex();
    this.SwipedTabsSlider.lockSwipes(true);
  }

  selectTab(index) {    
  	this.SwipedTabsSlider.lockSwipes(false);
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
    this.SwipedTabsSlider.slideTo(index, 500);
    this.SwipedTabsSlider.lockSwipes(true);
  }

  updateIndicatorPosition() {

  	if( this.SwipedTabsSlider.length()> this.SwipedTabsSlider.getActiveIndex()) {
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';

  		this.activeTabs = this.SwipedTabsSlider.getActiveIndex();
  	}
    
  }

  animateIndicator($event) {
  	if(this.SwipedTabsIndicator)
   	    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
  }

  backBtn() {
  	this.navCtrl.pop();
  }

  groupChat() {
  	this.navCtrl.push(ChatgrouppagePage, {key: this.selectedGroupID});
  }

  viewSelectedProfile(key) {
  	this.navCtrl.push(SelectedprofilePage, {
  		key: key
  	});
  }

  changeGroupName() {
    const prompt = this.alertCtrl.create({
      title: 'Change Group Name',
      message: "Enter a new name for this group.",
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Group Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Change',
          handler: data => {
            
            if(data.GroupName != '') {
            	this.group.groupName = data.groupName;

            	this.groupProvider.updateGroupName(this.group, this.selectedGroupID)
            	.then( res => {
            		this.toast.showToast('Group name has been changed.');
            	})
            	.catch( err => {
            		this.alert.showAlert('Change Group Name', err);
            	});

            } else {
            	this.alert.showAlert('Change Group Name', 'Group name cannot be empty!')
            }
          }
        }
      ]
    });
    prompt.present();
  }

  settings(admin) {
  	const adminActionSheet = this.actionSheetCtrl.create({
      title: 'Group Setting',
      buttons: [
        {
          text: 'Change Photo',
          icon: 'images',
          handler: () => {
            this.changePhotoOptions();
          }
        },{
          text: 'Change Group Name',
          icon: 'create',
          handler: () => {
            this.changeGroupName();
          }
        },{
          text: 'Delete Group',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            this.deleteGroup();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    const actionSheet = this.actionSheetCtrl.create({
      title: 'Group Setting',
      buttons: [
        {
          text: 'Change Photo',
          icon: 'images',
          handler: () => {
            this.changePhotoOptions();
          }
        },{
          text: 'Change Group Name',
          icon: 'create',
          handler: () => {
            this.changeGroupName();
          }
        },{
          text: 'Leave Group',
          icon: 'exit',
          role: 'destructive',
          handler: () => {
            this.leaveGroup();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

  	if(admin) {
  		adminActionSheet.present();
  	} else {
  		actionSheet.present();
  	}
    
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.updateGroupPhoto(this.imgUrl);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.updateGroupPhoto(this.imgUrl);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  openAddMemberModal() {
  	
  	const addMemModal = this.modal.create(AddmemtogroupPage, {
    	key: this.selectedGroupID
  	});

  	addMemModal.present();
  }

  showConfirm(group, option: number) {

  	var title = '';
  	var message = '';
  	var choices = '';

  	if(option == 1) {
  		title = 'Delete Group?';
  		message = 'Are you sure you want to delete this group?';
  		choices = 'Delete';
  	} else {
  		title = 'Leave Group?';
  		message = 'Are you sure you want to leave this group?';
  		choices = 'Leave';
  	}

    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: choices,
          handler: () => {
            

            switch (option) {
            	case 1:
            		
            		this.groupProvider.leaveDeleteSelectedGroup(group)
    				    .then( res => {
    				    	this.toast.showToast('You have successfully deleted the group!');
    				    	this.navCtrl.setRoot(GrouplistPage);
    				    })
    				    .catch( err => {
    				    	
    				    	this.alert.showAlert('Delete Group', 'Something went wrong, please try again later!');
    				    });
            		break;

            	case 2:
            		
            		this.groupProvider.leaveDeleteSelectedGroup(group)
    				    .then( res => {
    				    	
    				    	this.toast.showToast('You have successfully left the group!');
    				    	this.navCtrl.setRoot(GrouplistPage);
    				    })
    				    .catch( err => {
    				    	
    				    	this.alert.showAlert('Leave Group', 'Something went wrong, please try again later!');
    				    });
            		break;
            	
            	default:
            		
            		break;
            }
          }
        }
      ]
    });
    confirm.present();
  }

  deleteGroup() {
  	var group = {};

  	group['group/' + this.selectedGroupID ] = {}

    this.groupProvider.getGroupMembersKeyList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
    	snap.forEach(elem => {
    		
    		group['groupMember/' + this.selectedGroupID + '/' + elem.payload.key ] = {}
    		group['users/' + elem.payload.key + '/joinedGroup/' + this.selectedGroupID ] = {}
        group['chatGroupMessages/' + this.selectedGroupID ] = {}
    	});
    });

    this.showConfirm(group, 1);
  }

  leaveGroup() {
  	var group = {};

  	group['users/' + this.authProvider.getCurrentUserID() + '/joinedGroup/' + this.selectedGroupID ] = {}
  	group['groupMember/' + this.selectedGroupID + '/' + this.authProvider.getCurrentUserID() ] = {} 

  	this.showConfirm(group, 2);
  }

  changePhotoOptions() {
  	this.presentActionSheet();
  }

  updateGroupPhoto(imgResult) {
  	this.groupProvider.updateGroupProfileImages(imgResult, this.group, this.selectedGroupID)
  	.then( res => {
  		console.log(res);
  	})
  	.catch( err => {
  		console.log(err);
  	});
  }

  addNewProjectBoard() {
  	let prompt = this.alertCtrl.create({
      title: 'New Project Board',
      message: "Enter a name for the new project board",
      inputs: [
        {
          name: 'boardName',
          placeholder: 'Project Board Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Add',
          handler: data => {
            
            if(data.boardName != '') {
              this.projBoard.boardName = data.boardName;

              this.projBoardProvider.addNewProjBoard(this.selectedGroupID, this.projBoard)
              .then( res => {
                this.toast.showToast("New Board '" + this.projBoard.boardName + "' has been added.");
              })
              .catch( err => {
                this.alert.showAlert('New Project Board', 'Something went wrong. Please try again later!');
              });
            } else {
              this.alert.showAlert('New Project Board', "Project Board Name Cannot Be Empty. <br>Please Try again.");
            }
          }
        }
      ]
    });
    prompt.present();
  }

  removeSelectedMember(key, selectedUserName) {

  	const confirm = this.alertCtrl.create({
      title: 'Remove from group?',
      message: 'Are you sure you want to remove '+ selectedUserName +' from group?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Remove',
          handler: () => {
            this.groupProvider.removeSelectedUserFromGroup(key, this.selectedGroupID)
		  	.then( res => {
		  		this.toast.showToast(selectedUserName + ' has been removed from the group.');
		  	})
		  	.catch( err => {
		  		this.alert.showAlert('Remove Member', 'Something went wrong, please try again later!');
		  		console.log(err);
		  	});
          }
        }
      ]
    });
    confirm.present();
  }

  projectProgressPercentage(totalCompletedTask, totalTask) {
    var percentage = 0;

    percentage = (totalCompletedTask / totalTask) * 100;

    if(String(percentage) === "NaN") {
      percentage = 0;
    }

    return percentage.toFixed(0);
  }

  selectProjBoard(key) {
    this.navCtrl.push(SelectedprojPage, {
      groupKey: this.selectedGroupID,
      boardKey: key
    });
  }

  renameProject(board: ProjectBoard) {
    let prompt = this.alertCtrl.create({
      title: 'Rename Project Board',
      message: "Enter new name for the selected board.",
      inputs: [
        {
          name: 'boardName',
          placeholder: 'New Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Rename',
          handler: data => {
            
            board.boardName = data.boardName;

            this.projBoardProvider.renameProjectBoard(this.selectedGroupID, board)
            .then( res => {
              this.toast.showToast('Project Board renamed successfully.');
            })
            .catch( err => {
              this.alert.showAlert('Rename Project Board', 'Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    prompt.present();
  }

  deleteProject(selectedBoardKey, boardName) {
    let proj = {};

    this.tasksecProvider.getTaskSectionList(selectedBoardKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {
        
        if(snap.length > 0) {
          proj['projectBoard/' + this.selectedGroupID + '/' + selectedBoardKey] = {}
          proj['taskSection/' + selectedBoardKey] = {}

          snap.forEach(elem => {
            for (var key in elem.task) {
              if (elem.task.hasOwnProperty(key)) {
                proj['task/' + key] = {}
              }
            }
          });

          

          this.projBoardProvider.deleteProjBoard(proj)
          .then( res => {
            this.toast.showToast("Project Board '" + boardName + "' has been deleted.");
            console.log(res);
          })
          .catch( err => {
            this.alert.showAlert('Delete Project Board', err);
            console.log(err);
          });
        } else if(snap.length < 1) {
          proj['projectBoard/' + this.selectedGroupID + '/' + selectedBoardKey] = {}

          this.projBoardProvider.deleteProjBoard(proj)
          .then( res => {
            this.toast.showToast("Project Board '" + boardName + "' has been deleted.");
            console.log(res);
          })
          .catch( err => {
            this.alert.showAlert('Delete Project Board', err);
            console.log(err);
          });
        }
      }
    });
  }

}

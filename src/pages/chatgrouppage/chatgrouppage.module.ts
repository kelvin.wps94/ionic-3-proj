import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatgrouppagePage } from './chatgrouppage';

@NgModule({
  declarations: [
    ChatgrouppagePage,
  ],
  imports: [
    IonicPageModule.forChild(ChatgrouppagePage),
  ],
})
export class ChatgrouppagePageModule {}

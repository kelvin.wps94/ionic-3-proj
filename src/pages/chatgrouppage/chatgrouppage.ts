import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ActionSheetController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GroupProvider } from '../../providers/group/group';
import { ChatGroupMessages } from '../../models/chatgroupmessages/chatgroupmessages.model';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { CameraProvider } from '../../providers/camera/camera';
import { Observable } from 'rxjs/Observable';
import { UserProvider } from '../../providers/user/user';
import { GprofilePage } from '../gprofile/gprofile';

/**
 * Generated class for the ChatgrouppagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatgrouppage',
  templateUrl: 'chatgrouppage.html',
})
export class ChatgrouppagePage {

	selectedGroupID: string = '';
	ownUserID: string = '';
  ownName: string = '';
  loaded: boolean = false;
  admin: boolean = false;
  selectedGroupName: string = '';
  selectedGroupPhotoURL: string = '';
  ChatGroupMsg = {} as ChatGroupMessages;
  message: string = '';
  groupMemberList: object[] = [];
  imgUrl: any;
  chatMessagesList$: Observable<ChatGroupMessages[]>;
  userList$: Observable<ChatGroupMessages[]>;
  totalNumOfMsg: number = 0;
  loopCount = 0;

  @ViewChild(Content) content:Content;
  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider, private groupProvider: GroupProvider, public zone: NgZone, private chatSingleProvider: ChatmsgProvider, public actionSheetCtrl: ActionSheetController, private cameraProvider: CameraProvider, private userProvider: UserProvider) {
  	this.selectedGroupID = this.navParams.get('key');

  	this.ownUserID = this.authProvider.getCurrentUserID();
    this.ownName = this.authProvider.getCurrentUserName();

    this.groupProvider.getSelectedGroupDetails(this.selectedGroupID)
    .valueChanges()
    .subscribe(snapshot => {
      this.zone.run( () => {
        this.selectedGroupName = snapshot.groupName;
        this.selectedGroupPhotoURL = snapshot.groupPhotoURL;
        this.loaded = true;

        if(snapshot.admin == this.ownUserID) {
          this.admin = true;
        }
      });
    });

    this.loadMessages();
    this.loadMemberList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatgrouppagePage');
  }

  backBtn() {
    this.navCtrl.pop();
  }

  loadMessages() {
    this.chatMessagesList$ = this.chatSingleProvider
    .getGroupChatMessages(this.selectedGroupID)
    .snapshotChanges()
    .map( changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.userList$ = this.userProvider
    .getAllUser()
    .snapshotChanges()
    .map( changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }

  presentActionSheet(chatGroupMsg: ChatGroupMessages) {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatGroupMsg);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatGroupMsg);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  loadMemberList() {
    var tempMemList = [];

    this.groupProvider.getGroupMembersKeyList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
      snap.forEach(elem => {
        if(elem.payload.key != this.ownUserID) {
          tempMemList.push(elem.payload.key);
        }
      });
    });

    this.groupMemberList = tempMemList;
  }

  scrollAfterFewSecond() {
    setTimeout(() => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    }, 100);
  }

  scrolltoBottom() {
    setTimeout( () => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    });
  }

  scroll(last, index) {

    if(this.totalNumOfMsg < (index + 1)) {
      this.loopCount = 0;

      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }

    } else {
    
      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
          
        }
      }
    }

    this.loopCount++;
  }

  sendMessage(chatGroupMsg: ChatGroupMessages) {
    
    var groupMemberKey = {};
    var msgKey = '';

    if(this.message != '') {

      chatGroupMsg.message = this.message;
      chatGroupMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      chatGroupMsg.type = 'messages';
      chatGroupMsg.tempName = this.ownName;

      msgKey = this.chatSingleProvider.sendChatGroupMessages(chatGroupMsg, this.selectedGroupID);

      this.groupMemberList.forEach(elem => {
        groupMemberKey['notifications/' + elem + '/' + msgKey ] = {
          from: this.selectedGroupID,
          type: 'chatGroup'
        }
      });

      this.chatSingleProvider.updateGroupChatLastMsg(this.selectedGroupID, chatGroupMsg.message, chatGroupMsg.msgSentTime, chatGroupMsg.type)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });

      this.chatSingleProvider.sendGroupNotifications(groupMemberKey)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });

      this.message = '';
      this.scrolltoBottom();
      this.loadMemberList();
    }
  }

  sendImage(imgResult, chatGroupMsg: ChatGroupMessages) {

    var groupMemberKey = {};
    var msgKey: string = '';

    chatGroupMsg.message = this.message;
    chatGroupMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
    chatGroupMsg.type = 'images';

    this.chatSingleProvider.sendChatGroupMessagesWithImage(chatGroupMsg, this.selectedGroupID, imgResult)
    .then( (res:any) => {
      msgKey = res;

      this.groupMemberList.forEach(elem => {
        groupMemberKey['notifications/' + elem + '/' + msgKey ] = {
          from: this.selectedGroupID,
          type: 'chatGroup'
        }
      });

      this.chatSingleProvider.sendGroupNotifications(groupMemberKey)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });
    })
    .catch( err => {
      console.log(err);
    });

    this.message = '';
    this.scrolltoBottom();
    this.loadMemberList();
  }

  openOptions(chatGroupMsg: ChatGroupMessages) {
    this.presentActionSheet(chatGroupMsg);
  }

  groupInfo() {
    if(this.navCtrl.getPrevious().name == 'GprofilePage') {
      this.navCtrl.pop();
    } else {
      this.navCtrl.push(GprofilePage, {key: this.selectedGroupID});
    }
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FriendsPage } from './friends';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    FriendsPage,
  ],
  imports: [
    IonicPageModule.forChild(FriendsPage),
    IonicImageLoader
  ],
})
export class FriendsPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddmemtogroupPage } from './addmemtogroup';

@NgModule({
  declarations: [
    AddmemtogroupPage,
  ],
  imports: [
    IonicPageModule.forChild(AddmemtogroupPage),
  ],
})
export class AddmemtogroupPageModule {}

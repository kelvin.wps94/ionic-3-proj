import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GroupProvider } from '../../providers/group/group';
import { Group } from '../../models/group/group.model';
import { GroupprofilePage } from '../groupprofile/groupprofile';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';
/**
 * Generated class for the CreategroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-creategroup',
  templateUrl: 'creategroup.html',
})
export class CreategroupPage {

  newGroup: Group = {
    groupName: '',
    admin: this.authProvider.getCurrentUserID(),
    groupPhotoURL: 'https://firebasestorage.googleapis.com/v0/b/login-chat-app-63063.appspot.com/o/group_profileImages%2Fgroup.png?alt=media&token=abb54b93-99d1-4395-a1f8-03c58f8f7d14'
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private groupProvider: GroupProvider, private authProvider: AuthProvider, private alertProvider: AlertProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreategroupPage');
  }

  backToTabs() {
  	this.navCtrl.setRoot(TabsPage);
  }

  createGroup(group: Group) {
    if(group.groupName != '') {
      this.groupProvider.addNewGroup(group)
      .then( (res: any) => {
        this.navCtrl.setRoot(GroupprofilePage, {key: res});
      })
      .catch( err => {
        this.alertProvider.showAlert('Create Group', 'Something went wrong, please try again later!');
      });
    } else {
      this.alertProvider.showAlert('Create Group', 'Group name cannot be empty. <br>Please fill in the group name.');
    }
  }
}

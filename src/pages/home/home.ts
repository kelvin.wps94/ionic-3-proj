import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { UserCredentials } from '../../models/users/users.model';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	userCredentials = {} as UserCredentials;

  constructor(public navCtrl: NavController, private auth: AuthProvider, private alert: AlertProvider, private imghandler: ImagehandlerProvider, private platf: Platform) {

  }

  login(userCredentials: UserCredentials) {
  	this.auth.loggingIn(userCredentials)
  	.then( res => {
  		this.navCtrl.setRoot(TabsPage);
  	})
  	.catch( err => {
  		this.alert.showAlert("User's Credential", "Invalid user's credentials. <br>Please login using a valid user's credential.");
  	});
  }

  signUp() {
  	this.navCtrl.push(RegisterPage);
  }

  forgotPassword() {
  	this.navCtrl.push(ForgotpasswordPage);
  }

}

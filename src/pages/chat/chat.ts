import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserlistandrequestPage } from '../userlistandrequest/userlistandrequest';
import { UserCredentials } from '../../models/users/users.model';
import { Observable } from 'rxjs/Observable';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { ChatsinglePage } from '../chatsingle/chatsingle';
import { UserProvider } from '../../providers/user/user';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { ChatSingleMessages } from '../../models/chatsinglemessages/chatsinglemessages.model';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  userList$: Observable<UserCredentials[]>;
  chatContactList$: Observable<ChatSingleMessages[]>;
  receiveReq: number = 0;

  constructor(private friendReqProvider: FriendreqProvider, public navCtrl: NavController, public navParams: NavParams, private chatmsgProvider: ChatmsgProvider, private userProvider: UserProvider) {
    this.getAllChatContactList();

    this.friendReqProvider.getRecRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.receiveReq = snap.length;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

  allUserListPage() {
  	this.navCtrl.parent.parent.push(UserlistandrequestPage);
  }

  selectedChatConversation(key) {
    this.navCtrl.parent.parent.setRoot(ChatsinglePage, {
      key: key
    });
  }

  getAllChatContactList() {
    this.chatContactList$ = this.chatmsgProvider.getChatList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.userList$ = this.userProvider.getAllUser()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

  }

  searchuser(searchbar): Observable<UserCredentials[]> {
    this.getAllChatContactList();
    var q = searchbar.target.value;

    if(q.trim() == '') {
      return;
    }

    this.userList$ = this.userList$.map( res => {
      return res.filter( result => {
        return result.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1;
      });
    })
  }
}

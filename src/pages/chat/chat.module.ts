import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ChatPage,

  ],
  imports: [
    IonicPageModule.forChild(ChatPage),
    IonicImageLoader,
    PipesModule
  ],
})
export class ChatPageModule {}

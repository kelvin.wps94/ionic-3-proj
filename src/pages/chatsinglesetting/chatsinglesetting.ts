import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';

/**
 * Generated class for the ChatsinglesettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatsinglesetting',
  templateUrl: 'chatsinglesetting.html',
})
export class ChatsinglesettingPage {

	selectedUserID: string = '';
	selectedUsersName: string = '';
  selectedUserPhotoURL: string = '';
  totalMessages: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, public zone: NgZone, public alertCtrl: AlertController, private chatmsg: ChatmsgProvider) {
  	this.selectedUserID = this.navParams.get('key');

  	this.userProvider.getSelectedUsersDetails(this.selectedUserID)
  	.valueChanges()
  	.subscribe(snapshot => {
  		this.zone.run( () => {
	      this.selectedUsersName = snapshot.displayName;
          this.selectedUserPhotoURL = snapshot.photoURL;
	    });
  	});

    this.chatmsg.getChatMessages(this.selectedUserID)
    .snapshotChanges()
    .subscribe(snap => {
      this.totalMessages = snap.length;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatsinglesettingPage');
  }

  clearChatConfirmation() {
    let confirm = this.alertCtrl.create({
      title: 'Clear Chat Messages?',
      message: 'Are you sure you wanted to clear the chat messages with this person?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Clear',
          handler: () => {
            
            this.chatmsg.clearChatMessages(this.selectedUserID)
            .then( res => {

            })
            .catch( err => {

            });
          }
        }
      ]
    });
    confirm.present();
  }

  backTo() {
  	this.navCtrl.pop();
  }

  clearChat() {
    this.clearChatConfirmation();
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsinglesettingPage } from './chatsinglesetting';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ChatsinglesettingPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatsinglesettingPage),
    IonicImageLoader
  ],
})
export class ChatsinglesettingPageModule {}

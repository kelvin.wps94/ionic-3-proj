import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { FriendsProvider } from '../../providers/friends/friends';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';
import { ToastProvider } from '../../providers/toast/toast';
import { Friends } from '../../models/friends/friends.model';

/**
 * Generated class for the SelectedprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selectedprofile',
  templateUrl: 'selectedprofile.html',
})
export class SelectedprofilePage {

	selectedUserID: string = '';
	usersCred = {} as UserCredentials;
  isFriends: boolean = false;
  requestSent: boolean = false;
  requestReceive: boolean = false;
  friendRequest = {} as FriendRequest;
  friend = {} as Friends;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, public loadingCtrl: LoadingController, public zone: NgZone, private frenProvider: FriendsProvider, private frenReqProvider: FriendreqProvider, private toast: ToastProvider, public alertCtrl: AlertController) {
  	this.selectedUserID = this.navParams.get('key');
  	this.loadUserDetails();

    this.frenProvider.getSelectedFriends(this.selectedUserID)
    .snapshotChanges()
    .subscribe( snap => {
      if(snap.length > 0) {
        this.isFriends = true;
        console.log(snap);
      } else {
        this.isFriends = false;
        console.log(snap);
      }
    });

    this.frenReqProvider.getSelectedSentRequest(this.selectedUserID)
    .snapshotChanges()
    .subscribe( snap => {
      if(snap.length > 0) {
        this.requestSent = true;
        console.log(snap);
      } else {
        this.requestSent = false;
        console.log(snap);
      }
    });

    this.frenReqProvider.getSelectedRecRequest(this.selectedUserID)
    .snapshotChanges()
    .subscribe( snap => {
      if(snap.length > 0) {
        this.requestReceive = true;
        console.log(snap);
      } else {
        this.requestReceive = false;
        console.log(snap);
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectedprofilePage');
  }

  loadUserDetails() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Content.. please wait!'
    });
    loader.present();

  	this.userProvider.getSelectedUsersDetails(this.selectedUserID).valueChanges().subscribe(snap => {
  		this.usersCred = snap;
      this.zone.run( () => {
        this.usersCred.photoURL = snap.photoURL;
      });
      loader.dismiss();
  	});
  	
  }

  backButton() {
  	this.navCtrl.pop();
  }

  showConfirm(key, displayName) {
    let confirm = this.alertCtrl.create({
      title: 'Unfriend ' + displayName + '?',
      message: 'Are you sure you wanted to unfriend ' + displayName + '?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            
            this.frenProvider.unfriendSelectedPerson(key)
            .then( res => {
              this.toast.showToast("You have unfriended " + displayName + ".");
            })
            .catch( err => {
              this.toast.showToast('Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    confirm.present();
  }

  showConfirm1(key, displayName) {
    let confirm = this.alertCtrl.create({
      title: 'Decline Friend Request?',
      message: 'Are you sure you wanted to decline this friend request?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            
            this.frenReqProvider.cancelReceiveFriendRequest(key)
            .then( res => {
              this.toast.showToast("You have declined " + displayName + "'s friends request.");
            })
            .catch( err => {
              this.toast.showToast('Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    confirm.present();
  }

  unfriend() {
    this.showConfirm(this.selectedUserID, this.usersCred.displayName);
  }

  addFriend() {
    this.frenReqProvider.submitFriendReq(this.selectedUserID, this.friendRequest)
    .then( res => {
      this.toast.showToast('You have successfully send a friend request to ' + this.usersCred.displayName);
    })
    .catch( err => {
      this.toast.showToast('Something went wrong, please try again later.');
    });
  }

  cancelRequest() {
    this.frenReqProvider.cancelSentFriendRequest(this.selectedUserID)
    .then( res => {
      this.toast.showToast("You have cancel this sent friends request.");
    })
    .catch( err => {
      this.toast.showToast("Something went wrong. Please try again later!");
    });
  }

  acceptRequest() {
    this.frenReqProvider.cancelReceiveFriendRequest(this.selectedUserID)
    .then( res => {
      this.frenReqProvider.acceptFriendRequest(this.selectedUserID, this.friend)
      .then( res => {
        this.toast.showToast("You're now friends with " + this.usersCred.displayName);
      })
      .catch( err => {
        this.toast.showToast('Something went wrong. Please try agian later!');
      })
    })
    .catch( err => {
      this.toast.showToast('Something went wrong. Please try again later!');
    })
  }

  declineRequest() {
    this.showConfirm1(this.selectedUserID, this.usersCred.displayName);
  }

}

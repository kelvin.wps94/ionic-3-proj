import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedprofilePage } from './selectedprofile';

@NgModule({
  declarations: [
    SelectedprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(SelectedprofilePage),
  ],
})
export class SelectedprofilePageModule {}

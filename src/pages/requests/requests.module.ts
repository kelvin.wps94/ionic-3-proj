import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestsPage } from './requests';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    RequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestsPage),
    IonicImageLoader
  ],
})
export class RequestsPageModule {}

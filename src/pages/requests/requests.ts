import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';
import { Observable } from 'rxjs/Observable';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserCredentials } from '../../models/users/users.model';
import { Friends } from '../../models/friends/friends.model';
import { AlertProvider } from '../../providers/alert/alert';
import { ToastProvider } from '../../providers/toast/toast'; 
/**
 * Generated class for the RequestsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-requests',
  templateUrl: 'requests.html',
})
export class RequestsPage {

	sentreqList$: Observable<UserCredentials[]>;
  receivereqList$: Observable<UserCredentials[]>;
	sentReqListNum: number = 0;
  receiveReqListNum: number = 0;
  friend = {} as Friends;


  constructor(private alertCtrl: AlertController, private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private friendreqProvider: FriendreqProvider, private afDatabase: AngularFireDatabase, private alert: AlertProvider, private toast: ToastProvider) {
    this.sentreqList$ = this.friendreqProvider.getAllSentRequest();
    this.receivereqList$ = this.friendreqProvider.getAllFriendRequest();
    
    this.sentreqList$.subscribe(res => {
      this.sentReqListNum = res.length;
    });

    this.receivereqList$.subscribe(res => {
      this.receiveReqListNum = res.length;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestsPage');
  }

  backToTabs() {
  	this.navCtrl.parent.parent.setRoot(TabsPage);
  }

  showConfirm(key, displayName) {
    let confirm = this.alertCtrl.create({
      title: 'Decline Friend Request?',
      message: 'Are you sure you wanted to decline this friend request?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            
            this.friendreqProvider.cancelReceiveFriendRequest(key)
            .then( res => {
              this.toast.showToast("You have declined " + displayName + "'s friends request.");
            })
            .catch( err => {
              this.toast.showToast('Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    confirm.present();
  }

  acceptFriendReq(key, displayName) {
    this.friendreqProvider.cancelReceiveFriendRequest(key)
    .then( res => {
      this.friendreqProvider.acceptFriendRequest(key,this.friend)
      .then( res => {
        this.toast.showToast("You're now friends with " + displayName);
      })
      .catch( err => {
        this.toast.showToast('Something went wrong. Please try agian later!');
      })
    })
    .catch( err => {
      this.toast.showToast('Something went wrong. Please try again later!');
    })
  }

  cancelSentFRequest(key) {

    this.friendreqProvider.cancelSentFriendRequest(key)
    .then( res => {
      this.toast.showToast("You have cancel this sent friends request.");
    })
    .catch( err => {
      this.toast.showToast("Something went wrong. Please try again later!");
    });
  }

  denyFriendReq(key, displayName) {
    this.showConfirm(key, displayName);
  }

}

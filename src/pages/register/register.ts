import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserCredentials } from '../../models/users/users.model';
import { UserProvider } from '../../providers/user/user';
import { ProfilepicPage } from '../profilepic/profilepic';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

	newUser: UserCredentials = {
		email: '',
		password: '',
		displayName: '',
    photoURL: 'https://firebasestorage.googleapis.com/v0/b/login-chat-app-63063.appspot.com/o/profileImages%2Fdefaultavatar%2Fmedium-default-avatar.png?alt=media&token=bc558370-c9c8-4b11-be08-431d6b6fc1f8',
	  status: "Hey there, I'm using Ant-Nest..."
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private user: UserProvider, private loadingCtrl: LoadingController, private alert: AlertProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerUser(newUser: UserCredentials) {

  	let loader = this.loadingCtrl.create({
  		content: 'Please wait'
  	});
  	loader.present();

    if(newUser.email != '' && newUser.password != '' && newUser.displayName != '') {
    	this.user.addUser(newUser)
    	.then( res => {
    		loader.dismiss();
    		this.navCtrl.push(ProfilepicPage, {
    			newUser: newUser
    		});
    	})
    	.catch( err => {
    		loader.dismiss();
    		this.alert.showAlert('Account Registration', err);
    	});
    } else {
      loader.dismiss();
      this.alert.showAlert(
        'Account Registration', 
        'Please fill in all the required fields:<br>' +
        '- <b>Email:</b> Must be in correct format and cannot be empty.<br>' +
        '- <b>Password:</b> Should be at least 6 characters.<br>' +
        '- <b>Display Name:</b> Cannot be empty.<br>'
      )

    }
  }

  backButton() {
    this.navCtrl.pop();
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsinglePage } from './chatsingle';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ChatsinglePage,
  ],
  imports: [
    IonicPageModule.forChild(ChatsinglePage),
    IonicImageLoader
  ],
})
export class ChatsinglePageModule {}

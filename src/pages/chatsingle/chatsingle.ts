import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ActionSheetController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { UserCredentials } from '../../models/users/users.model';
import { UserProvider } from '../../providers/user/user';
import { ChatSingleMessages } from '../../models/chatsinglemessages/chatsinglemessages.model';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../../providers/auth/auth';
import { CameraProvider } from '../../providers/camera/camera';
import { ChatsinglesettingPage } from '../chatsinglesetting/chatsinglesetting';
import { SelectedprofilePage } from '../selectedprofile/selectedprofile';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the ChatsinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatsingle',
  templateUrl: 'chatsingle.html',
})
export class ChatsinglePage {

	selectedUserID: string = '';
	selectedUsersName: string = '';
  selectedUserPhotoURL: string = '';
	ChatSingleMsg = {} as ChatSingleMessages;
  message: string = '';
  //chatMessagesList$: Observable<ChatSingleMessages[]>;
  ownUserID: string = '';
  imgUrl: any;
  _imageViewerCtrl: ImageViewerController;
  totalNumOfMsg: number = 0;
  loopCount = 0;
  testing;
  allMessages: object[] = [];

  @ViewChild(Content) content:Content;
  constructor(private cameraProvider: CameraProvider, public actionSheetCtrl: ActionSheetController, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private chatSingleProvider: ChatmsgProvider, private authProvider: AuthProvider, private imageViewerCtrl: ImageViewerController) {
  	this.selectedUserID = this.navParams.get('key');

  	this.userProvider.getSelectedUsersDetails(this.selectedUserID)
  	.valueChanges()
  	.subscribe(snapshot => {
  		this.zone.run( () => {
	        this.selectedUsersName = snapshot.displayName;
          this.selectedUserPhotoURL = snapshot.photoURL;
	    });
  	});

    this.ownUserID =  this.authProvider.getCurrentUserID();

    this.loadMessages();
    this.scrollAfterFewSecond();

    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatsinglePage');
  }

  /*loadMessages() {
    this.chatMessagesList$ = this.chatSingleProvider
    .getChatMessages(this.selectedUserID)
    .snapshotChanges()
    .map( changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }*/

  //For better load images approaches
  loadMessages() {

    this.chatSingleProvider
    .getChatMessages(this.selectedUserID)
    .snapshotChanges()
    .subscribe(snap => {
      var tempObject: object[] = [];

      snap.forEach(elem => {
        if(elem.key != 'lastMsgDate') {
          tempObject.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        }
      });

      this.allMessages = tempObject;
    });
  }

  scroll(last, index) {

    if(this.totalNumOfMsg < (index + 1)) {
      this.loopCount = 0;

      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }

    } else {
    
      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }
    }

    this.loopCount++;
  }

  presentActionSheet(chatSingleMsg: ChatSingleMessages) {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatSingleMsg);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatSingleMsg);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  backToTabs() {
  	this.navCtrl.setRoot(TabsPage);
  }

  scrolltoBottom() {
    setTimeout( () => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    });
  }

  scrollAfterFewSecond() {
    setTimeout(() => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    }, 100);
  }

  sendMessage(chatSingleMsg: ChatSingleMessages) {

    if(this.message != '') {
      chatSingleMsg.message = this.message;
      chatSingleMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      chatSingleMsg.type = 'messages';

      this.chatSingleProvider.sendChatSingleMessages(chatSingleMsg, this.selectedUserID);

      this.message = '';
      this.scrolltoBottom();
    }
  }

  sendImage(imgResult, chatSingleMsg: ChatSingleMessages) {

    chatSingleMsg.message = this.message;
    chatSingleMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
    chatSingleMsg.type = 'images';

    this.chatSingleProvider.sendChatSingleMessagesWithImageFromCamera(chatSingleMsg, this.selectedUserID, imgResult);

    this.message = '';
    this.scrolltoBottom();
  }

  openOptions(chatSingleMsg: ChatSingleMessages) {
    this.presentActionSheet(chatSingleMsg);
  }

  openProfile() {
    this.navCtrl.push(SelectedprofilePage, {key: this.selectedUserID});
  }

  chatSetting() {
    this.navCtrl.push(ChatsinglesettingPage, {key: this.selectedUserID});
  }

}

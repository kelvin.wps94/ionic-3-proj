import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Group } from '../../models/group/group.model';
import { GroupProvider } from '../../providers/group/group';
import { UserGroup } from '../../models/usergroup/usergroup.model';
import { UserCredentials } from '../../models/users/users.model';
import { ChatSingleMessages } from '../../models/chatsinglemessages/chatsinglemessages.model';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { UserProvider } from '../../providers/user/user';
import { ChatsinglepagePage } from '../chatsinglepage/chatsinglepage';
import { ChatgrouppagePage } from '../chatgrouppage/chatgrouppage';
import { GroupLastMsg } from '../../models/grouplastmsg/grouplastmsg.model';
import { FriendlistPage } from '../friendlist/friendlist';
import { GrouplistPage } from '../grouplist/grouplist';

/**
 * Generated class for the ConversationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html',
})
export class ConversationsPage {
	@ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides;

	SwipedTabsIndicator: any = null;
	tabs: any = [];

	userList$: Observable<UserCredentials[]>;
	chatContactList$: ChatSingleMessages[];
	groupChatList$: GroupLastMsg[] = [];
	groupList$: Observable<Group[]>;
	joinedGroupList$: UserGroup[] = [];
	searchString: string = '';
  activeTabs: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private chatmsgProvider: ChatmsgProvider, private userProvider: UserProvider, private groupProvider: GroupProvider) {
  	this.tabs = ["Single", "Group"]
  	this.getAllChatContactList();
  	this.getGroupList();
  }

  ionViewDidEnter() {
  	this.SwipedTabsIndicator = document.getElementById("indicator");
    this.activeTabs = this.SwipedTabsSlider.getActiveIndex();
  }

  selectTab(index) {

  	this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
  	this.SwipedTabsSlider.slideTo(index, 500);
  }

  updateIndicatorPosition(num) {

  	if(this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';

      if(num == 1) {

        if(this.searchString != '') {
          this.searchString = '';
      
          if(this.SwipedTabsSlider.getActiveIndex() == 0) {
            this.getGroupList();
          } else {
            this.getAllChatContactList();
          }
        }
      } else {
        this.activeTabs = this.SwipedTabsSlider.getActiveIndex();
      }
  	}
  }

  animateIndicator($event) {

  	if(this.SwipedTabsIndicator) {
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
    }
  }

  getAllChatContactList() {
    this.chatmsgProvider.getChatList()
    .snapshotChanges()
    .subscribe(snap => {
      var tempObj = [];

      snap.forEach(elem => {
        tempObj.push({
          key: elem.payload.key,
          ...elem.payload.val()
        });
      });

      this.chatContactList$ = tempObj;
    });

    this.userList$ = this.userProvider.getAllUser()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

  }

  getGroupList() {
    this.groupProvider.getJoinedGroupList()
    .snapshotChanges()
    .subscribe(snap => {
      var tempObj = [];

      snap.forEach(elem => {
        tempObj.push({
          key: elem.payload.key,
          ...elem.payload.val()
        });
      });

      this.joinedGroupList$ = tempObj;
    });

    this.groupList$ = this.groupProvider.getGroupList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.chatmsgProvider.getAllGroupChatMessages()
    .snapshotChanges()
    .subscribe(snap => {

      var tempObj = [];
      snap.forEach(elem => {
        tempObj.push({
          key: elem.key,
          ...elem.payload.val()
        });
      });

      this.groupChatList$ = tempObj;
    });
  }

  searchuser(searchbar): Observable<UserCredentials[]> {
  	
    this.getAllChatContactList();
    var q = searchbar.target.value;

    if(q.trim() == '') {
      return;
    }

    this.userList$ = this.userList$.map( res => {
      return res.filter( result => {
        return result.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1;
      });
    })
  }

  searchgroup(searchbar): Observable<Group[]> {
  	
    this.getGroupList();
    var q = searchbar.target.value;

    if(q.trim() == '') {
      return;
    }

    this.groupList$ = this.groupList$.map( res => {
      return res.filter( result => {
        return result.groupName.toLowerCase().indexOf(q.toLowerCase()) > -1;
      });
    })
  }

  search(searchbar) {
  	if(this.SwipedTabsSlider.getActiveIndex() == 0) {
  		this.searchuser(searchbar);
  	} else {
  		this.searchgroup(searchbar);
  	}
  }

  clearGroupSearch(searchbar): Observable<UserCredentials[]> {
    this.getGroupList();

    return;
  }

  clearIndividualSearch(searchbar): Observable<UserCredentials[]> {
    this.getAllChatContactList();

    return;
  }

  clearSearch(searchbar) {
    if(this.SwipedTabsSlider.getActiveIndex() == 0) {
      this.clearIndividualSearch(searchbar);
    } else {
      this.clearGroupSearch(searchbar);
    }
  }

  selectedChatConversation(key) {
    this.navCtrl.push(ChatsinglepagePage, {key: key});
  }

  openSelectedGroupChat(key) {
    this.navCtrl.push(ChatgrouppagePage, {key: key});
  }

  friendList() {
    this.navCtrl.setRoot(FriendlistPage);
  }

  groupList() {
    this.navCtrl.setRoot(GrouplistPage);
  }

}

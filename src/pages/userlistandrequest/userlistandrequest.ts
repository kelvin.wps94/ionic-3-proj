import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';

/**
 * Generated class for the UserlistandrequestPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-userlistandrequest',
  templateUrl: 'userlistandrequest.html'
})
export class UserlistandrequestPage {

  alluserlistTab = 'AlluserlistPage'
  requestsTab = 'RequestsPage'
  receiveReq: number = 0;

  constructor(public navCtrl: NavController, private friendReqProvider: FriendreqProvider) {
  	this.friendReqProvider.getRecRequestListKey()
  	.snapshotChanges()
  	.subscribe(snap => {
  		this.receiveReq = snap.length;
  	});
  }

}

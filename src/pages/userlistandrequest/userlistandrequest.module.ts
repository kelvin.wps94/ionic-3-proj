import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserlistandrequestPage } from './userlistandrequest';

@NgModule({
  declarations: [
    UserlistandrequestPage,
  ],
  imports: [
    IonicPageModule.forChild(UserlistandrequestPage),
  ]
})
export class UserlistandrequestPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Task } from '../../models/task/task.model';
import { TaskSection } from '../../models/tasksection/tasksection.model';
import { UserCredentials } from '../../models/users/users.model';
import { GroupMember } from '../../models/groupmember/groupmember.model';
import { AssignedMember } from '../../models/assignedmember/assignedmember.model';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import { TaskProvider } from '../../providers/task/task';
import { TasksectionProvider } from '../../providers/tasksection/tasksection';
import { UserProvider } from '../../providers/user/user';
import { GroupProvider } from '../../providers/group/group';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the AssignmemberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assignmember',
  templateUrl: 'assignmember.html',
})
export class AssignmemberPage {

	selectedGroupKey: string = '';
	selectedBoardKey: string = '';
	selectedTaskSectKey: string = '';
	selectedTaskKey: string = '';

	currentUserId: string = '';
	isAdmin: boolean = false;

	task = {} as Task;
	taskSection = {} as TaskSection;
	assignedMem: AssignedMember = {
		isAssignedToTask: true
	}

	activityLog: ActivityLog = {
	    activityType: '',
	    handler: '',
	    handlerIsAdmin: false,
	    dateNTimeHandled: '',
	    logActivity: ''
	}

	userList$: UserCredentials[] = [];
	groupMemList$: GroupMember[] = [];
	assignedMemList$: AssignedMember[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private taskProvider: TaskProvider, private taskSectionProvider: TasksectionProvider, private userProvider: UserProvider, private groupProvider: GroupProvider, private authProvider: AuthProvider) {
  	this.selectedGroupKey = this.navParams.get('groupKey');
    this.selectedBoardKey = this.navParams.get('boardKey');
  	this.selectedTaskSectKey = this.navParams.get('taskSectKey');
  	this.selectedTaskKey = this.navParams.get('taskKey');
  	this.currentUserId = this.authProvider.getCurrentUserID();

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {

        if(snap.admin == this.currentUserId) {
          this.isAdmin = true;
        }
      }
    });

  	this.taskProvider.getSelectedTaskDetails(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {
  			this.task = snap;
  		}
  	});

  	this.taskSectionProvider.getSelectedTaskSectionDetails(this.selectedBoardKey, this.selectedTaskSectKey)
  	.valueChanges()
  	.subscribe(snap => {
  		if (snap) {
  			this.taskSection = snap;
  		}
  	});

  	this.groupProvider.getGroupMembersKeyList(this.selectedGroupKey)
  	.snapshotChanges()
  	.subscribe(snap => {
  		var tempObj = [];

  		snap.forEach(elem => {
  			tempObj.push({
  				key: elem.payload.key,
  				...elem.payload.val()
  			});
  		});

  		this.groupMemList$ = tempObj;
  	});

  	this.userProvider.getAllUser()
  	.snapshotChanges()
  	.subscribe(snap => {
  		var tempObj = [];

  		snap.forEach(elem => {
  			tempObj.push({
  				key: elem.payload.key,
  				...elem.payload.val()
  			});
  		});

  		this.userList$ = tempObj;
  	});

  	this.taskProvider.getAssignedMemList(this.selectedTaskKey)
  	.snapshotChanges()
  	.subscribe(snap => {
  		var tempObj = [];

  		snap.forEach(elem => {
  			tempObj.push({
  				key: elem.payload.key,
  				...elem.payload.val()
  			});
  		});

  		this.assignedMemList$ = tempObj;
  	});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignmemberPage');
  }

  backButton() {
  	this.navCtrl.pop()
  }

  assignMemToTask(memKey, assignedMem: AssignedMember, assignedMemName) {

  	var found = false;

  	this.assignedMemList$
  	.forEach(snap => {

  		if(snap.key == memKey) {
  			found = true;
  		}

  	});

  	if(found) {

  		this.taskProvider.removeAssignedMemberToTask(this.selectedTaskKey, memKey)
		  .then( res => {
		  	console.log('Removed: ' + res);

        this.activityLog.activityType = 'TaskAssignMemberLog';
        this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
        this.activityLog.handler = this.currentUserId;
        this.activityLog.handlerIsAdmin = this.isAdmin;
        this.activityLog.logActivity = "has unassigned " + assignedMemName + " to handle this task.";

        this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
        .then( r => {
          console.log('Activity has been Logged');
        })
        .catch( e => {
          console.log('Activity Logging: ' + e);
        });

		  	this.taskProvider.getAssignedMemList(this.selectedTaskKey)
		  	.snapshotChanges()
		  	.subscribe( snap => {
		  		if(snap.length < 1) {
		  			this.task.assignedMember = false;

				  	this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
				  	.then( res => {
				  		console.log('Update Task Item Status: ' + res);
				  	})
				  	.catch( err => {
				  		console.log('Update Task Item Status Failed: ' + err);
				  	});
		  		}
		  	});
		})
		.catch( err => {
		  	console.log(err);
		});

  	} else {

  		this.taskProvider.assignMemberToTask(this.selectedTaskKey, assignedMem, memKey)
		.then( res => {
		  	console.log('Assigned: ' + res);

	        this.activityLog.activityType = 'TaskAssignMemberLog';
	        this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
	        this.activityLog.handler = this.currentUserId;
	        this.activityLog.handlerIsAdmin = this.isAdmin;
	        this.activityLog.logActivity = "has assigned " + assignedMemName + " to handle this task.";

	        this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
	        .then( r => {
	          console.log('Activity has been Logged');
	        })
	        .catch( e => {
	          console.log('Activity Logging: ' + e);
	        });

			this.task.assignedMember = true;
			this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
		  	.then( res => {
		  		console.log('Update Task Item Status: ' + res);
		  	})
		  	.catch( err => {
		  		console.log('Update Task Item Status Failed: ' + err);
		  	});
		})
		.catch( err => {
			console.log(err);
		});
  	}
  }

  checking(groupMemkey) {

  	var found = false;

  	if(this.assignedMemList$.length > 0) {
  		this.assignedMemList$
	  	.forEach(snap => {
	  		if(snap.key == groupMemkey) {
	  			found = true;
	  		}
	  	});

	  	if(found) {
	  		return true;
	 	} else {
	  		return false;
	  	}

  	} else {
  		return false;
  	}

  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignmemberPage } from './assignmember';

@NgModule({
  declarations: [
    AssignmemberPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignmemberPage),
  ],
})
export class AssignmemberPageModule {}

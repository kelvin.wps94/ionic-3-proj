import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectdashboardPage } from './projectdashboard';

@NgModule({
  declarations: [
    ProjectdashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectdashboardPage),
  ],
})
export class ProjectdashboardPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { ProjectboardProvider } from '../../providers/projectboard/projectboard';
import { ProjectBoard } from '../../models/projectboard/projectboard.model';
import { ToastProvider } from '../../providers/toast/toast';
import { AlertProvider } from '../../providers/alert/alert';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../../providers/auth/auth';
import { GroupProvider } from '../../providers/group/group';
import { SelectedprojboardPage } from '../selectedprojboard/selectedprojboard';
import { TasksectionProvider } from '../../providers/tasksection/tasksection';
import { TaskSection } from '../../models/tasksection/tasksection.model';

/**
 * Generated class for the ProjectdashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projectdashboard',
  templateUrl: 'projectdashboard.html',
})
export class ProjectdashboardPage {

	selectedGroupID: string = '';
  currentUserId: string = '';
  isAdmin: boolean = false;
  percentage: number = 0;

  projBoard: ProjectBoard = {
    boardName: '',
    hasTaskCompleteSection: false,
    totalTask: 0,
    totalCompletedTask: 0
  }

  projBoardList$: Observable<ProjectBoard[]>;
  tempTaskSecValues: object[] = [];
  tempTaskSecSnapshot: TaskSection[] = [];
  totalProjBoard: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private projBoardProvider: ProjectboardProvider, private toast: ToastProvider, private alert: AlertProvider, private authProvider: AuthProvider, private groupProvider: GroupProvider, public actionSheetCtrl: ActionSheetController, private tasksecProvider: TasksectionProvider) {
  	this.selectedGroupID = this.navParams.get('key');
    this.currentUserId = this.authProvider.getCurrentUserID();
    
    this.projBoardList$ = this.projBoardProvider.getProjectBoardList(this.selectedGroupID)
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.groupProvider.getSelectedGroupDetails(this.selectedGroupID)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {

        if(snap.admin == this.currentUserId) {
          this.isAdmin = true;
        }

      }
    });

    this.projBoardProvider.getProjectBoardList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
      this.totalProjBoard = snap.length;
    });
  }

  deleteProjectBoard(selectedBoardKey, boardName) {
    let proj = {};

    this.tasksecProvider.getTaskSectionList(selectedBoardKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {
        
        if(snap.length > 0) {
          proj['projectBoard/' + this.selectedGroupID + '/' + selectedBoardKey] = {}
          proj['taskSection/' + selectedBoardKey] = {}

          snap.forEach(elem => {
            for (var key in elem.task) {
              if (elem.task.hasOwnProperty(key)) {
                proj['task/' + key] = {}
              }
            }
          });

          

          this.projBoardProvider.deleteProjBoard(proj)
          .then( res => {
            this.toast.showToast("Project Board '" + boardName + "' has been deleted.");
            console.log(res);
          })
          .catch( err => {
            this.alert.showAlert('Delete Project Board', err);
            console.log(err);
          });
        } else if(snap.length < 1) {
          proj['projectBoard/' + this.selectedGroupID + '/' + selectedBoardKey] = {}

          this.projBoardProvider.deleteProjBoard(proj)
          .then( res => {
            this.toast.showToast("Project Board '" + boardName + "' has been deleted.");
            console.log(res);
          })
          .catch( err => {
            this.alert.showAlert('Delete Project Board', err);
            console.log(err);
          });
        }
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectdashboardPage');
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'New Project Board',
      message: "Enter a name for the new project board",
      inputs: [
        {
          name: 'boardName',
          placeholder: 'Project Board Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Add',
          handler: data => {
            
            if(data.boardName != '') {
              this.projBoard.boardName = data.boardName;

              this.projBoardProvider.addNewProjBoard(this.selectedGroupID, this.projBoard)
              .then( res => {
                this.toast.showToast("New Board '" + this.projBoard.boardName + "' has been added.");
              })
              .catch( err => {
                this.alert.showAlert('New Project Board', 'Something went wrong. Please try again later!');
              });
            } else {
              this.alert.showAlert('New Project Board', "Project Board Name Cannot Be Empty. <br>Please Try again.");
            }
          }
        }
      ]
    });
    prompt.present();
  }

  showPromptRename(board: ProjectBoard) {
    let prompt = this.alertCtrl.create({
      title: 'Rename Project Board',
      message: "Enter new name for the selected board.",
      inputs: [
        {
          name: 'boardName',
          placeholder: 'New Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Rename',
          handler: data => {
            
            var tempName = board.boardName;
            board.boardName = data.boardName;

            this.projBoardProvider.renameProjectBoard(this.selectedGroupID, board)
            .then( res => {
              this.toast.showToast('Project Board renamed successfully.');
            })
            .catch( err => {
              this.alert.showAlert('Rename Project Board', 'Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    prompt.present();
  }

  addNewProjectBoard() {
    this.showPrompt();
  }

  selectProjBoard(selectedBoardKey) {
    this.navCtrl.push(SelectedprojboardPage, {
      groupKey: this.selectedGroupID,
      boardKey: selectedBoardKey
    });
  }

  backToGroupProfile() {
    this.navCtrl.pop();
  }

  projectProgressPercentage(totalCompletedTask, totalTask) {
    var percentage = 0;

    percentage = (totalCompletedTask / totalTask) * 100;

    if(String(percentage) === "NaN") {
      percentage = 0;
    }

    return percentage.toFixed(0);
  }

  projectProgressStatus(totalCompletedTask, totalTask) {

    var percentage = 0;

    percentage = (totalCompletedTask / totalTask) * 100;

    if(String(percentage) === "NaN") {
      percentage = 0;
    }

    if(parseInt(percentage.toFixed(0)) > 99) {
      return 'Completed'
    } else {
      return 'In-progress'
    }

  }

  boardMoreOptions(board: ProjectBoard) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Project Board Options',
      buttons: [
        {
          text: 'Rename Board',
          icon: 'create',
          handler: () => {
            this.showPromptRename(board);
          }
        },{
          text: 'Delete Board',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            
            this.deleteProjectBoard(board.key, board.boardName);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });

    if(this.isAdmin) {
      actionSheet.present();
    }
  }

}

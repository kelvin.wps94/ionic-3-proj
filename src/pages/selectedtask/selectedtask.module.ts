import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedtaskPage } from './selectedtask';

@NgModule({
  declarations: [
    SelectedtaskPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectedtaskPage),
  ],
})
export class SelectedtaskPageModule {}

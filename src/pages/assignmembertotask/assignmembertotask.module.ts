import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignmembertotaskPage } from './assignmembertotask';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    AssignmembertotaskPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignmembertotaskPage),
    IonicImageLoader
  ],
})
export class AssignmembertotaskPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivitylogPage } from './activitylog';

@NgModule({
  declarations: [
    ActivitylogPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivitylogPage),
  ],
})
export class ActivitylogPageModule {}

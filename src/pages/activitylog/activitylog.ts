import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Select, DateTime } from 'ionic-angular';
import { TaskProvider } from '../../providers/task/task';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import { UserCredentials } from '../../models/users/users.model';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ActivitylogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activitylog',
  templateUrl: 'activitylog.html',
})
export class ActivitylogPage {

	selectedTaskKey: string = '';
	totalOfActivityLog: number = 0;
	loggingActivity: ActivityLog[] = [];
	userList: UserCredentials[] = [];
	preventTriggerTwice: number = 0;
	hideComponent: boolean = true;
	maxFilterDate: any;

	filterOptions = {
	    filterType: 'all',
	    filterValue: ''
	}

	@ViewChild('activityLogType') activityLogType: Select;
	@ViewChild('filterDateSelection') filterDateSelection: DateTime;
  constructor(public navCtrl: NavController, public navParams: NavParams, private taskProvider: TaskProvider, public zone: NgZone, private userProvider: UserProvider, public actionSheetCtrl: ActionSheetController) {
  	this.selectedTaskKey = this.navParams.get('taskKey');

  	this.getActivityLoggingList();

  	this.userProvider.getAllUser()
    .snapshotChanges()
    .subscribe(snap => {
      var tempUserList = [];

      if(snap) {
        snap.forEach(elem => {
          tempUserList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          })
        });
      }

      this.userList = tempUserList;
    });

    this.maxFilterDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0,-1));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitylogPage');
  }

  backButton() {
  	this.navCtrl.pop();
  }

  getActivityLoggingList() {
    this.taskProvider.getActivityLogging(this.selectedTaskKey)
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run( () => {
        var tempActivityLogObj = [];

        if(snap) {
          snap.forEach(elem => {
            tempActivityLogObj.push({
              key: elem.payload.key,
              ...elem.payload.val()
            });
          });
        }

        this.loggingActivity = tempActivityLogObj;
        this.totalOfActivityLog = this.loggingActivity.length;
      });
    });
  }

  openFilterOptions() {
  	console.log('yes');

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Filter Options:',
      buttons: [
        {
          text: 'Show All Activity',
          icon: 'paper',
          handler: () => {

            this.filterOptions.filterType = 'all';
            this.filterOptions.filterValue = '';
            this.getActivityLoggingList();

          }
        },{
          text: 'Filter By Type',
          icon: 'funnel',
          handler: () => {
            this.preventTriggerTwice = 0;
            this.activityLogType.open();

          }
        },{
          text: 'Filter By Date',
          icon: 'calendar',
          handler: () => {
            this.preventTriggerTwice = 0;
            this.filterDateSelection.open();

          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  changeFilterType() {
    if(this.preventTriggerTwice == 0) {
      this.filterOptions.filterType = 'type';
      this.getActivityLoggingList();
      this.preventTriggerTwice++;
    }
  }

  updateFilterMaxDate() {
    this.maxFilterDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0,-1));
  }

  setFilterDate() {
    if(this.preventTriggerTwice == 0) {
      this.filterOptions.filterType = 'date';
      this.getActivityLoggingList();
      this.preventTriggerTwice++;
    }
  }

}

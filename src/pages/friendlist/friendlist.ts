import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Friends } from '../../models/friends/friends.model';
import { FriendsProvider } from '../../providers/friends/friends';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { Observable } from 'rxjs/Observable'
import { ChatsinglepagePage } from '../chatsinglepage/chatsinglepage';
import { ToastProvider } from '../../providers/toast/toast';
import { SelectedprofilePage } from '../selectedprofile/selectedprofile';
import { FindfriendsPage } from '../findfriends/findfriends';

/**
 * Generated class for the FriendlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friendlist',
  templateUrl: 'friendlist.html',
})
export class FriendlistPage {

	friendsList$: Observable<Friends[]>;
  	userList$: Observable<UserCredentials[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public friendsProvider: FriendsProvider, private userProvider: UserProvider, private toast: ToastProvider, public alertCtrl: AlertController) {
  	this.getAllFriendsList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendlistPage');
  }

  getAllFriendsList() {
    this.friendsList$ = this.friendsProvider.getFriendList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.userList$ = this.userProvider.getAllUser()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }

  searchuser(searchbar): Observable<UserCredentials[]> {
    this.getAllFriendsList();
    var q = searchbar.target.value;

    if(q.trim() == '') {
      return;
    }

    this.userList$ = this.userList$.map( res => {
      return res.filter( result => {
        return result.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1;
      });
    })
  }

  findFriends() {
  	this.navCtrl.setRoot(FindfriendsPage);
  }

  showConfirm(key, displayName) {
    let confirm = this.alertCtrl.create({
      title: 'Unfriend ' + displayName + '?',
      message: 'Are you sure you wanted to unfriend ' + displayName + '?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            
            this.friendsProvider.unfriendSelectedPerson(key)
            .then( res => {
              this.toast.showToast("You have unfriended " + displayName + ".");
            })
            .catch( err => {
              this.toast.showToast('Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    confirm.present();
  }

  unfriendThisPerson(key, displayName) {
  	this.showConfirm(key, displayName);
  }

  chatWithThisPerson(key) {
  	this.navCtrl.push(ChatsinglepagePage, {
  		key: key
  	});
  }

  viewSelectedProfile(key) {
  	this.navCtrl.push(SelectedprofilePage, {
  		key: key
  	});
  }

}

import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform, ActionSheetController} from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { AlertProvider } from '../../providers/alert/alert';
import { UserlistandrequestPage } from '../userlistandrequest/userlistandrequest';
import { FriendsPage } from '../friends/friends';
import { CameraProvider } from '../../providers/camera/camera';
import { FirebasestorageProvider } from '../../providers/firebasestorage/firebasestorage';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

	usersCred = {} as UserCredentials;
  loaded: boolean = false;
  receiveReq: number = 0;

  constructor(private friendReqProvider: FriendreqProvider, private fireStore: FirebasestorageProvider, public alertCtrl: AlertController, private alert: AlertProvider, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private imagehandler: ImagehandlerProvider, public zone: NgZone, private auth: AuthProvider, public plt: Platform, public actionSheetCtrl: ActionSheetController, private cameraProvider: CameraProvider, private imageViewerCtrl: ImageViewerController) {
    this.friendReqProvider.getRecRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.receiveReq = snap.length;
    });
  }

  presentImage(myImage) {
    const imageViewer = this.imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  ionViewWillEnter() {
    this.loadUserDetails();
  }

  allUserListPage() {
    this.navCtrl.parent.parent.push(UserlistandrequestPage);
  }

  presentActionSheet() {
    let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });

    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                this.usersCred.photoURL = url;
                this.userProvider.updateimage(url, this.usersCred)
                .then( res => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Profile Images successfully updated!');
                })
                .catch( err => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
                });
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
              });
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                this.usersCred.photoURL = url;
                this.userProvider.updateimage(url, this.usersCred)
                .then( res => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Profile Images successfully updated!');
                })
                .catch( err => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
                });
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
              });
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  loadUserDetails() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Content.. please wait!'
    });
    loader.present();

  	this.userProvider.getUserDetails().valueChanges().subscribe(snap => {
  		this.usersCred = snap;
      this.zone.run( () => {
        this.usersCred.photoURL = snap.photoURL;
      });
      loader.dismiss();
      this.loaded = true;
  	});
  	
  }

  editDisplayName() {
    let alert = this.alertCtrl.create({
      title: 'Edit Display Name',
      inputs: [{
        name: 'displayName',
        placeholder: 'Display Name'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      },
        {
          text: 'Update',
          handler: data => {
            if(data.displayName) {
              this.usersCred.displayName = data.displayName;
              this.userProvider.updateDisplayName(this.usersCred)
              .then( res => {
                this.alert.showAlert('Edit Display Name', 'Display name successfully updated!');
              })
              .catch( err => {
                this.alert.showAlert('Edit Display Name', 'Error: Something went wrong.<br>Please check your connections and try again.');

              });
            }
          }
        }]
    });
    alert.present();
  }

  logoutConfirmation() {
    let alert = this.alertCtrl.create({
      title: 'Logging Out',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Logout',
          handler: () => {
            
            this.userProvider.cleanDeviceTokenLogout()
            .then( res => {
              this.auth.loggingOut()
              .then( res => {
                this.navCtrl.parent.parent.setRoot(HomePage);
              })
              .catch( err => {
                this.alert.showAlert('Authentication', 'Error Logging Out. Please check your connections and try again!');
              });
            })
            .catch( err => {
              this.alert.showAlert('Authentication', 'Error Logging Out. Please check your connections and try again!');
            });
          }
        }
      ]
    });
    alert.present();
  }

  friendList() {
    this.navCtrl.parent.parent.push(FriendsPage);
  }

  logout() {
    this.logoutConfirmation();
  }

  editImage() {
    this.presentActionSheet();

  }

}

import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, AlertController } from 'ionic-angular';
import { UserCredentials } from '../../models/users/users.model';
import { Observable } from 'rxjs/Observable';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';
import { Friends } from '../../models/friends/friends.model';
import { FriendsProvider } from '../../providers/friends/friends';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { ToastProvider } from '../../providers/toast/toast';
import { SelectedprofilePage } from '../selectedprofile/selectedprofile';
/**
 * Generated class for the FindfriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-findfriends',
  templateUrl: 'findfriends.html',
})
export class FindfriendsPage {
	@ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides;
	title: string = '';
	hideSearch: boolean = false;
	SwipedTabsIndicator: any = null;
	tabs: any = [];
	userList$: UserCredentials[] = [];
  tempUserList$: UserCredentials[] = [];
  currentUserId: string = this.auth.getCurrentUserID();
  sentreqList: FriendRequest[] = [];
	receivereqList: FriendRequest[] = [];
	friendList: Friends[] = [];
	friendRequest = {} as FriendRequest;

	sentreqList$: Observable<UserCredentials[]>;
  	receivereqList$: Observable<UserCredentials[]>;
  	sentReqListNum: number = 0;
  	receiveReqListNum: number = 0;
  	friend = {} as Friends;
  	searchString: string = '';
  	hideList: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private auth: AuthProvider, private zone: NgZone, private friendreqProvider: FriendreqProvider, private friendProvider: FriendsProvider, private toast: ToastProvider, public alertCtrl: AlertController) {
  	this.tabs = ["Find Friends", "Request"];

  	this.sentreqList$ = this.friendreqProvider.getAllSentRequest();
    this.receivereqList$ = this.friendreqProvider.getAllFriendRequest();
    
    this.sentreqList$.subscribe(res => {
      this.sentReqListNum = res.length;
    });

    this.receivereqList$.subscribe(res => {
      this.receiveReqListNum = res.length;
    });

  }

  ionViewWillEnter() {
  	this.hideList = true;
    this.searchString = '';
    this.getAllUserList();
    this.loadRequiredCheckingList();
  }

  getAllUserList() {
    
  	this.userProvider.getAllUser()
  	.snapshotChanges()
  	.subscribe( snap => {
  		var tempObj = [];

      snap.forEach(elem => {
        tempObj.push({
          key: elem.payload.key,
          ...elem.payload.val()
        });
      });

      this.userList$ = tempObj;
      this.tempUserList$ = tempObj;
  	});

  }

  ionViewDidEnter() {
  	this.SwipedTabsIndicator = document.getElementById("indicator");

  	if(this.SwipedTabsSlider.getActiveIndex() == 0) {
  		this.title = 'Find Friends';
  		this.hideSearch = false;
  	} else {
  		this.title = 'Requests';
  		this.hideSearch = true;
  	}
  }

  selectTab(index) {

  	this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
  	this.SwipedTabsSlider.slideTo(index, 500);
  }

  updateIndicatorPosition(num) {

  	if(this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';

  		if(num == 0) {

  			if(this.SwipedTabsSlider.getActiveIndex() == 0) {
  				this.title = 'Find Friends';
  				this.hideSearch = false;
  			} else {
  				this.title = 'Requests';
  				this.hideSearch = true;
  			}
  		}
    }
  }

  animateIndicator($event) {

  	if(this.SwipedTabsIndicator) {
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
    }
  }

  loadRequiredCheckingList() {

    this.friendreqProvider.getSentRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempSentReqList = [];

        snap.forEach(elem => {
          tempSentReqList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.sentreqList = tempSentReqList;
      });
    });

    this.friendreqProvider.getRecRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempRecReqList = [];

        snap.forEach(elem => {
          tempRecReqList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.receivereqList = tempRecReqList;
      });
    });

    this.friendProvider.getFriendList()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempFriendList = [];

        snap.forEach(elem => {
          tempFriendList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.friendList = tempFriendList;
      });
    });
  }

  onClearSearchBar(searchbar): Observable<UserCredentials[]> {
  	this.hideList = true;

  	return;
  }

  searchuser(searchbar): Observable<UserCredentials[]> {
  	if(this.searchString.length == 0) {
  		this.hideList = true;
  	} else {
  		this.hideList = false;
  	}

    this.userList$ = this.tempUserList$;
  	var q = searchbar.target.value;

  	if(q.trim() == '') {
  		return;
  	}

    this.userList$ = this.userList$.filter( result => {
      if((result.displayName.toLowerCase().indexOf(q.toLowerCase())) > -1) {
        return true;
      }
      return false;
    });
  }

  sendReq(requestedUser: UserCredentials) {
  	this.friendreqProvider.submitFriendReq(requestedUser.key, this.friendRequest)
    .then( res => {
      this.toast.showToast('You have successfully send a friend request to ' + requestedUser.displayName);
    })
    .catch( err => {
      this.toast.showToast('Something went wrong, please try again later.');
    });

  }

  viewProfile(key) {
  	this.navCtrl.push(SelectedprofilePage, {
  		key: key
  	});
  }

  showConfirm(key, displayName) {
    let confirm = this.alertCtrl.create({
      title: 'Decline Friend Request?',
      message: 'Are you sure you wanted to decline this friend request?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            
            this.friendreqProvider.cancelReceiveFriendRequest(key)
            .then( res => {
              this.toast.showToast("You have declined " + displayName + "'s friends request.");
            })
            .catch( err => {
              this.toast.showToast('Something went wrong. Please try again later!');
            });
          }
        }
      ]
    });
    confirm.present();
  }

  acceptFriendReq(key, displayName) {
    this.friendreqProvider.cancelReceiveFriendRequest(key)
    .then( res => {
      this.friendreqProvider.acceptFriendRequest(key,this.friend)
      .then( res => {
        this.toast.showToast("You're now friends with " + displayName);
      })
      .catch( err => {
        this.toast.showToast('Something went wrong. Please try agian later!');
      })
    })
    .catch( err => {
      this.toast.showToast('Something went wrong. Please try again later!');
    })
  }

  cancelSentFRequest(key) {

    this.friendreqProvider.cancelSentFriendRequest(key)
    .then( res => {
      this.toast.showToast("You have cancel this sent friends request.");
    })
    .catch( err => {
      this.toast.showToast("Something went wrong. Please try again later!");
    });
  }

  denyFriendReq(key, displayName) {
    this.showConfirm(key, displayName);
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindfriendsPage } from './findfriends';

@NgModule({
  declarations: [
    FindfriendsPage,
  ],
  imports: [
    IonicPageModule.forChild(FindfriendsPage),
  ],
})
export class FindfriendsPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupprofilePage } from './groupprofile';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    GroupprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(GroupprofilePage),
    IonicImageLoader
  ],
})
export class GroupprofilePageModule {}

import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { GroupProvider } from '../../providers/group/group';
import { Group } from '../../models/group/group.model';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { AuthProvider } from '../../providers/auth/auth';
import { Observable } from 'rxjs/Observable';
import { AddmembertogroupPage } from '../addmembertogroup/addmembertogroup';
import { CameraProvider } from '../../providers/camera/camera';
import { ToastProvider } from '../../providers/toast/toast';
import { AlertProvider } from '../../providers/alert/alert';
import { ChatgroupPage } from '../chatgroup/chatgroup';
import { ProjectdashboardPage } from '../projectdashboard/projectdashboard';

/**
 * Generated class for the GroupprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groupprofile',
  templateUrl: 'groupprofile.html',
})
export class GroupprofilePage {

	selectedGroupID: string = '';
	admin: boolean = false;
	group = {} as Group;
	user = {} as UserCredentials;
	groupMemberList$: Observable<UserCredentials[]>;
	adminID: string = '';
	currentUserId: string = '';
	totalNoOfMem: number = 0;
	imgUrl: any;
  isMember: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private groupProvider: GroupProvider, private userProvider: UserProvider, private authProvider: AuthProvider, private actionSheetCtrl: ActionSheetController, private cameraProvider: CameraProvider, public alertCtrl: AlertController, private toast: ToastProvider, private alert: AlertProvider, public zone: NgZone) {
  	this.selectedGroupID = this.navParams.get('key');
  	this.currentUserId = this.authProvider.getCurrentUserID();

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupID)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {
	  		this.group = snap;
	  		this.userProvider.getSelectedUsersDetails(snap.admin)
	  		.valueChanges()
	  		.subscribe(snapS => {
	  			this.user = snapS;
	  		});
        
        this.adminID = snap.admin;

	  		if(snap.admin == this.currentUserId) {
	  			this.admin = true;
	  		}
	  	}
  	});


    /*this.groupProvider.getJoinedGroupList()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run( () => {

        this.isMember = false;

        snap.forEach(elem => {
          if(elem.payload.key == this.selectedGroupID) {
            this.isMember = true;
          }
        });

        if(!this.isMember) {
          this.alert.showAlert('Group', 'You have been removed from the group');
          this.navCtrl.setRoot(TabsPage);
        }
      });
    });*/
    

  	this.groupMemberList$ = this.groupProvider.getGroupMembersList(this.selectedGroupID);
  	this.groupMemberList$.subscribe(snap => {
  		this.totalNoOfMem = snap.length;
  	});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupprofilePage');
  }

  backToTabs() {
  	this.navCtrl.setRoot(TabsPage);
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.updateGroupPhoto(this.imgUrl);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.updateGroupPhoto(this.imgUrl);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  showConfirm(group, option: number) {

  	var title = '';
  	var message = '';
  	var choices = '';

  	if(option == 1) {
  		title = 'Delete Group?';
  		message = 'Are you sure you want to delete this group?';
  		choices = 'Delete';
  	} else {
  		title = 'Leave Group?';
  		message = 'Are you sure you want to leave this group?';
  		choices = 'Leave';
  	}

    let confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'No',
          handler: () => {
            
          }
        },
        {
          text: choices,
          handler: () => {
            

            switch (option) {
            	case 1:
            		
            		this.groupProvider.leaveDeleteSelectedGroup(group)
    				    .then( res => {
    				    	this.toast.showToast('You have successfully deleted the group!');
    				    	this.navCtrl.setRoot(TabsPage);
    				    })
    				    .catch( err => {
    				    	
    				    	this.alert.showAlert('Delete Group', 'Something went wrong, please try again later!');
    				    });
            		break;

            	case 2:
            		
            		this.groupProvider.leaveDeleteSelectedGroup(group)
    				    .then( res => {
    				    	
    				    	this.toast.showToast('You have successfully left the group!');
    				    	this.navCtrl.setRoot(TabsPage);
    				    })
    				    .catch( err => {
    				    	
    				    	this.alert.showAlert('Leave Group', 'Something went wrong, please try again later!');
    				    });
            		break;
            	
            	default:
            		
            		break;
            }
          }
        }
      ]
    });
    confirm.present();
  }

  addMemberToGroup() {
  	this.navCtrl.push(AddmembertogroupPage, {key: this.selectedGroupID});
  }

  removeSelectedMember(key, selectedUserName) {

  	this.groupProvider.removeSelectedUserFromGroup(key, this.selectedGroupID)
  	.then( res => {
  		this.toast.showToast(selectedUserName + ' has been removed from the group.');
  		
  	})
  	.catch( err => {
  		this.alert.showAlert('Remove Member', 'Something went wrong, please try again later!');
  		console.log(err);
  	});
  }

  changePhotoOptions() {
  	this.presentActionSheet();
  }

  updateGroupPhoto(imgResult) {
  	this.groupProvider.updateGroupProfileImages(imgResult, this.group, this.selectedGroupID)
  	.then( res => {
  		console.log(res);
  	})
  	.catch( err => {
  		console.log(err);
  	});
  }

  deleteGroup() {
  	var group = {};

  	group['group/' + this.selectedGroupID ] = {}

    this.groupProvider.getGroupMembersKeyList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
    	snap.forEach(elem => {
    		
    		group['groupMember/' + this.selectedGroupID + '/' + elem.payload.key ] = {}
    		group['users/' + elem.payload.key + '/joinedGroup/' + this.selectedGroupID ] = {}
        group['chatGroupMessages/' + this.selectedGroupID ] = {}
    	});
    });

    this.showConfirm(group, 1);
  }

  leaveGroup() {
  	var group = {};

  	group['users/' + this.authProvider.getCurrentUserID() + '/joinedGroup/' + this.selectedGroupID ] = {}
  	group['groupMember/' + this.selectedGroupID + '/' + this.authProvider.getCurrentUserID() ] = {} 

  	this.showConfirm(group, 2);
  }

  groupChat() {
  	this.navCtrl.push(ChatgroupPage, {key: this.selectedGroupID});
  }

  goToDashboard() {
    this.navCtrl.push(ProjectdashboardPage, {key: this.selectedGroupID});
  }
}

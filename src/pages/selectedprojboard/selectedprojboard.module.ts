import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedprojboardPage } from './selectedprojboard';

@NgModule({
  declarations: [
    SelectedprojboardPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectedprojboardPage),
  ],
})
export class SelectedprojboardPageModule {}

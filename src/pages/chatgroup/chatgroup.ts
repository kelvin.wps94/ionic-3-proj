import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ActionSheetController } from 'ionic-angular';
import { CameraProvider } from '../../providers/camera/camera';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { ChatGroupMessages } from '../../models/chatgroupmessages/chatgroupmessages.model';
import { GroupProvider } from '../../providers/group/group';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../../providers/auth/auth';
import { TabsPage } from '../tabs/tabs';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the ChatgroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatgroup',
  templateUrl: 'chatgroup.html',
})
export class ChatgroupPage {

	selectedGroupName: string = '';
	selectedGroupPhotoURL: string = '';
	message: string = '';
	ChatGroupMsg = {} as ChatGroupMessages;
	totalNumOfMsg: number = 0;
	loopCount = 0;
	selectedGroupID: string = '';
	imgUrl: any;
	chatMessagesList$: Observable<ChatGroupMessages[]>;
	ownUserID: string = '';
  ownName: string = '';
	loopCountSenderName = 0;
	totalNumOfMsgCount: number = 0;
	userList$: Observable<ChatGroupMessages[]>;
  groupMemberList: object[] = [];
  admin: boolean = false;
  loaded: boolean = false;

  @ViewChild(Content) content:Content;
  constructor(private alert: AlertProvider, private cameraProvider: CameraProvider, public actionSheetCtrl: ActionSheetController, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private chatSingleProvider: ChatmsgProvider, private groupProvider: GroupProvider, private authProvider: AuthProvider) {
  	this.selectedGroupID = this.navParams.get('key');

  	this.ownUserID = this.authProvider.getCurrentUserID();
    this.ownName = this.authProvider.getCurrentUserName();

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupID)
  	.valueChanges()
  	.subscribe(snapshot => {
  		this.zone.run( () => {
  			this.selectedGroupName = snapshot.groupName;
  			this.selectedGroupPhotoURL = snapshot.groupPhotoURL;
        this.loaded = true;

        if(snapshot.admin == this.ownUserID) {
          this.admin = true;
        }
  		});
  	});

    /*if(!this.admin) {
      this.groupProvider.getJoinedGroupList()
      .snapshotChanges()
      .subscribe(snap => {
        this.zone.run( () => {
          var found = false;

          snap.forEach(elem => {
            if(elem.payload.key == this.selectedGroupID) {
              found = true;
            }
          });

          if(!found) {
            this.alert.showAlert('Group', 'You have been removed from the group');
            this.navCtrl.setRoot(TabsPage);
          }
        });
      });
    }*/

  	this.loadMessages();
    this.loadMemberList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatgroupPage');
  }

  ionViewWillLeave() {
    this.loaded = false;
  }

  backTo() {
  	this.navCtrl.pop();
  }

  loadMessages() {
    this.chatMessagesList$ = this.chatSingleProvider
    .getGroupChatMessages(this.selectedGroupID)
    .snapshotChanges()
    .map( changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.userList$ = this.userProvider
    .getAllUser()
    .snapshotChanges()
    .map( changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }

  scrollAfterFewSecond() {
    setTimeout(() => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    }, 100);
  }

  scrolltoBottom() {
    setTimeout( () => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    });
  }

  scroll(last, index) {

    if(this.totalNumOfMsg < (index + 1)) {
      this.loopCount = 0;

      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }

    } else {
    
      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
          
        }
      }
    }

    this.loopCount++;
  }

  presentActionSheet(chatGroupMsg: ChatGroupMessages) {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatGroupMsg);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatGroupMsg);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  sendMessage(chatGroupMsg: ChatGroupMessages) {
    
    var groupMemberKey = {};
    var msgKey = '';

    if(this.message != '') {

      chatGroupMsg.message = this.message;
      chatGroupMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      chatGroupMsg.type = 'messages';
      chatGroupMsg.tempName = this.ownName;

      msgKey = this.chatSingleProvider.sendChatGroupMessages(chatGroupMsg, this.selectedGroupID);

      this.groupMemberList.forEach(elem => {
        groupMemberKey['notifications/' + elem + '/' + msgKey ] = {
          from: this.selectedGroupID,
          type: 'chatGroup'
        }
      });

      this.chatSingleProvider.sendGroupNotifications(groupMemberKey)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });

      this.message = '';
      this.scrolltoBottom();
      this.loadMemberList();
    }
  }

  loadMemberList() {
    var tempMemList = [];

    this.groupProvider.getGroupMembersKeyList(this.selectedGroupID)
    .snapshotChanges()
    .subscribe(snap => {
      snap.forEach(elem => {
        if(elem.payload.key != this.ownUserID) {
          tempMemList.push(elem.payload.key);
        }
      });
    });

    this.groupMemberList = tempMemList;
  }

  openOptions(chatGroupMsg: ChatGroupMessages) {
    this.presentActionSheet(chatGroupMsg);
  }

  sendImage(imgResult, chatGroupMsg: ChatGroupMessages) {

    var groupMemberKey = {};
    var msgKey: string = '';

    chatGroupMsg.message = this.message;
    chatGroupMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
    chatGroupMsg.type = 'images';

    this.chatSingleProvider.sendChatGroupMessagesWithImage(chatGroupMsg, this.selectedGroupID, imgResult)
    .then( (res:any) => {
      msgKey = res;

      this.groupMemberList.forEach(elem => {
        groupMemberKey['notifications/' + elem + '/' + msgKey ] = {
          from: this.selectedGroupID,
          type: 'chatGroup'
        }
      });

      this.chatSingleProvider.sendGroupNotifications(groupMemberKey)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });
    })
    .catch( err => {
      console.log(err);
    });

    this.message = '';
    this.scrolltoBottom();
    this.loadMemberList();
  }

}

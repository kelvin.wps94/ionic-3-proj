import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatgroupPage } from './chatgroup';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ChatgroupPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatgroupPage),
    IonicImageLoader
  ],
})
export class ChatgroupPageModule {}

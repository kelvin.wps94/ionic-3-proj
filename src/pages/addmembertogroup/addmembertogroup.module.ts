import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddmembertogroupPage } from './addmembertogroup';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    AddmembertogroupPage,
  ],
  imports: [
    IonicPageModule.forChild(AddmembertogroupPage),
    IonicImageLoader
  ],
})
export class AddmembertogroupPageModule {}

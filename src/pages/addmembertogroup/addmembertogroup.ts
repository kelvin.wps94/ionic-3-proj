import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { FriendsProvider } from '../../providers/friends/friends';
import { UserProvider } from '../../providers/user/user';
import { Friends } from '../../models/friends/friends.model';
import { UserCredentials } from '../../models/users/users.model';
import { GroupProvider } from '../../providers/group/group';
import firebase from 'firebase';
/**
 * Generated class for the AddmembertogroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addmembertogroup',
  templateUrl: 'addmembertogroup.html',
})
export class AddmembertogroupPage {

	selectedGroupID: string = '';

	friendList$: Observable<Friends[]>;
	userList$: Observable<UserCredentials[]>;
	groupMemberList: object[] = [];
  fireDB = firebase.database().ref('/');
	checkedCount: number = 0;

	storedKey: object[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private friendsProvider: FriendsProvider, private groupProvider: GroupProvider) {
  	this.selectedGroupID = this.navParams.get('key');
  	this.getAllFriendsList();
  	this.loadExistingMemberList();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AddmembertogroupPage');
  }

  backTo() {
  	this.navCtrl.pop();
  }

  filterExistingUser(key) {
  	var found: boolean = false;
  	
  	if(this.groupMemberList.length > 0) {
  		this.groupMemberList.forEach( (item, index) => {
  			if(item == key) {
  				found = true;
  			} 
  		});

  		if(found) {
  			return true
  		} else {
  			return false;
  		}
  	} else {
  		return false;
  	}
  }

  addUserToList(key) {
  	var found: boolean = false;
  	var foundIndex: number;

  	if(this.storedKey.length > 0) {
  		this.storedKey.forEach( (item, index) => {
  			if(item == key) {
  				found = true;
  				foundIndex = index;
  			} 
  		});

  		if(found) {
  			this.storedKey.splice(foundIndex, 1);
  		} else {
  			this.storedKey.push(key);
  		}
  	} else {
  		this.storedKey.push(key);
  	}

  	this.checkedCount = this.storedKey.length;
  }

  checking(key) {
  	var found: boolean = false;
  	
  	if(this.storedKey.length > 0) {
  		this.storedKey.forEach( (item, index) => {
  			if(item == key) {
  				found = true;
  			} 
  		});

  		if(found) {
  			return true
  		} else {
  			return false;
  		}
  	} else {
  		return false;
  	}
  }

  submitAddMemberList() {
  	var newMemberList = {};
  	this.storedKey.forEach(elem => {
  		newMemberList['users/' + elem + '/joinedGroup/' + this.selectedGroupID ] = {
  			hasThisGroup: true
  		}

  		newMemberList['groupMember/' + this.selectedGroupID + '/' + elem] = {
  			isMember: true
  		}

      newMemberList['notifications/' + elem + '/' + this.fireDB.push().key ] = {
          from: this.selectedGroupID,
          type: 'groupJoined'
      }
  	});

  	this.groupProvider.addNewMember(newMemberList)
  	.then( res => {
  		this.navCtrl.pop();
  		console.log(res);
  	})
  	.catch( err => {
  		console.log(err);
  	});

  }

  getAllFriendsList() {
  	this.friendList$ = this.friendsProvider.getFriendList()
  	.snapshotChanges()
  	.map(changes => {
  		return changes.map(c => ({
  			key: c.payload.key,
  			...c.payload.val()
  		}));
  	});

  	this.userList$ = this.userProvider.getAllUser()
  	.snapshotChanges()
  	.map(changes => {
  		return changes.map(c => ({
  			key: c.payload.key,
  			...c.payload.val()
  		}));
  	});
  }

  searchuser(searchbar): Observable<UserCredentials[]> {
  	this.getAllFriendsList();
  	var q = searchbar.target.value;

  	if(q.trim() == '') {
  		return;
  	}

  	this.userList$ = this.userList$.map( res => {
  		return res.filter( result => {
  			return result.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1;
  		});
  	})
  }

  loadExistingMemberList() {
  	var tempMemList = [];
  	this.groupProvider.getGroupMembersKeyList(this.selectedGroupID)
  	.snapshotChanges()
  	.subscribe(snap => {
  		snap.forEach(elem => {
  			tempMemList.push(elem.payload.key);
  		});
  	});

  	this.groupMemberList = tempMemList;
  }

}

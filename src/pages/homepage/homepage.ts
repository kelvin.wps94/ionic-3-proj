import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
/**
 * Generated class for the HomepagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homepage',
  templateUrl: 'homepage.html',
})
export class HomepagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private chatMsgProvider: ChatmsgProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomepagePage');
  }

}

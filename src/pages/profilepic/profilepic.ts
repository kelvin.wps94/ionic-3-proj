import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, ActionSheetController } from 'ionic-angular';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { UserCredentials } from '../../models/users/users.model';
import { UserProvider } from '../../providers/user/user';
import { TabsPage } from '../tabs/tabs'; 
import { AlertProvider } from '../../providers/alert/alert';
import { CameraProvider } from '../../providers/camera/camera';
import { FirebasestorageProvider } from '../../providers/firebasestorage/firebasestorage';

/**
 * Generated class for the ProfilepicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profilepic',
  templateUrl: 'profilepic.html',
})
export class ProfilepicPage {

	imgurl = 'https://firebasestorage.googleapis.com/v0/b/login-chat-app-63063.appspot.com/o/profileImages%2Fdefaultavatar%2Fmedium-default-avatar.png?alt=media&token=bc558370-c9c8-4b11-be08-431d6b6fc1f8';
	moveon = true;

	newUser = {} as UserCredentials;

  constructor(private fireStore: FirebasestorageProvider ,public navCtrl: NavController, public navParams: NavParams, public imgProvider: ImagehandlerProvider, public zone: NgZone, public userProvider: UserProvider, public loadingCtrl: LoadingController, public plt: Platform, private alert: AlertProvider, public actionSheetCtrl: ActionSheetController, private cameraProvider: CameraProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilepicPage');
    this.newUser = this.navParams.get('newUser');
    
  }

  presentActionSheet() {
    let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });

    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                loader.dismiss();
                this.imgurl = url;
                this.moveon = false;
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Upload Profile Photo','Something went wrong. Please try again!');
              })
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Upload Profile Photo','Something went wrong. Please try again!');
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                loader.dismiss();
                this.imgurl = url;
                this.moveon = false;
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Upload Profile Photo','Something went wrong. Please try again!');
              })
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Upload Profile Photo','Something went wrong. Please try again!');
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  chooseImage() {
    this.presentActionSheet();
  }

  proceed() {
  	this.navCtrl.setRoot(TabsPage);
  }

  updateProceed() {
  	this.userProvider.updateimage(this.imgurl, this.newUser)
  	.then( (res:any) => {
  		this.navCtrl.setRoot(TabsPage);
  	})
    .catch( err => {
      this.alert.showAlert('Updating Profile Pictures', 'Something went wrong when updating images. Please try again!');
    });
  }

}

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ModalController, Select, Slides } from 'ionic-angular';
import { ProjectBoard } from '../../models/projectboard/projectboard.model';
import { ProjectboardProvider } from '../../providers/projectboard/projectboard';
import { Observable } from 'rxjs/Observable';
import { Task } from '../../models/task/task.model';
import { TasksectionProvider } from '../../providers/tasksection/tasksection';
import { AuthProvider } from '../../providers/auth/auth';
import { GroupProvider } from '../../providers/group/group';
import { TaskSection } from '../../models/tasksection/tasksection.model';
import { AlertProvider } from '../../providers/alert/alert';
import { ToastProvider } from '../../providers/toast/toast';
import { TaskProvider } from '../../providers/task/task';
import { SelectedtaskPage } from '../selectedtask/selectedtask';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import { StaskPage } from '../stask/stask';
/**
 * Generated class for the SelectedprojPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selectedproj',
  templateUrl: 'selectedproj.html',
})
export class SelectedprojPage {

	@ViewChild('taskNameInput') taskNameInput;
	@ViewChild(Slides) slides: Slides;

	selectedGroupKey: string = '';
	selectedBoardKey: string = '';

	board = {} as ProjectBoard;

	taskSection: TaskSection = {
		taskSectionTitle: '',
		isTaskCompletedSection: false
	};

	task: Task = {
		taskName: '',
		taskDue: false,
		subTask: false,
		poll: false,
		assignedMember: false,
    	priority: 'none'
	}

	activityLog: ActivityLog = {
	    activityType: '',
	    handler: '',
	    handlerIsAdmin: false,
	    dateNTimeHandled: '',
	    logActivity: ''
	}

	tasks = {} as Task;

	taskList$: Observable<Task[]>;
	isAdmin: boolean = false;
	currentUserId: string = '';
	addSectionPending: boolean = false;
	taskAddPending: boolean = false;
	hideComponent: boolean = true;
	isPriviledgeUser: boolean = false;
	selectedTaskKey: string = '';
	selectedTaskSectKey: string = '';
	selectedTaskSectTitle: string = '';
	selectedTaskSectIsCompletedSection: boolean = false;
	indexes: number = 0;
	selectedTaskSectionKey: TaskSection;

	@ViewChild('taskSectionList') taskSectLists: Select;
  constructor(public navCtrl: NavController, public navParams: NavParams, private tasksecProvider: TasksectionProvider, public actionSheetCtrl: ActionSheetController, private authProvider: AuthProvider, private groupProvider: GroupProvider, private projBoardProvider: ProjectboardProvider, private alert: AlertProvider, private toast: ToastProvider, public alertCtrl: AlertController, private taskProvider: TaskProvider, public modal: ModalController) {
  	this.selectedGroupKey = this.navParams.get('groupKey');
  	this.selectedBoardKey = this.navParams.get('boardKey');
  	this.currentUserId = this.authProvider.getCurrentUserID();

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupKey)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {

	  		if(snap.admin == this.currentUserId) {
	  			this.isAdmin = true;
	  			console.log(this.isAdmin);
	  		}
	  	}
  	});

  	this.projBoardProvider.getSelectedBoardDetails(this.selectedGroupKey, this.selectedBoardKey)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {
  			this.board = snap;
  		}
  	});

  	this.taskList$ = this.tasksecProvider.getTaskSectionAndTaskList(this.selectedBoardKey)
  	.snapshotChanges()
  	.map(changes => {
  		return changes.map(c => ({
  			key: c.payload.key,
  			...c.payload.val()
  		}));
  	});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectedprojPage');
  }

  backBtn() {
  	this.navCtrl.pop();
  }

  addSections() {
  	this.addSectionPending = true;
  }

  moreOptions(taskSectKey, sectionTitle, taskSect: TaskSection) {
    this.presentActionSheet(taskSectKey, sectionTitle, taskSect);
  }

  presentActionSheet(taskSectKey, sectionTitle, taskSect: TaskSection) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Task Sections Options',
      buttons: [
        {
          text: 'Remove This Section',
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            this.showConfirm(taskSectKey, sectionTitle, taskSect);
          }
        },{
          text: 'Rename This Section',
          icon: 'create',
          handler: () => {
            this.showPrompt(taskSectKey, taskSect);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  showConfirm(taskSectKey, sectionTitle, taskSect: TaskSection) {
    let confirm = this.alertCtrl.create({
      title: "Remove '" + sectionTitle + "' Section",
      message: 'Are you sure you want to remove this section?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Remove',
          handler: () => {
            this.board.hasTaskCompleteSection = false;

            if(taskSect.isTaskCompletedSection == true) {

              this.projBoardProvider.updateHasTaskCompletedSection(this.selectedGroupKey, this.selectedBoardKey, this.board)
              .then( res => {

                console.log('hasTaskCompletedSection Updated: ' + this.board.hasTaskCompleteSection);

                this.tasksecProvider.removeTaskSection(this.selectedBoardKey, taskSectKey)
                .then( () => {
                  this.toast.showToast("Task Section '" + sectionTitle + "' has been removed.");
                })
                .catch( err => {
                  this.alert.showAlert('Remove Task Section', err);
                });

              })
              .catch( err => {
                this.alert.showAlert('Remove Task Section', err);
              });

            } else {

              this.tasksecProvider.removeTaskSection(this.selectedBoardKey, taskSectKey)
              .then( () => {
                this.toast.showToast("Task Section '" + sectionTitle + "' has been removed.");
              })
              .catch( err => {
                this.alert.showAlert('Remove Task Section', err);
              });

            }
          }
        }
      ]
    });
    confirm.present();
  }

  showPrompt(taskSectKey, taskSect: TaskSection) {
    let prompt = this.alertCtrl.create({
      title: 'Rename Section',
      message: "Enter a new title if you wished to change the title of this section",
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Rename',
          handler: data => {

            if(data.title != taskSect.taskSectionTitle) {
              
              var oldTitle = taskSect.taskSectionTitle;

              taskSect.taskSectionTitle = data.title;
              this.tasksecProvider.renameTaskSection(this.selectedBoardKey, taskSectKey, taskSect)
              .then( res => {
                this.toast.showToast('Task Section [ ' + oldTitle + ' ] has been renamed to [ ' + taskSect.taskSectionTitle + ' ]' )
              })
              .catch( err => {
                this.alert.showAlert('Rename Task Section', err);
              });
            }
          }
        }
      ]
    });
    prompt.present();
  }

  addOrCancelSection(choices, taskSect: TaskSection) {

  	if(choices == 'add') {

  		if(taskSect.taskSectionTitle != '') {

  			this.tasksecProvider.addNewTaskSection(this.selectedBoardKey, taskSect)
  			.then( res => {
  				if(taskSect.isTaskCompletedSection) {
  					this.board.hasTaskCompleteSection = true;

  					this.projBoardProvider.updateHasTaskCompletedSection(this.selectedGroupKey, this.selectedBoardKey, this.board)
  					.then( res => {
  						this.toast.showToast('[' + taskSect.taskSectionTitle + '] is added to the task section.');
  						this.taskSection.taskSectionTitle = '';
  						this.taskSection.isTaskCompletedSection = false;
  					})
  					.catch( err => {
  						this.alert.showAlert('Add Task Section', 'Something went wrong. Please try again later!');
  					});
  				} else {
  					this.toast.showToast('[' + taskSect.taskSectionTitle + '] is added to the task section.');
  					this.taskSection.taskSectionTitle = '';
  					this.taskSection.isTaskCompletedSection = false;
  				}
  			})
  			.catch(err => {
  				this.alert.showAlert('Add Task Section', 'Something went wrong. Please try again later!');
  			});

  			
  			this.addSectionPending = false;
  		} else {
  			this.taskSection.taskSectionTitle = '';
  			this.alert.showAlert('Add Task Section', 'Task Title Cannot be empty! Please fill in the requirements.');
  		}
  		
  	} else {
  		this.addSectionPending = false;
  	}

  }

  addTask() {
  	this.taskAddPending = true;

  	setTimeout(() => {
        this.taskNameInput.setFocus();
      },150);
  }

  addOrCancelTask(choices, taskSectKey, task: Task) {

  	if(choices == 'add') {

  		if(task.taskName != '') {

  			this.taskProvider.addTask(this.selectedBoardKey, taskSectKey, task)
  			.then( res => {
  				this.toast.showToast('[' + task.taskName + '] has been added.');
  				this.task.taskName = '';
  				this.taskAddPending = false;
          this.board.totalTask = this.board.totalTask + 1;

          this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
          .then( res => {
            console.log(res);
          })
          .catch( err => {
            console.log(err);
          });
  			})
  			.catch( err => {
  				this.alert.showAlert('Add Task', 'Something went wrong. Please try again later!');
  				this.task.taskName = '';
  			});
  		} else {
  			this.alert.showAlert('Add Task', 'Task Name cannot be empty! Please fill in the requirements.');
  		}
  
  	} else {
  		this.task.taskName = '';
  		this.taskAddPending = false;
  	}
  }

  showSelectedTaskModal(taskSectKey, taskKey) {
  	const selectedTaskModal = this.modal.create(StaskPage, {
      groupKey: this.selectedGroupKey,
  		boardKey: this.selectedBoardKey,
  		taskSectKey: taskSectKey,
  		taskKey: taskKey
  	});

  	selectedTaskModal.present();
  }

  selectedTaskOptions(taskSectionKey: TaskSection, taskKey, tasks: Task) {
  	console.log('ehhh');
    this.selectedTaskKey = taskKey;
    this.selectedTaskSectKey = taskSectionKey.key;
    this.selectedTaskSectTitle = taskSectionKey.taskSectionTitle;
    this.selectedTaskSectIsCompletedSection = taskSectionKey.isTaskCompletedSection;
    this.tasks.taskName = tasks.taskName;
    this.tasks.taskDue = tasks.taskDue;
    this.tasks.subTask = tasks.subTask;
    this.tasks.priority = tasks.priority;
    this.tasks.poll = tasks.poll;
    this.tasks.assignedMember = tasks.assignedMember;

    let adminActionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Move Task',
          icon: 'move',
          handler: () => {
            
            this.taskSectLists.open();

          }
        },{
          text: 'Rename Task',
          icon: 'create',
          handler: () => {

            this.renameTask(taskSectionKey.key, taskKey, tasks);

          }
        },{
          text: 'Delete Task',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            var task = {};

            task['task/' + taskKey ] = {}
            task['taskSection/' + this.selectedBoardKey + '/' + taskSectionKey.key + '/task/' + taskKey] = {}

            this.taskProvider.firebaseCRUD(task)
            .then(res => {
              this.toast.showToast('Task has been removed.');
              
              if(this.taskSection.isTaskCompletedSection) {
                this.board.totalCompletedTask = this.board.totalCompletedTask - 1;
                this.board.totalTask = this.board.totalTask - 1;

                this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
                .then( res => {
                  console.log(res);
                })
                .catch( err => {
                  console.log(err);
                });

              } else {

                this.board.totalTask = this.board.totalTask - 1;

                this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
                .then( res => {
                  console.log(res);
                })
                .catch( err => {
                  console.log(err);
                });
              }
            })
            .catch(err => {
              this.alert.showAlert('Remove Task', err);
            })
            
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Move Task',
          icon: 'move',
          handler: () => {
            
            this.taskSectLists.open();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    
    

    this.taskProvider.getAssignedMemList(taskKey)
    .snapshotChanges()
    .subscribe(snap => {
      if(snap) {
        var found = false;

        if(snap.length > 0) {
          snap.forEach(elem => {
            if(elem.payload.key == this.currentUserId) {
              found = true;
            }
          });

          this.isPriviledgeUser = found;

        } else {
          this.isPriviledgeUser = true;
        }

        if(this.isAdmin && !this.isPriviledgeUser) {
          adminActionSheet.present();
        } else if (!this.isAdmin && this.isPriviledgeUser) {
          actionSheet.present();
        } else if (this.isAdmin && this.isPriviledgeUser) {
          adminActionSheet.present();
        }
      }
    });
  }

  renameTask(taskSectionKey, taskKey, tasks: Task) {
    var tempName;

    let prompt = this.alertCtrl.create({
      title: 'Rename Task',
      message: "Enter a new name for this task.",
      inputs: [
        {
          name: 'taskName',
          placeholder: 'Task Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'Rename',
          handler: data => {
            tempName = tasks.taskName;
            tasks.taskName = data.taskName;

            this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, taskSectionKey, taskKey, tasks)
            .then( res => {
              this.toast.showToast("'" + tempName + "' task has been renamed successfully to '" + data.taskName + "'");
            })
            .catch( err => {
              this.alert.showAlert('Rename Task', err);
            });
          }
        }
      ]
    });
    prompt.present();
  }

  getIndex(index) {
    this.indexes = index;
  }

  selectedTaskSect() {
    var task = {};
    var taskSectTempName1 = '';
    var taskSectTempName2 = '';

    taskSectTempName1 = this.selectedTaskSectTitle;
    taskSectTempName2 = this.selectedTaskSectionKey.taskSectionTitle;

    task['taskSection/' + this.selectedBoardKey + '/' + this.selectedTaskSectionKey.key + '/task/' + this.selectedTaskKey ] = {
      assignedMember: this.tasks.assignedMember,
      poll: this.tasks.poll,
      subTask: this.tasks.subTask,
      taskDue: this.tasks.taskDue,
      taskName: this.tasks.taskName,
      priority: this.tasks.priority
    }

    task['taskSection/' + this.selectedBoardKey + '/' + this.selectedTaskSectKey + '/task/' + this.selectedTaskKey ] = {}


    this.taskProvider.firebaseCRUD(task)
    .then( res => {

      this.activityLog.activityType = 'MoveTaskLog';
      this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      this.activityLog.handler = this.currentUserId;
      this.activityLog.handlerIsAdmin = this.isAdmin;
      this.activityLog.logActivity = "moved this task from '" + taskSectTempName1 + "' to '" + taskSectTempName2 + "'.";

      this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
      .then( r => {
        console.log('Activity has been Logged');
      })
      .catch( e => {
        console.log('Activity Logging: ' + e);
      });

      if(this.selectedTaskSectionKey.isTaskCompletedSection) {

        this.board.totalCompletedTask = this.board.totalCompletedTask + 1;

        this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
        .then( r => {
          console.log(r);
        })
        .catch( e => {
          console.log(e);
        });

      } else {

        if(this.selectedTaskSectIsCompletedSection) {

          this.board.totalCompletedTask = this.board.totalCompletedTask - 1;

          this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
          .then( r => {
            console.log(r);
          })
          .catch( e => {
            console.log(e);
          });
        }
      }
      this.slides.slideTo(this.indexes, 500);

      console.log(res);
    })
    .catch( err => {
      console.log(err);
    });
  }

}

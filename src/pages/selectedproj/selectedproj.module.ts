import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedprojPage } from './selectedproj';

@NgModule({
  declarations: [
    SelectedprojPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectedprojPage),
  ],
})
export class SelectedprojPageModule {}

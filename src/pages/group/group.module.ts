import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupPage } from './group';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    GroupPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupPage),
    IonicImageLoader
  ],
})
export class GroupPageModule {}

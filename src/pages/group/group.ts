import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserlistandrequestPage } from '../userlistandrequest/userlistandrequest';
import { CreategroupPage } from '../creategroup/creategroup';
import { Group } from '../../models/group/group.model';
import { Observable } from 'rxjs/Observable';
import { GroupProvider } from '../../providers/group/group';
import { GroupprofilePage } from '../groupprofile/groupprofile';
import { UserGroup } from '../../models/usergroup/usergroup.model';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';

/**
 * Generated class for the GroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {

  groupList$: Observable<Group[]>;
  joinedGroupList$: Observable<UserGroup[]>;
  receiveReq: number = 0;

  constructor(private friendReqProvider: FriendreqProvider, public navCtrl: NavController, public navParams: NavParams, private groupProvider: GroupProvider) {
    //this.groupList$ = this.groupProvider.getAllGroupList();
    this.getGroupList();
    this.friendReqProvider.getRecRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.receiveReq = snap.length;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupPage');
  }

  allUserListPage() {
    this.navCtrl.parent.parent.push(UserlistandrequestPage);
  }

  createNewGroup() {
    this.navCtrl.parent.parent.push(CreategroupPage);
  }

  openSelectedGroup(key) {
    this.navCtrl.parent.parent.setRoot(GroupprofilePage, {key: key});
  }

  getGroupList() {
    this.joinedGroupList$ = this.groupProvider.getJoinedGroupList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.groupList$ = this.groupProvider.getGroupList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }

  searchuser(searchbar): Observable<Group[]> {
    this.getGroupList();
    var q = searchbar.target.value;

    if(q.trim() == '') {
      return;
    }

    this.groupList$ = this.groupList$.map( res => {
      return res.filter( result => {
        return result.groupName.toLowerCase().indexOf(q.toLowerCase()) > -1;
      });
    })
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { GroupProvider } from '../../providers/group/group';
import { Group } from '../../models/group/group.model';
import { UserGroup } from '../../models/usergroup/usergroup.model';
import { GprofilePage } from '../gprofile/gprofile';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertProvider } from '../../providers/alert/alert';

/**
 * Generated class for the GrouplistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grouplist',
  templateUrl: 'grouplist.html',
})
export class GrouplistPage {

  groupList$: Observable<Group[]>;
  joinedGroupList$: Observable<UserGroup[]>;
  newGroup: Group = {
    groupName: '',
    admin: this.authProvider.getCurrentUserID(),
    groupPhotoURL: 'https://firebasestorage.googleapis.com/v0/b/login-chat-app-63063.appspot.com/o/group_profileImages%2Fgroup.png?alt=media&token=abb54b93-99d1-4395-a1f8-03c58f8f7d14'
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private groupProvider: GroupProvider, public alertCtrl: AlertController, private authProvider: AuthProvider, private alertProvider: AlertProvider) {
    this.getGroupList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GrouplistPage');
  }

  createGroup() {
    const prompt = this.alertCtrl.create({
      title: 'Create New Group',
      message: "Enter a name for this new group",
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Group Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Create',
          handler: data => {
            this.newGroup.groupName = data.groupName;

            if(this.newGroup.groupName != '') {
              this.groupProvider.addNewGroup(this.newGroup)
              .then( (res: any) => {
                this.navCtrl.push(GprofilePage, {key: res});
              })
              .catch( err => {
                this.alertProvider.showAlert('Create Group', 'Something went wrong, please try again later!');
              });
            } else {
              this.alertProvider.showAlert('Create Group', 'Group name cannot be empty. <br>Please fill in the group name.');
            }
          }
        }
      ]
    });
    prompt.present();
  }

  openSelectedGroup(key) {
    this.navCtrl.push(GprofilePage, {key: key});
  }

  getGroupList() {
    this.joinedGroupList$ = this.groupProvider.getJoinedGroupList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.groupList$ = this.groupProvider.getGroupList()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });
  }

}

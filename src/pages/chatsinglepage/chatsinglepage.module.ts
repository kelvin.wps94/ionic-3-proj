import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsinglepagePage } from './chatsinglepage';

@NgModule({
  declarations: [
    ChatsinglepagePage,
  ],
  imports: [
    IonicPageModule.forChild(ChatsinglepagePage),
  ],
})
export class ChatsinglepagePageModule {}

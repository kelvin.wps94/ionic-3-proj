import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ActionSheetController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ChatSingleMessages } from '../../models/chatsinglemessages/chatsinglemessages.model';
import { ChatmsgProvider } from '../../providers/chatmsg/chatmsg';
import { CameraProvider } from '../../providers/camera/camera';
import { SingleChatLastMsg } from '../../models/singlechatlastmsg/singlechatlastmsg.model';
import { AuthProvider } from '../../providers/auth/auth';
import { ImageViewerController } from 'ionic-img-viewer';
import { ImgLoader } from 'ionic-image-loader';

/**
 * Generated class for the ChatsinglepagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatsinglepage',
  templateUrl: 'chatsinglepage.html',
})
export class ChatsinglepagePage {

  	selectedUserID: string = '';
  	selectedUsersName: string = '';
    selectedUserPhotoURL: string = '';
    imgUrl: any;
    _imageViewerCtrl: ImageViewerController;
    ChatSingleMsg = {} as ChatSingleMessages;
    singleLastMsg = {} as SingleChatLastMsg;
    message: string = '';
    todayDate: any;
    ownUserID: string = '';
    allMessages: object[] = [];
    totalNumOfMsg: number = 0;
    loopCount = 0;

  @ViewChild(Content) content:Content;
  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, public zone: NgZone, private chatSingleProvider: ChatmsgProvider, public actionSheetCtrl: ActionSheetController, private cameraProvider: CameraProvider, private authProvider: AuthProvider, private imageViewerCtrl: ImageViewerController) {
  	this.selectedUserID = this.navParams.get('key');

  	this.userProvider.getSelectedUsersDetails(this.selectedUserID)
  	.valueChanges()
  	.subscribe(snapshot => {
  		this.zone.run( () => {
	        this.selectedUsersName = snapshot.displayName;
          this.selectedUserPhotoURL = snapshot.photoURL;
	    });
  	});

    this.ownUserID =  this.authProvider.getCurrentUserID();
  	
    this.loadMessages();
    this.scrollAfterFewSecond();

    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatsinglepagePage');
  }

  backBtn() {
  	this.navCtrl.pop();
  }

  loadMessages() {

    this.chatSingleProvider
    .getChatMessages(this.selectedUserID)
    .snapshotChanges()
    .subscribe(snap => {
      var tempObject: object[] = [];

      snap.forEach(elem => {
        if(elem.key != 'lastMsgDate') {
          tempObject.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        }
      });

      this.allMessages = tempObject;
    });
  }

  scrollAfterFewSecond() {
    setTimeout(() => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    }, 100);
  }

  presentActionSheet(chatSingleMsg: ChatSingleMessages) {
    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatSingleMsg);
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.imgUrl = res;
              this.sendImage(this.imgUrl, chatSingleMsg);
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  scrolltoBottom() {
    setTimeout( () => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    });
  }

  scroll(last, index) {

    if(this.totalNumOfMsg < (index + 1)) {
      this.loopCount = 0;

      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }

    } else {
    
      if(this.loopCount == 0) {

        if(last) {
          this.totalNumOfMsg = index + 1;
          this.scrollAfterFewSecond();
        }
      }
    }

    this.loopCount++;
  }

  sendMessage(chatSingleMsg: ChatSingleMessages, singleLastMsg: SingleChatLastMsg) {

    if(this.message != '') {
      var todayD = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      
      chatSingleMsg.message = this.message;
      chatSingleMsg.type = 'messages';
      chatSingleMsg.msgSentTime = todayD;
      this.chatSingleProvider.sendChatSingleMessages(chatSingleMsg, this.selectedUserID);

      singleLastMsg.lastMsgDate = todayD;
      this.chatSingleProvider.updateSingleChatLastMsg(this.selectedUserID, singleLastMsg)
      .then( res => {
        console.log(res);
      })
      .catch( err => {
        console.log(err);
      });

      this.message = '';
      this.scrolltoBottom();
    }
  }

  sendImage(imgResult, chatSingleMsg: ChatSingleMessages) {

    chatSingleMsg.message = this.message;
    chatSingleMsg.type = 'images';
    chatSingleMsg.msgSentTime = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
    this.chatSingleProvider.sendChatSingleMessagesWithImageFromCamera(chatSingleMsg, this.selectedUserID, imgResult);

    this.message = '';
    this.scrolltoBottom();
  }

  openOptions(chatSingleMsg: ChatSingleMessages) {
    this.presentActionSheet(chatSingleMsg);
  }

}

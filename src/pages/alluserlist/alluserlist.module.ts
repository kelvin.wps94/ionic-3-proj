import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlluserlistPage } from './alluserlist';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AlluserlistPage,
  ],
  imports: [
    IonicPageModule.forChild(AlluserlistPage),
    IonicImageLoader,
    PipesModule
  ],
})
export class AlluserlistPageModule {}

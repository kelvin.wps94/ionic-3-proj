import { Component, NgZone, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { UserCredentials } from '../../models/users/users.model';
import { Observable } from 'rxjs/Observable';
import { TabsPage } from '../tabs/tabs';
import { AuthProvider } from '../../providers/auth/auth';
import { FriendreqProvider } from '../../providers/friendreq/friendreq';
import { FriendRequest } from '../../models/friendrequest/friendrequest.model';
import { AlertProvider } from '../../providers/alert/alert';
import { ToastProvider } from '../../providers/toast/toast';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { FriendsProvider } from '../../providers/friends/friends';
import { Friends } from '../../models/friends/friends.model';

/**
 * Generated class for the AlluserlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alluserlist',
  templateUrl: 'alluserlist.html',
})
export class AlluserlistPage {

	userList$: UserCredentials[] = [];
  tempUserList$: UserCredentials[] = [];
  sentreqList: FriendRequest[] = [];
  receivereqList: FriendRequest[] = [];
  friendList: Friends[] = [];
	currentUserId: string = this.auth.getCurrentUserID();
	friendRequest = {} as FriendRequest;

  constructor(public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private auth: AuthProvider, private friendreqProvider: FriendreqProvider, private alert: AlertProvider, private toast: ToastProvider, private friendProvider: FriendsProvider, private zone: NgZone, private ref: ChangeDetectorRef) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlluserlistPage');
  }

  ionViewWillEnter() {
    this.getAllUserList();
    this.loadRequiredCheckingList();
  }

  getAllUserList() {
    
  	this.userProvider.getAllUser()
  	.snapshotChanges()
  	.subscribe( snap => {
  		var tempObj = [];

      snap.forEach(elem => {
        tempObj.push({
          key: elem.payload.key,
          ...elem.payload.val()
        });
      });

      this.userList$ = tempObj;
      this.tempUserList$ = tempObj;
  	});

  }

  /*filterExistInSentReq(key) {
    var foundInSentReqList: boolean = false;

    if(this.sentreqList.length > 0) {
      this.sentreqList.forEach( (item, index) => {
        if(item == key) {
          foundInSentReqList = true;
        } 
      });

      if(foundInSentReqList) {
        return true
      } else {
        return false;
      }

    } else {
      return false;
    }

  }

  filterExistInRecReq(key) {

    var foundInRecReqList: boolean = false;

    if(this.receivereqList.length > 0) {
      this.receivereqList.forEach( (item, index) => {
        if(item == key) {
          foundInRecReqList = true;
        } 
      });

      if(foundInRecReqList) {
        return true
      } else {
        return false;
      }
      
    } else {
      return false;
    }
  }

  filterExistInFriend(key) {
    var foundInFriendList: boolean = false;

    if(this.friendList.length > 0) {
      this.friendList.forEach( (item, index) => {
        if(item == key) {
          foundInFriendList = true;
        } 
      });

      if(foundInFriendList) {
        return true
      } else {
        return false;
      }
      
    } else {
      return false;
    }
  }*/

  searchuser(searchbar): Observable<UserCredentials[]> {
    this.userList$ = this.tempUserList$;
  	var q = searchbar.target.value;

  	if(q.trim() == '') {
  		return;
  	}

    this.userList$ = this.userList$.filter( result => {
      if((result.displayName.toLowerCase().indexOf(q.toLowerCase())) > -1) {
        return true;
      }
      return false;
    });
  }

  sendReq(requestedUser: UserCredentials) {
  	this.friendreqProvider.submitFriendReq(requestedUser.key, this.friendRequest)
    .then( res => {
      this.toast.showToast('You have successfully send a friend request to ' + requestedUser.displayName);
    })
    .catch( err => {
      this.toast.showToast('Something went wrong, please try again later.');
    });

  }

  backToTabs() {
  	this.navCtrl.parent.parent.setRoot(TabsPage);
  }

  loadRequiredCheckingList() {

    this.friendreqProvider.getSentRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempSentReqList = [];

        snap.forEach(elem => {
          tempSentReqList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.sentreqList = tempSentReqList;
      });
    });

    this.friendreqProvider.getRecRequestListKey()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempRecReqList = [];

        snap.forEach(elem => {
          tempRecReqList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.receivereqList = tempRecReqList;
      });
    });

    this.friendProvider.getFriendList()
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run(() => {
        var tempFriendList = [];

        snap.forEach(elem => {
          tempFriendList.push({
            key: elem.payload.key,
            ...elem.payload.val()
          });
        });

        this.friendList = tempFriendList;
      });
    });
  }

}

import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController, AlertController } from 'ionic-angular';
import { UserCredentials } from '../../models/users/users.model';
import { UserProvider } from '../../providers/user/user';
import { CameraProvider } from '../../providers/camera/camera';
import { FirebasestorageProvider } from '../../providers/firebasestorage/firebasestorage';
import { AlertProvider } from '../../providers/alert/alert';
import { FriendlistPage } from '../friendlist/friendlist';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

	usersCred = {} as UserCredentials;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, public actionSheetCtrl: ActionSheetController, public loadingCtrl: LoadingController, private cameraProvider: CameraProvider, private fireStore: FirebasestorageProvider, private alert: AlertProvider, public zone: NgZone, public alertCtrl: AlertController) {
  }

  loadUserDetails() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Content.. please wait!'
    });
    loader.present();

  	this.userProvider.getUserDetails().valueChanges().subscribe(snap => {
  		this.usersCred = snap;
	    this.zone.run( () => {
	    	this.usersCred.photoURL = snap.photoURL;
	    });
	    loader.dismiss();
  	});
  	
  }

  ionViewWillEnter() {
    this.loadUserDetails();
  }

  presentActionSheet() {
    let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });

    let actionSheet = this.actionSheetCtrl.create({
      
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(1)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                this.usersCred.photoURL = url;
                this.userProvider.updateimage(url, this.usersCred)
                .then( res => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Profile Images successfully updated!');
                })
                .catch( err => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
                });
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
              });
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
            });
          }
        },{
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            
            loader.present();
            this.cameraProvider.triggerCamera(0)
            .then(res => {
              this.fireStore.uploadProfilePhoto(res)
              .then( (url: any) => {
                this.usersCred.photoURL = url;
                this.userProvider.updateimage(url, this.usersCred)
                .then( res => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Profile Images successfully updated!');
                })
                .catch( err => {
                  loader.dismiss();
                  this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
                });
              })
              .catch( err => {
                loader.dismiss();
                this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
              });
            })
            .catch( err => {
              loader.dismiss();
              this.alert.showAlert('Update Profile images', 'Error: Something went wrong.<br>Please check your connections and try again.');
            });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  editDisplayName() {
    let alert = this.alertCtrl.create({
      title: 'Edit Display Name',
      inputs: [{
        name: 'displayName',
        placeholder: 'Display Name...'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      },
        {
          text: 'Update',
          handler: data => {
            if(data.displayName) {
              this.usersCred.displayName = data.displayName;
              this.userProvider.updateDisplayName(this.usersCred)
              .then( res => {
                this.alert.showAlert('Edit Display Name', 'Display name successfully updated!');
              })
              .catch( err => {
                this.alert.showAlert('Edit Display Name', 'Error: Something went wrong.<br>Please check your connections and try again.');

              });
            }
          }
        }]
    });
    alert.present();
  }

  editStatus() {
    let alert = this.alertCtrl.create({
      title: 'Edit Status',
      inputs: [{
        name: 'status',
        placeholder: 'Status...'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: data => {

        }
      },
        {
          text: 'Update',
          handler: data => {
            if(data.status) {
              this.usersCred.status = data.status;
              this.userProvider.updateUserStatus(this.usersCred)
              .then( res => {
                this.alert.showAlert('Edit Status', 'Status successfully updated!');
              })
              .catch( err => {
                this.alert.showAlert('Edit Status', 'Error: Something went wrong.<br>Please check your connections and try again.');

              });
            }
          }
        }]
    });
    alert.present();
  }

  editProfile() {

  	let actionSheet = this.actionSheetCtrl.create({
    	buttons: [
	        {
	          text: 'Edit Display Name',
	          handler: () => {
	            this.editDisplayName();
	          }
	        },{
	          text: 'Edit Status',
	          handler: () => {
	          	this.editStatus();
	          }
	        },{
	          text: 'Cancel',
	          role: 'cancel',
	          handler: () => {
	            
	          }
	        }
	      ]
    });
    actionSheet.present();
  }

  editImage() {
    this.presentActionSheet();
  }

  friendList() {
  	this.navCtrl.setRoot(FriendlistPage);
  }

}

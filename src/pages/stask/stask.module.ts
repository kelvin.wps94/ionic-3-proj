import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaskPage } from './stask';

@NgModule({
  declarations: [
    StaskPage,
  ],
  imports: [
    IonicPageModule.forChild(StaskPage),
  ],
})
export class StaskPageModule {}

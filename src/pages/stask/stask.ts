import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController, Content, Select, AlertController, DateTime, ModalController } from 'ionic-angular';
import { Observable } from 'rxjs/observable';
import { Task } from '../../models/task/task.model';
import { TaskItems } from '../../models/taskitems/taskitems.model';
import { TaskSection } from '../../models/tasksection/tasksection.model';
import { SubTask } from '../../models/subtask/subtask.model';
import { Poll } from '../../models/poll/poll.model';
import { PollOptions } from '../../models/polloptions/polloptions.model';
import { Voters } from '../../models/voters/votes.model';
import { ProjectBoard } from '../../models/projectboard/projectboard.model';
import { ActivityLog } from '../../models/activitylog/activitylog.model';
import { UserCredentials } from '../../models/users/users.model';
import { TaskProvider } from '../../providers/task/task';
import { ToastProvider } from '../../providers/toast/toast';
import { AlertProvider } from '../../providers/alert/alert';
import { ProjectboardProvider } from '../../providers/projectboard/projectboard';
import { TasksectionProvider } from '../../providers/tasksection/tasksection';
import { AuthProvider } from '../../providers/auth/auth';
import { GroupProvider } from '../../providers/group/group';
import { DatetimeinstringProvider } from '../../providers/datetimeinstring/datetimeinstring';
import { SubtaskProvider } from '../../providers/subtask/subtask';
import { AssignmemberPage } from '../assignmember/assignmember';
import { ActivitylogPage } from '../activitylog/activitylog';
import { PollPage } from '../poll/poll';
import firebase from 'firebase';
/**
 * Generated class for the StaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stask',
  templateUrl: 'stask.html',
})
export class StaskPage {

	fireDB = firebase.database().ref('/');

	currentUserId: string = '';
	selectedTaskKey: string = '';
	selectedGroupKey: string = '';
	selectedBoardKey: string = '';
	selectedTaskSectKey: string = '';
	selectedTaskPriority: string = '';
	totalVote: number = 0;
	totalOfSubTask: number = 0;
	pollOptionTotal: number = 0;
	storedVoteTotal: number = 0;
	totalAssignedMem: number = 0;
	numOfAllowedVote: number = 1;
	preventTriggerTwice: number = 0;
	isAdmin: boolean = false;
	activePage: boolean = false;
	priviledges: boolean = false;
 	pollExpired: boolean = false;
	hideComponent: boolean = true;
 	expandSubTask: boolean = false;
 	pollIsCreated: boolean = false;
 	singleChoices: boolean = false;
 	eligibleToVote: boolean = false;
 	multipleChoices: boolean = false;
 	taskDueDateIsSet: boolean = false;
 	addPollingPending: boolean = false;
 	pollMultipleOptions: boolean = false;
 	taskPollDueDateIsSet: boolean = false;
 	addSubTaskBtnDisabled: boolean = true;
	dueDate: any;
	minDate: any;
 	maxDate: any;
 	todayDate: any;
 	tempDueDate: any;
 	pollDueDate: any;
 	maxFilterDate: any;
 	tempPollDueDate: any;

 	pollOptions: any = [];

 	storedVote: PollOptions[] = [];
 	subTaskObjectList: SubTask[] = [];
 	voterMultipleVotes: Voters[] = [];
 	pollOptionsObjectList: PollOptions[] = [];
	taskSectionList$: Observable<TaskSection[]>;

	task = {} as Task;
	polls = {} as Poll;
	voter = {} as Voters;
	board = {} as ProjectBoard;
	taskItems = {} as TaskItems;
	taskSection = {} as TaskSection;

	poll: Poll = {
	    pollingType: '',
	    pollingTarget: '',
	    numOfAllowedVote: 1
	}

	subTask: SubTask = {
	    subTaskTitle: '',
	    isDone: false
	}

	activityLog: ActivityLog = {
	    activityType: '',
	    handler: '',
	    handlerIsAdmin: false,
	    dateNTimeHandled: '',
	    logActivity: ''
	}

	@ViewChild(Content) content:Content;
	@ViewChild('taskSectionList') taskSectLists: Select;
	@ViewChild('taskPriorityList') taskPriorityList: Select;
	@ViewChild('dueDatePicker') dueDatePicker: DateTime;
	@ViewChild('pollDueDatePicker') pollDueDatePicker: DateTime;
  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvider: AuthProvider, private groupProvider: GroupProvider, private taskProvider: TaskProvider, public viewCtrl: ViewController, public actionSheetCtrl: ActionSheetController, private toast: ToastProvider, private alertProvider: AlertProvider, private projBoardProvider: ProjectboardProvider, public alertCtrl: AlertController, private taskSectionProvider: TasksectionProvider, public zone: NgZone, private dateTimeSting: DatetimeinstringProvider, public modal: ModalController, private subtaskProvider: SubtaskProvider) {
  	this.selectedGroupKey = this.navParams.get('groupKey');
    this.selectedBoardKey = this.navParams.get('boardKey');
  	this.selectedTaskSectKey = this.navParams.get('taskSectKey');
  	this.selectedTaskKey = this.navParams.get('taskKey');
  	this.currentUserId = this.authProvider.getCurrentUserID();
  	this.todayDate = new Date(new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
  	this.minDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).getTime() + 60 * 60 * 1000).toISOString().slice(0,-1));
    this.maxDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).getTime() + (365 * 5) * 24 * 60 * 60 * 1000).toISOString().slice(0,-1));
    this.maxFilterDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0,-1));

  	this.groupProvider.getSelectedGroupDetails(this.selectedGroupKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {

        if(snap.admin == this.currentUserId) {
          this.isAdmin = true;
        }
      }
    });

    this.taskProvider.getSelectedTaskDetails(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey)
  	.valueChanges()
  	.subscribe(snap => {
  		if(snap) {
  			this.task = snap;
  		}
  	});

  	this.taskProvider.getPoll(this.selectedTaskKey)
    .valueChanges()
    .subscribe(snap => {

      if(snap) {
        this.polls = snap;
        this.pollIsCreated = true;

        if(snap.pollingType == 'single') {
          this.singleChoices = true;
          this.multipleChoices = false;
        } else if (snap.pollingType == 'multiple') {
          this.singleChoices = false;
          this.multipleChoices = true;
        }

        if(snap.pollingDueDate) {
          
          this.polls.pollingDueDate = (new Date((new Date(Date.parse(snap.pollingDueDate) - (new Date()).getTimezoneOffset()))).toISOString().slice(0,-1));
          
          if(Date.parse(this.todayDate) > Date.parse(snap.pollingDueDate)) {
            this.pollExpired = true;
          } else {
            this.pollExpired = false;            
          }
        }

      } else {
        this.pollIsCreated = false;
      }
    });

  	this.taskProvider.getAssignedMemList(this.selectedTaskKey)
    .snapshotChanges()
    .subscribe(snap => {
      if(snap) {

      	console.log('test');
        
        var found = false;

        snap.forEach(elem => {
          if(elem.payload.key == this.currentUserId) {
            found = true;
            console.log('yes');
          }
        });

        this.priviledges = found

        if(this.polls.pollingTarget == 'group') {
          this.eligibleToVote = true
          console.log('group yes');
        } else if (this.polls.pollingTarget == 'assigned') {
          this.eligibleToVote = found
          console.log('assigned yes');
        }

        if(this.isAdmin) {
          this.eligibleToVote = true;
        }

        if(snap.length < 1) {
          this.priviledges = true; 
        } else {
          this.priviledges = found;
        }
      }

      console.log(this.priviledges);

      this.totalAssignedMem = snap.length;

    });

    this.taskProvider.getSelectedTaskItemsInfo(this.selectedTaskKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {
        this.taskItems = snap;
        this.dueDate = snap.dueDate;

        if(snap.dueDate) {
          this.taskItems.dueDate = (new Date((new Date(Date.parse(snap.dueDate) - (new Date()).getTimezoneOffset()))).toISOString().slice(0,-1));
          this.taskDueDateIsSet = true;
        } else {
          this.taskDueDateIsSet = false;
        }
      }
    });

    this.taskSectionProvider.getSelectedTaskSectionDetails(this.selectedBoardKey, this.selectedTaskSectKey)
  	.valueChanges()
  	.subscribe(snap => {
  		this.zone.run( () => {
        if (snap) {
          this.taskSection = snap;
        }
      });
  	});

  	this.taskSectionList$ = this.taskSectionProvider.getTaskSectionList(this.selectedBoardKey)
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      }));
    });

    this.subtaskProvider.getSubTaskList(this.selectedTaskKey)
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run( () => {
        var tempSubTaskObj = [];
        
        if(snap) {
          snap.forEach(elem => {
            tempSubTaskObj.push({
              key: elem.payload.key,
              ...elem.payload.val()
            });
          });
        }

        this.totalOfSubTask = snap.length;

        this.subTaskObjectList = tempSubTaskObj;
      })
      
    });

    this.taskProvider.getSelectedVoters(this.selectedTaskKey)
    .valueChanges()
    .subscribe(snap => {
      if(snap) {
        this.voter = snap;
      }
    });

    this.taskProvider.getPollOptions(this.selectedTaskKey)
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run( () => {
        var tempPollOptionsObj = [];
        
        if(snap) {
          snap.forEach(elem => {
            tempPollOptionsObj.push({
              key: elem.payload.key,
              ...elem.payload.val()
            });
          });
        }

        this.pollOptionsObjectList = tempPollOptionsObj;

        this.totalVote = 0;

        this.pollOptionsObjectList.forEach(elem => {
          this.totalVote = this.totalVote + elem.totalVote;
        });
      });
    });

    this.taskProvider.getVoterMultipleVotes(this.selectedTaskKey)
    .snapshotChanges()
    .subscribe(snap => {
      this.zone.run( () => {
        var tempVotersMultipleVoteObj = [];
        
        if(snap) {
          snap.forEach(elem => {
            tempVotersMultipleVoteObj.push({
              key: elem.payload.key,
              ...elem.payload.val()
            });
          });
        }

        this.voterMultipleVotes = tempVotersMultipleVoteObj;
        
      });
    });

    this.pollOptions = [
      {option: ''},
      {option: ''}
    ]

    this.pollOptionTotal = this.pollOptions.length;
  }

  ionViewWillEnter() {
    this.activePage = true;
  }

  ionViewWillLeave() {
    this.activePage = false;
  }

  modalDismiss() {
  	this.viewCtrl.dismiss();
  }

  taskMoreOptions(admin, priviledgesUser) {

    if(admin && !priviledgesUser) {
      this.moreTaskOptionsAdminActionSheet();
    } else if (!admin && priviledgesUser) {
      this.moreTaskOptionsActionSheet();
    } else if (admin && priviledgesUser) {
      this.moreTaskOptionsAdminActionSheet();
    }

  }

  moreTaskOptionsAdminActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'More Option:',
      buttons: [
        {
          text: 'Move Task',
          icon: 'move',
          handler: () => {
            
            this.taskSectLists.open();

          }
        },{
          text: 'Rename Task',
          icon: 'create',
          handler: () => {
            
            this.renameTask();
          }
        },{
          text: 'Delete Task',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            var task = {};

            task['task/' + this.selectedTaskKey ] = {}
            task['taskSection/' + this.selectedBoardKey + '/' + this.selectedTaskSectKey + '/task/' + this.selectedTaskKey] = {}

            this.taskProvider.firebaseCRUD(task)
            .then(res => {
              this.toast.showToast('Task has been removed.');
              
              if(this.taskSection.isTaskCompletedSection) {
                this.board.totalCompletedTask = this.board.totalCompletedTask - 1;
                this.board.totalTask = this.board.totalTask - 1;

                this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
                .then( res => {
                  console.log(res);
                  this.modalDismiss();
                })
                .catch( err => {
                  console.log(err);
                });

              } else {

                this.board.totalTask = this.board.totalTask - 1;

                this.projBoardProvider.updateTaskProgress(this.selectedGroupKey, this.selectedBoardKey, this.board)
                .then( res => {
                  console.log(res);
                  this.modalDismiss();
                })
                .catch( err => {
                  console.log(err);
                });
              }
            })
            .catch(err => {
              this.alertProvider.showAlert('Remove Task', err);
            })
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  renameTask() {
    let prompt = this.alertCtrl.create({
      title: 'Rename Task',
      message: "Enter a new name for this task.",
      inputs: [
        {
          name: 'taskName',
          placeholder: 'Task Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'Rename',
          handler: data => {
            var tempName;

            tempName = this.task.taskName;
            this.task.taskName = data.taskName;

            this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
            .then( res => {
              this.toast.showToast("This task has been renamed successfully.");

              this.activityLog.activityType = 'TaskRenameLog';
              this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
              this.activityLog.handler = this.currentUserId;
              this.activityLog.handlerIsAdmin = this.isAdmin;
              this.activityLog.logActivity = "has renamed task from '" + tempName + "' to '" + data.taskName + "'.";

              this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
              .then( r => {
                console.log('Activity has been Logged');
              })
              .catch( e => {
                console.log('Activity Logging: ' + e);
              });
            })
            .catch( err => {
              this.alertProvider.showAlert('Rename Task', err);
            });
          }
        }
      ]
    });
    prompt.present();
  }

  moreTaskOptionsActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'More Option:',
      buttons: [
        {
          text: 'Move Task',
          icon: 'move',
          handler: () => {
            
            this.taskSectLists.open();
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });
    actionSheet.present();
  }

  assignMemberToTask() {
    const selectedTaskModal = this.modal.create(AssignmemberPage, {
      groupKey: this.selectedGroupKey,
      boardKey: this.selectedBoardKey,
      taskSectKey: this.selectedTaskSectKey,
      taskKey: this.selectedTaskKey
  	});

  	selectedTaskModal.present();
  }

  selectTaskPriority() {

    if(this.isAdmin) {
      this.taskPriorityList.open();
    }
  }

  setTaskPriority() {

    if(this.selectedTaskPriority != 'none') {

      this.taskProvider.setTaskPriority(this.selectedTaskKey, this.selectedTaskPriority)
      .then( res => {
        console.log('Set Task Priority: ' + res);

        this.task.priority = this.selectedTaskPriority;

        this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
        .then( res => {
            console.log('Task priority status updated: ' + res);

            this.activityLog.activityType = 'TaskPriorityLog';
            this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
            this.activityLog.handler = this.currentUserId;
            this.activityLog.handlerIsAdmin = this.isAdmin;
            this.activityLog.logActivity = "set this task priority to " + this.selectedTaskPriority;

            this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
            .then( r => {
              console.log('Activity has been Logged');
            })
            .catch( e => {
              console.log('Activity Logging: ' + e);
            });
        })
        .catch( err => {
            this.alertProvider.showAlert('Task priority status update failed', err);
        });

      })
      .catch( err => {
        this.alertProvider.showAlert('Failed to set task priority', err);
      });

    } else {

      this.taskProvider.removeTaskPriority(this.selectedTaskKey)
      .then( res => {
        console.log('Remove Task Priority: ' + res);

        this.task.priority = this.selectedTaskPriority;

        this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
        .then( res => {
            console.log('Task priority status updated: ' + res);

            this.activityLog.activityType = 'TaskPriorityLog';
            this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
            this.activityLog.handler = this.currentUserId;
            this.activityLog.handlerIsAdmin = this.isAdmin;
            this.activityLog.logActivity = "set this task priority to " + this.selectedTaskPriority;

            this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
            .then( r => {
              console.log('Activity has been Logged');
            })
            .catch( e => {
              console.log('Activity Logging: ' + e);
            });
        })
        .catch( err => {
            this.alertProvider.showAlert('Task priority status update failed', err);
        });
      })
      .catch( err => {
        console.log('Failed to remove task priority: ' + err);
      });
    }
  }

  clearDueDate() {
    this.taskProvider.removeTaskDueDate(this.selectedTaskKey)
    .then( res => {
      this.dueDate = null;
      this.task.taskDue = false;
      this.toast.showToast('Due Date has been removed.');

      this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
      .then( res => {
          console.log('Due Date Removed Status Updated: ' + res);

          this.activityLog.activityType = 'TaskDueDateLog';
          this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
          this.activityLog.handler = this.currentUserId;
          this.activityLog.handlerIsAdmin = this.isAdmin;
          this.activityLog.logActivity = 'removed the due date from this task.';

          this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
          .then( r => {
            console.log('Activity has been Logged');
          })
          .catch( e => {
            console.log('Activity Logging: ' + e);
          });
      })
      .catch( err => {
          this.alertProvider.showAlert('Due Date Status Update', err);
      });
    })
    .catch( err => {
      this.toast.showToast('Remove Due Date Failed. ' + err);
    });
  }

  callDueDatePicker() {
  	if(this.isAdmin) {
	    this.updateMinMaxDate('dueDate');
	    this.dueDatePicker.open();
	}
  }

  updateMinMaxDate(options) {
    this.minDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).getTime() + 60 * 60 * 1000).toISOString().slice(0,-1));
    this.maxDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).getTime() + (365 * 5) * 24 * 60 * 60 * 1000).toISOString().slice(0,-1));
    this.preventTriggerTwice = 0;

    if(options == 'dueDate') {
      this.tempDueDate = this.dueDate;
    } else if(options == 'poll') {
      this.tempPollDueDate = this.pollDueDate;
    }
  }

  setDueDate() {

    let verified = false;
    let notEmpty = false;

    if(this.dueDate) {
      notEmpty = true;
      if(new Date(Date.parse(this.dueDate.slice(0,-1))).getFullYear() == new Date(Date.parse(this.minDate)).getFullYear()) {

        if(new Date(Date.parse(this.dueDate.slice(0,-1))).getMonth() == new Date(Date.parse(this.minDate)).getMonth()) {

          if(new Date(Date.parse(this.dueDate.slice(0,-1))).getDate() == new Date(Date.parse(this.minDate)).getDate()) {

            if(new Date(Date.parse(this.dueDate.slice(0,-1))).getHours() == new Date(Date.parse(this.minDate)).getHours()) {

              if(new Date(Date.parse(this.dueDate.slice(0,-1))).getMinutes() >= new Date(Date.parse(this.minDate)).getMinutes()) {
                verified = true;
              }

            } else if(new Date(Date.parse(this.dueDate.slice(0,-1))).getHours() > new Date(Date.parse(this.minDate)).getHours()) {
              verified = true;
            }

          } else if(new Date(Date.parse(this.dueDate.slice(0,-1))).getDate() > new Date(Date.parse(this.minDate)).getDate()) {

            if(new Date(Date.parse(this.dueDate.slice(0,-1))).getDate() == (new Date(Date.parse(this.minDate)).getDate() + 1)) {

              if(new Date(Date.parse(this.dueDate.slice(0,-1))).getHours() < new Date(Date.parse(this.minDate)).getHours()) {
                  
                if(new Date(Date.parse(this.dueDate.slice(0,-1))).getMinutes() >= new Date(Date.parse(this.minDate)).getMinutes()) {
                  verified = true;
                }

              } else {
                verified = true;
              }

            } else {
              verified = true;
            }

          }

        } else if(new Date(Date.parse(this.dueDate.slice(0,-1))).getMonth() > new Date(Date.parse(this.minDate)).getMonth()) {
          verified = true;
        }

      } else if(new Date(Date.parse(this.dueDate.slice(0,-1))).getFullYear() > new Date(Date.parse(this.minDate)).getFullYear()) {
        verified = true;
      }
    }

    if(verified) {

      this.taskProvider.setTaskDueDate(this.selectedTaskKey, this.dueDate)
      .then( res => {

        if(this.dueDate) {

          let temp = (new Date((new Date(Date.parse(this.dueDate) - (new Date()).getTimezoneOffset()))).toISOString().slice(0,-1));
          let tempDueDate = new Date(Date.parse(temp));
          let dateNTime = this.dateTimeSting.getDay(tempDueDate.getDay().toString()) + ', ' + tempDueDate.getDate() + ' ' + this.dateTimeSting.getMonth(tempDueDate.getMonth().toString()) + ' ' + tempDueDate.getFullYear() + ' at ' + this.dateTimeSting.getTime(tempDueDate.getHours(), tempDueDate.getMinutes());
          
          this.toast.showToast('Due Date has been added.');

          this.task.taskDue = true;

          this.activityLog.activityType = 'TaskDueDateLog';
          this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
          this.activityLog.handler = this.currentUserId;
          this.activityLog.handlerIsAdmin = this.isAdmin;
          this.activityLog.logActivity = 'set this task to be due by ' + dateNTime + '.';

          this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
          .then( r => {
            console.log('Activity has been Logged');
          })
          .catch( e => {
            console.log('Activity Logging: ' + e);
          });

        } else {

          this.task.taskDue = false;

        }

        this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
        .then( res => {
            console.log('Due Date Status Updated: ' + res);
        })
        .catch( err => {
            this.alertProvider.showAlert('Due Date Status Update', err);
        });
      })
      .catch( err => {
        this.toast.showToast('Set Due Date Failed. ' + err);
      });

    } else if(notEmpty) {
      this.alertProvider.showAlert('Set Due Date', 'Due date must be 1 hour after the current time and date of today');

      if(!this.taskDueDateIsSet) {
        this.dueDate = null;
      }
    }
  }

  expandTaskList() {

    if(this.expandSubTask) {
      this.expandSubTask = false;
    } else {
      this.expandSubTask = true;
    }
  }

  subTaskStatusChange(subTask: SubTask) {

    if(subTask.isDone) {
      subTask.isDone = false;
      this.activityLog.activityType = 'SubtaskLog';
      this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      this.activityLog.handler = this.currentUserId;
      this.activityLog.handlerIsAdmin = this.isAdmin;
      this.activityLog.logActivity = "has marked sub-task '" + subTask.subTaskTitle + "' incomplete on this task";
    } else {
      subTask.isDone = true;
      this.activityLog.activityType = 'SubtaskLog';
      this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      this.activityLog.handler = this.currentUserId;
      this.activityLog.handlerIsAdmin = this.isAdmin;
      this.activityLog.logActivity = "has marked sub-task '" + subTask.subTaskTitle + "' completed on this task";
    }

    this.subtaskProvider.updateSubTaskStatus(this.selectedTaskKey, subTask)
    .then( res => {
      console.log('isDone status updated: ' + res);

      this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
      .then( r => {
        console.log('Activity has been Logged');
      })
      .catch( e => {
        console.log('Activity Logging: ' + e);
      });
    })
    .catch( err => {
      this.toast.showToast('Update Sub-Task Status Failed. ' + err);
    });
  }

  deleteSelectedSubTask(subTask: SubTask) {
    this.subtaskProvider.deleteSelectedSubTask(this.selectedTaskKey, subTask)
    .then( res => {
      this.toast.showToast("Sub-Task '" + subTask.subTaskTitle + "' has been removed");

      this.subtaskProvider.getSubTaskList(this.selectedTaskKey)
      .snapshotChanges()
      .subscribe(snap => {
        if(snap.length < 1) {
          this.task.subTask = false;

          this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
          .then( res => {
              console.log('Sub-Task Status Updated: ' + res);
          })
          .catch( err => {
              this.alertProvider.showAlert('Sub-Task Status Update', err);
          });
        }
      });
    })
    .catch( err => {
      this.toast.showToast('Remove Sub-Task Failed. ' + err);
    });
  }

  countInputLength() {

    if(this.subTask.subTaskTitle.length > 0) {
      this.addSubTaskBtnDisabled = false;
    } else {
      this.addSubTaskBtnDisabled = true;
    }
  }

  addSubTask(subTask: SubTask) {
    
    this.subtaskProvider.addSubTask(this.selectedTaskKey, subTask)
    .then(res => {
      this.task.subTask = true;

      this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
      .then( res => {
          console.log('Sub-task added: ' + res);
      })
      .catch( err => {
          this.alertProvider.showAlert('Add Sub-Task', err);
      });
    })
    .catch(err => {
      this.alertProvider.showAlert('Add Sub-Task', err);
    });

    this.subTask.subTaskTitle = '';
    this.addSubTaskBtnDisabled = true;

  }

  addPolling() {
    if(this.addPollingPending) {
      this.addPollingPending = false;
    } else {
      this.addPollingPending = true;
    }
  }

  removePolling() {
    this.taskProvider.removePoll(this.selectedTaskKey)
    .then( res => {
      this.toast.showToast('Poll has been removed.');

      this.task.poll = false;

      this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
      .then( res => {
        console.log('Task Items Status Updated' + res);

        this.activityLog.activityType = 'TaskPollLog';
        this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
        this.activityLog.handler = this.currentUserId;
        this.activityLog.handlerIsAdmin = this.isAdmin;
        this.activityLog.logActivity = 'removed the poll on this task';

        this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
        .then( r => {
          console.log('Activity has been Logged');
        })
        .catch( e => {
          console.log('Activity Logging: ' + e);
        });
      })
      .catch( err => {
        console.log('Task Items Status Update Failed: ' + err);
      });
    })
    .catch( err => {
      this.alertProvider.showAlert('Remove Poll', err);
    });
  }

  checkPollingType() {
    if(this.poll.pollingType == 'multiple') {
      this.pollMultipleOptions = true;
    } else {
      this.pollMultipleOptions = false;
    }
  }

  callPollDatePicker() {
    this.updateMinMaxDate('poll');
    this.pollDueDatePicker.open();
  }

  checkPollDueDate() {

      let verified = false;

      if(this.pollDueDate) {
        console.log('1');

        if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getFullYear() == new Date(Date.parse(this.minDate)).getFullYear()) {
          console.log('2.1');

          if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getMonth() == new Date(Date.parse(this.minDate)).getMonth()) {
            console.log('3.1');

            if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getDate() == new Date(Date.parse(this.minDate)).getDate()) {
              console.log('4.1');

              if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getHours() == new Date(Date.parse(this.minDate)).getHours()) {
                console.log('a5.1');
                console.log(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getHours());
                console.log(new Date(Date.parse(this.minDate)).getHours());

                if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getMinutes() >= new Date(Date.parse(this.minDate)).getMinutes()) {
                  console.log('a6');
                  verified = true;
                  this.taskPollDueDateIsSet = true;
                }

              } else if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getHours() > new Date(Date.parse(this.minDate)).getHours()) {
                console.log('a5.2');
                verified = true;
                this.taskPollDueDateIsSet = true;
              }

            } else if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getDate() > new Date(Date.parse(this.minDate)).getDate()) {
              console.log('4.2');

              if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getDate() == (new Date(Date.parse(this.minDate)).getDate() + 1)) {
                console.log('b5.1');

                if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getHours() < new Date(Date.parse(this.minDate)).getHours()) {
                  console.log('b6.1');

                  if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getMinutes() >= new Date(Date.parse(this.minDate)).getMinutes()) {
                    console.log('b7');
                    verified = true;
                    this.taskPollDueDateIsSet = true;
                  }

                } else {
                  console.log('b6.2');
                  verified = true;
                  this.taskPollDueDateIsSet = true;
                }

              } else {
                console.log('b5.2');
                verified = true;
                this.taskPollDueDateIsSet = true;
              }

            }

          } else if (new Date(Date.parse(this.pollDueDate.slice(0,-1))).getMonth() > new Date(Date.parse(this.minDate)).getMonth()) {
            console.log('3.2');
            verified = true;
            this.taskPollDueDateIsSet = true;
          }

        } else if(new Date(Date.parse(this.pollDueDate.slice(0,-1))).getFullYear() > new Date(Date.parse(this.minDate)).getFullYear()) {
          console.log('2.2');
          verified = true;
          this.taskPollDueDateIsSet = true;
        }
      }
    
      if(!verified) {
      this.alertProvider.showAlert('Set Poll Due Date', 'Poll due date must be 1 hour after the current time and date of today');

        if(!this.taskPollDueDateIsSet) {
          this.pollDueDate = null;
          this.taskPollDueDateIsSet = false;
        } else {
          this.pollDueDate = this.tempPollDueDate;
          this.taskPollDueDateIsSet = true;
        }
      }

  }

  removePollDueDate() {
     this.pollDueDate = null;
     this.taskPollDueDateIsSet = false;
  }

  removePollOptions(index) {
    this.pollOptions.splice(index, 1);
    this.pollOptionTotal = this.pollOptions.length;
  }

  addPollOptions() {
    this.pollOptions.push({
      option: ''
    })
    this.pollOptionTotal = this.pollOptions.length;
    this.scrolltoBottom();
  }

  scrolltoBottom() {
    setTimeout( () => {
      if(this.content._scroll){
        this.content.scrollToBottom(0);
      }
    });
  }

  createPoll(poll: Poll) {

    var checkPollOptionsHasEmpty = false;
    var checkPollAllowedNum = false;

    this.pollOptions.forEach(elem => {
      if(elem.option == '') {
        checkPollOptionsHasEmpty = true;
      }
    });

    if(this.poll.pollingType == 'single') {
      checkPollAllowedNum = true;
    } else if(this.poll.pollingType == 'multiple') {

      if(this.numOfAllowedVote > this.pollOptions.length) {
        checkPollAllowedNum = false;
      } else {
        checkPollAllowedNum = true;
      }
    }

    var pollItems = {};
    var pollOption = {};

    if(this.poll.pollingType != '' && this.poll.pollingTarget != '' && !checkPollOptionsHasEmpty && checkPollAllowedNum) {

      if(this.pollDueDate) {

        if(this.poll.pollingType == 'single') {
          pollItems['task/' + this.selectedTaskKey + '/poll'] = {
            pollingType: poll.pollingType,
            pollingTarget: poll.pollingTarget,
            pollingDueDate: this.pollDueDate,
            numOfAllowedVote: 1
          }
        } else {
          pollItems['task/' + this.selectedTaskKey + '/poll'] = {
            pollingType: poll.pollingType,
            pollingTarget: poll.pollingTarget,
            pollingDueDate: this.pollDueDate,
            numOfAllowedVote: this.numOfAllowedVote
          }
        }
        
      } else {

        if(this.poll.pollingType == 'single') {
          pollItems['task/' + this.selectedTaskKey + '/poll'] = {
            pollingType: poll.pollingType,
            pollingTarget: poll.pollingTarget,
            numOfAllowedVote: 1
          }
        } else {
          pollItems['task/' + this.selectedTaskKey + '/poll'] = {
            pollingType: poll.pollingType,
            pollingTarget: poll.pollingTarget,
            numOfAllowedVote: this.numOfAllowedVote
          }
        }

      }

      this.pollOptions.forEach(elem => {
        pollOption['task/' + this.selectedTaskKey + '/poll/pollOptions/' + this.fireDB.push().key ] = {
          option: elem.option,
          totalVote: 0
        }
      });

      this.taskProvider.firebaseCRUD(pollItems)
      .then( res => {

        console.log('Poll Items Update: ' + res);
        
        this.taskProvider.firebaseCRUD(pollOption)
        .then( r => {
            console.log('Poll Options Update: ' + r);

            this.task.poll = true;

            this.taskProvider.updateTaskItemsStatus(this.selectedBoardKey, this.selectedTaskSectKey, this.selectedTaskKey, this.task)
            .then( res => {
                this.taskPollDueDateIsSet = false;

                console.log('Due Date Status Updated: ' + res);

                this.activityLog.activityType = 'TaskPollLog';
                this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
                this.activityLog.handler = this.currentUserId;
                this.activityLog.handlerIsAdmin = this.isAdmin;
                this.activityLog.logActivity = 'created a poll on this task';

                this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
                .then( r => {
                  console.log('Activity has been Logged');
                })
                .catch( e => {
                  console.log('Activity Logging: ' + e);
                });
            })
            .catch( err => {
                this.alertProvider.showAlert('Due Date Status Update', err);
            });
        })
        .catch( e => {
            console.log('Poll Options Error: ' + e);
        });
      })
      .catch( err => {
        console.log('Poll Items Error: ' + err);
      });

      this.poll.pollingTarget = '';
      this.poll.pollingType = '';
      this.pollDueDate = null;
      this.pollOptions = [
        {option: ''},
        {option: ''}
      ];
      this.pollOptionTotal = this.pollOptions.length;
      this.addPollingPending = false;
      this.numOfAllowedVote = 1;
      this.pollMultipleOptions = false;

    } else {
      
      if(this.poll.pollingType == 'single') {
        this.alertProvider.showAlert('Create Poll', "Please don't leave the required field empty!");
      } else if(this.poll.pollingType == 'multiple') {
        this.alertProvider.showAlert('Create Poll', "- Required field must not be empty<br>- Allowed vote cannot exceed the number of poll options.");
      } else {
        this.alertProvider.showAlert('Create Poll', "Please don't leave the required field empty!");
      }
    }
  }

  getPercentage(totalVote) {

    var percentage = (totalVote / this.totalVote) * 100;

    if(String(percentage) === "NaN") {
      percentage = 0;
    }

    return percentage.toFixed(2);
  }

  updateMultipleVote(options: PollOptions) {
    var found: boolean = false;
    var foundIndex: number;

    if(this.storedVote.length < this.polls.numOfAllowedVote) {
      if(this.storedVote.length > 0) {
        this.storedVote.forEach( (item, index) => {
          if(item.key == options.key) {
            found = true;
            foundIndex = index;
          } 
        });

        if(found) {
          this.storedVote.splice(foundIndex, 1);
        } else {
          this.storedVote.push({
            key: options.key,
            option: options.option,
            totalVote: options.totalVote
          });
        }
      } else {
        this.storedVote.push({
          key: options.key,
          option: options.option,
          totalVote: options.totalVote
        });
      }

      this.storedVoteTotal = this.storedVote.length;
    } else {
      this.storedVote.forEach( (item, index) => {
        if(item.key == options.key) {
          found = true;
          foundIndex = index;
        } 
      });

      if(found) {
        this.storedVote.splice(foundIndex, 1);
      }

      this.storedVoteTotal = this.storedVote.length;
    }
  }

  checking(key) {
    var found: boolean = false;
    
    if(this.storedVote.length == this.polls.numOfAllowedVote) {
      if(this.storedVote.length > 0) {
        this.storedVote.forEach( (item, index) => {
          if(item.key == key) {
            found = true;
          } 
        });

        if(found) {
          return true
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  submitMultipleVote() {
    var multipleVoteList = {};

    this.storedVote.forEach(elem => {

      multipleVoteList['task/' + this.selectedTaskKey + '/poll/pollOptions/' + elem.key] = {
        option: elem.option,
        totalVote: elem.totalVote + 1
      }

      multipleVoteList['task/' + this.selectedTaskKey + '/poll/voters/' + this.currentUserId + '/' + this.fireDB.push().key ] = {
        hasVoted: elem.key
      }

    });

    this.taskProvider.firebaseCRUD(multipleVoteList)
    .then( res => {

      console.log(res);

      this.activityLog.activityType = 'TaskPollLog';
      this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      this.activityLog.handler = this.currentUserId;
      this.activityLog.handlerIsAdmin = this.isAdmin;
      this.activityLog.logActivity = 'has voted on the poll.';

      this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
      .then( r => {
        console.log('Activity has been Logged');
      })
      .catch( e => {
        console.log('Activity Logging: ' + e);
      });
    })
    .catch( err => {
      console.log(err);
    });

    this.storedVote = [];
    this.storedVoteTotal = this.storedVote.length;
  }

  checkMultipleVote(optionsKey) {

    var found = false;

    this.voterMultipleVotes.forEach(elem => {
      if(elem.hasVoted == optionsKey) {
        found = true;
      }
    });

    if(found) {
      return true;
    } else {
      return false;
    }
  }

  updateFilterMaxDate() {
    this.maxFilterDate = (new Date((new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000))).toISOString().slice(0,-1));
  }

  formatPollDate(value) {
    return value.slice(0,-1);
  }

  updateVote(options: PollOptions) {
    var poll = {};

    poll['task/' + this.selectedTaskKey + '/poll/pollOptions/' + options.key] = {
      option: options.option,
      totalVote: options.totalVote + 1
    }

    poll['task/' + this.selectedTaskKey + '/poll/voters/' + this.currentUserId] = {
      hasVoted: options.key
    }

    this.taskProvider.firebaseCRUD(poll)
    .then( res => {
      console.log(res);

      this.activityLog.activityType = 'TaskPollLog';
      this.activityLog.dateNTimeHandled = (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().slice(0,-1);
      this.activityLog.handler = this.currentUserId;
      this.activityLog.handlerIsAdmin = this.isAdmin;
      this.activityLog.logActivity = 'has voted on the poll.';

      this.taskProvider.activityLogging(this.selectedTaskKey, this.activityLog)
      .then( r => {
        console.log('Activity has been Logged');
      })
      .catch( e => {
        console.log('Activity Logging: ' + e);
      });
    })
    .catch( err => {
      console.log(err);
    });
  }

  openActivityLog() {
  	const selectedTaskModal = this.modal.create(ActivitylogPage, {
      taskKey: this.selectedTaskKey
  	});

  	selectedTaskModal.present();
  }

  openPollPage() {
  	const selectedTaskModal = this.modal.create(PollPage, {
      taskKey: this.selectedTaskKey
  	});

  	selectedTaskModal.present();
  }

}

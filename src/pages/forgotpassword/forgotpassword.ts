import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';
import { HomePage } from '../home/home';
/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {

	email: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private user: UserProvider, private alert: AlertProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }

  passwordRecover() {
  	this.user.passwordReset(this.email)
  	.then( res => {
  		this.alert.showAlert('Password Recovery', 'Reset password email sent successfully!<br>Please follow the instructions in the email to reset your password!');
  		this.navCtrl.setRoot(HomePage);
  	})
  	.catch( err => {
  		this.alert.showAlert('Password Recovery', 'Error in sending reset password email. <br>Please check your connection and try again!');
  	});
  }

  backBtn() {
    this.navCtrl.pop();
  }

}
